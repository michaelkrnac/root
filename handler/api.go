package handler
import sqlng "database/sql"
import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"gitlab.com/michaelkrnac/root/database"
	"gitlab.com/michaelkrnac/root/models"
	"gitlab.com/michaelkrnac/root/router"
	"gitlab.com/michaelkrnac/root/wurzel"
)

// /api
type API struct {
	APIBible				*APIBible				// route /api/bible
	APIPerson				*APIPerson				// route /api/person
	APIAnimal				*APIAnimal				// route /api/animal
	APIAnimalClass			*APIAnimalClass			// route /api/animalclass
	APIPlace				*APIPlace				// route /api/place
	APIPlaceType			*APIPlaceType			// route /api/placetype
	APITribe				*APITribe				// route /api/tribe
	APIPlant				*APIPlant				// route /api/plant
	APIPlantType			*APIPlantType			// route /api/planttype
	APIMaterial				*APIMaterial			// route /api/material
	APIMaterialType			*APIMaterialType		// route /api/materialtype
	APIInstrument			*APIInstrument			// route /api/instrument
	APIInstrumentType		*APIInstrumentType		// route /api/instrumenttype
	APIUnit					*APIUnit				// route /api/unit
	APIUnitType				*APIUnitType			// route /api/unittype
	APIThing				*APIThing				// route /api/thing
	APIThingType			*APIThingType			// route /api/thingtype
	APIBooks				*APIBooks				// route /api/books
	APIWord					*APIWord				// route /api/word
	APICollection			*APICollection			// route /api/collection
	APISnippet				*APISnippet				// route /api/snippet
	APIVisuellidentifier	*APIVisuellidentifier	// route /api/visuellidentifier
}
func (h *API) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	h.APIBible = new(APIBible)
	h.APIPerson = new(APIPerson)
	h.APIAnimal = new(APIAnimal)
	h.APIAnimalClass = new(APIAnimalClass)
	h.APIPlace = new(APIPlace)
	h.APIPlaceType = new(APIPlaceType)
	h.APITribe = new(APITribe)
	h.APIPlant = new(APIPlant)
	h.APIPlantType = new(APIPlantType)
	h.APIMaterial = new(APIMaterial)
	h.APIMaterialType = new(APIMaterialType)
	h.APIInstrument = new(APIInstrument)
	h.APIInstrumentType = new(APIInstrumentType)
	h.APIUnit = new(APIUnit)
	h.APIUnitType = new(APIUnitType)
	h.APIThing = new(APIThing)
	h.APIThingType = new(APIThingType)
	h.APIBooks = new(APIBooks)
	h.APIWord = new(APIWord)
	h.APICollection = new(APICollection)
	h.APISnippet = new(APISnippet)
	h.APIVisuellidentifier = new(APIVisuellidentifier)

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "bible":
		h.APIBible.ServeHTTP(res, req)
	case "person":
		h.APIPerson.ServeHTTP(res, req)
	case "animal":
		h.APIAnimal.ServeHTTP(res, req)
	case "animalclass":
		h.APIAnimalClass.ServeHTTP(res, req)
	case "place":
		h.APIPlace.ServeHTTP(res, req)
	case "placetype":
		h.APIPlaceType.ServeHTTP(res, req)
	case "tribe":
		h.APITribe.ServeHTTP(res, req)
	case "plant":
		h.APIPlant.ServeHTTP(res, req)
	case "planttype":
		h.APIPlantType.ServeHTTP(res, req)
	case "material":
		h.APIMaterial.ServeHTTP(res, req)
	case "materialtype":
		h.APIMaterialType.ServeHTTP(res, req)
	case "instrument":
		h.APIInstrument.ServeHTTP(res, req)
	case "instrumenttype":
		h.APIInstrumentType.ServeHTTP(res, req)
	case "unit":
		h.APIUnit.ServeHTTP(res, req)
	case "unittype":
		h.APIUnitType.ServeHTTP(res, req)
	case "thing":
		h.APIThing.ServeHTTP(res, req)
	case "thingtype":
		h.APIThingType.ServeHTTP(res, req)
	case "books":
		h.APIBooks.ServeHTTP(res, req)
	case "word":
		h.APIWord.ServeHTTP(res, req)
	case "collection":
		h.APICollection.ServeHTTP(res, req)
	case "snippet":
		h.APISnippet.ServeHTTP(res, req)
	case "visuellidentifier":
		h.APIVisuellidentifier.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/animal
type APIAnimal struct {
	APIAnimalList   *APIAnimalList   // route /api/animal/
	APIAnimalSelect *APIAnimalSelect // route /api/animal/select/{ID}
	APIAnimalInsert *APIAnimalInsert // route /api/animal/insert
	APIAnimalUpdate *APIAnimalUpdate // route /api/animal/update
	APIAnimalDelete *APIAnimalDelete // route /api/animal/delete
	APIAnimalWord   *APIAnimalWord   // route /api/animal/word
}
func (h *APIAnimal) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	h.APIAnimalWord = new(APIAnimalWord)

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIAnimalList.ServeHTTP(res, req)
	case "select":
		h.APIAnimalSelect.ServeHTTP(res, req)
	case "insert":
		h.APIAnimalInsert.ServeHTTP(res, req)
	case "update":
		h.APIAnimalUpdate.ServeHTTP(res, req)
	case "delete":
		h.APIAnimalDelete.ServeHTTP(res, req)
	case "word":
		h.APIAnimalWord.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/animal/
type APIAnimalList struct {
}
func (h *APIAnimalList) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	animals := []models.Animal{}

	sql := `
		SELECT 
			a.*,
			c.name class_name	
		FROM brute_animal a
			JOIN brute_class c
				ON c.class_id = a.class_id
		ORDER BY a.name;
	`

	err := database.Con.Select(&animals, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, animals)
	return
}

// /api/animal/select/{ID}
type APIAnimalSelect struct {
}
func (h *APIAnimalSelect) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// get the ID from the URL and check if it is a valid INT
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	animalID, err := strconv.Atoi(head)
	if err != nil {
		wurzel.JSONError(res, fmt.Sprintf("Keine gültige Animal ID: %v", head))
		return
	}

	animal := models.Animal{}

	sql := `
		SELECT 
			a.*,
			c.name class_name	
		FROM brute_animal a
			JOIN brute_class c
				ON c.class_id = a.class_id
		WHERE animal_id = :animal_id;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Get(&animal, map[string]interface{}{"animal_id": animalID})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, animal)
	return
}

// /api/animal/insert
type APIAnimalInsert struct {
}
func (h *APIAnimalInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new animal struct
	animal := models.Animal{}
	err := json.NewDecoder(req.Body).Decode(&animal)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO brute_animal 
			(name, description, visuellidentifier_id, class_id)
			VALUES(:name, :description, :visuellidentifier_id, :class_id);
	`

	_, err = database.Con.NamedExec(sql, animal)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich in der Datenbank gespeichert", animal.Name))
	return
}

// /api/animal/update
type APIAnimalUpdate struct {
}
func (h *APIAnimalUpdate) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new animal struct
	animal := models.Animal{}
	err := json.NewDecoder(req.Body).Decode(&animal)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		UPDATE brute_animal 
		SET		name = :name, 
				description = :description, 
				visuellidentifier_id = :visuellidentifier_id, 
				class_id = :class_id 
		WHERE animal_id = :animal_id;
	`

	_, err = database.Con.NamedExec(sql, animal)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Änderungen an %v erfolgreich in der Datenbank gespeichert", animal.Name))
	return
}

// /api/animal/delete
type APIAnimalDelete struct {
}
func (h *APIAnimalDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new animal struct
	animal := models.Animal{}
	err := json.NewDecoder(req.Body).Decode(&animal)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM brute_animal 
		WHERE animal_id = :animal_id;
	`

	_, err = database.Con.NamedExec(sql, animal)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich gelöscht!", animal.Name))
	return
}

// /api/animalclass
type APIAnimalClass struct {
	APIAnimalClassList   *APIAnimalClassList   // route /api/animalclass/
	APIAnimalClassSelect *APIAnimalClassSelect // route /api/animalclass/select/{ID}
	APIAnimalClassInsert *APIAnimalClassInsert // route /api/animalclass/insert
	APIAnimalClassUpdate *APIAnimalClassUpdate // route /api/animalclass/update
	APIAnimalClassDelete *APIAnimalClassDelete // route /api/animalclass/delete
}
func (h *APIAnimalClass) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIAnimalClassList.ServeHTTP(res, req)
	case "select":
		h.APIAnimalClassSelect.ServeHTTP(res, req)
	case "insert":
		h.APIAnimalClassInsert.ServeHTTP(res, req)
	case "update":
		h.APIAnimalClassUpdate.ServeHTTP(res, req)
	case "delete":
		h.APIAnimalClassDelete.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/animalclass/
type APIAnimalClassList struct {
}
func (h *APIAnimalClassList) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	animalclasss := []models.AnimalClass{}

	sql := `
		SELECT *
		FROM brute_class
		ORDER BY name;
	`

	err := database.Con.Select(&animalclasss, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, animalclasss)
	return
}

// /api/animalclass/select/{ID}
type APIAnimalClassSelect struct {
}
func (h *APIAnimalClassSelect) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// get the ID from the URL and check if it is a valid INT
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	animalclassID, err := strconv.Atoi(head)
	if err != nil {
		wurzel.JSONError(res, fmt.Sprintf("Keine gültige AnimalClass ID: %v", head))
		return
	}

	animalclass := models.AnimalClass{}

	sql := `
		SELECT * 
		FROM brute_class
		WHERE class_id = :class_id;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Get(&animalclass, map[string]interface{}{"class_id": animalclassID})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, animalclass)
	return
}

// /api/animalclass/insert
type APIAnimalClassInsert struct {
}
func (h *APIAnimalClassInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new animalclass struct
	animalclass := models.AnimalClass{}
	err := json.NewDecoder(req.Body).Decode(&animalclass)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO brute_class 
			(name, description)
			VALUES(:name, :description);
	`

	_, err = database.Con.NamedExec(sql, animalclass)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich in der Datenbank gespeichert", animalclass.Name))
	return
}

// /api/animalclass/update
type APIAnimalClassUpdate struct {
}
func (h *APIAnimalClassUpdate) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new animalclass struct
	animalclass := models.AnimalClass{}
	err := json.NewDecoder(req.Body).Decode(&animalclass)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		UPDATE brute_class 
		SET		name = :name, 
				description = :description 
		WHERE class_id = :class_id;
	`

	_, err = database.Con.NamedExec(sql, animalclass)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Änderungen an %v erfolgreich in der Datenbank gespeichert", animalclass.Name))
	return
}

// /api/animalclass/delete
type APIAnimalClassDelete struct {
}
func (h *APIAnimalClassDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new animalclass struct
	animalclass := models.AnimalClass{}
	err := json.NewDecoder(req.Body).Decode(&animalclass)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM brute_class 
		WHERE class_id = :class_id;
	`

	_, err = database.Con.NamedExec(sql, animalclass)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich gelöscht!", animalclass.Name))
	return
}

// /api/animal/word
type APIAnimalWord struct {
	APIAnimalWordList   *APIAnimalWordList   // route /api/animal/word/
	APIAnimalWordInsert *APIAnimalWordInsert // route /api/animal/word/insert
	APIAnimalWordDelete *APIAnimalWordDelete // route /api/animal/word/delete
}
func (h *APIAnimalWord) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIAnimalWordList.ServeHTTP(res, req)
	case "insert":
		h.APIAnimalWordInsert.ServeHTTP(res, req)
	case "delete":
		h.APIAnimalWordDelete.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/animal/word/
type APIAnimalWordList struct {
}
func (h *APIAnimalWordList) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	// get the AnimalID as Entityid so we can reuse the word vue component
	// and decode the retrived json
	type entity struct {
		Entityid int
	}
	e := new(entity)

	err := json.NewDecoder(req.Body).Decode(&e)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		SELECT	*, 
				36 AS visuellidentifier_id 
		FROM bible_word_locations 
		WHERE entity_type = 'animal' 
			AND entity_id = :animalID 
		ORDER BY vers_id, position;
	`

	words := []models.Word{}
	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Select(&words, map[string]interface{}{"animalID": e.Entityid})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, words)
	return
}

// /api/animal/word/insert
type APIAnimalWordInsert struct {
}
func (h *APIAnimalWordInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new AnimalWord struct
	animalword := models.AnimalWord{}
	err := json.NewDecoder(req.Body).Decode(&animalword)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO brute_location 
			(word_id, animal_id)
		VALUES(:word_id, :animal_id);
	`

	_, err = database.Con.NamedExec(sql, animalword)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Erfolgreich in der Datenbank gespeichert"))
	return
}

// /api/animal/word/delete
type APIAnimalWordDelete struct {
}
func (h *APIAnimalWordDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new Word struct
	word := models.Word{}
	err := json.NewDecoder(req.Body).Decode(&word)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM brute_location 
		WHERE word_id = :word_id 
			AND animal_id = :entity_id;
	`

	_, err = database.Con.NamedExec(sql, word)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Erfolgreich aus der Datenbank gelöscht!"))
	return
}

// /api/bible
type APIBible struct {
	APIBibleObjectsCount		*APIBibleObjectsCount   // route /api/bible/objectscount
	APIBibleBookWordCount		*APIBibleBookWordCount   // route /api/bible/bookwordcount
	APIBiblePersonWordCount		*APIBiblePersonWordCount   // route /api/bible/personwordcount
	APIBibleWordCount			*APIBibleWordCount   // route /api/bible/wordcount
	APIBibleCollectionCount			*APIBibleCollectionCount   // route /api/bible/collectioncount
}
func (h *APIBible) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "objectscount":
		h.APIBibleObjectsCount.ServeHTTP(res, req)
	case "bookwordcount":
		h.APIBibleBookWordCount.ServeHTTP(res, req)
	case "personwordcount":
		h.APIBiblePersonWordCount.ServeHTTP(res, req)
	case "wordcount":
		h.APIBibleWordCount.ServeHTTP(res, req)
	case "collectioncount":
		h.APIBibleCollectionCount.ServeHTTP(res, req)
	default:
	}
	return
}

// /api/bible/objectscount
type APIBibleObjectsCount struct {
}
func (h *APIBibleObjectsCount) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	counts := []models.Count{}

	sql := `
		SELECT 
			type value, 
			count(*) count
		FROM snippet_object_list
		GROUP BY type;
	`

	err := database.Con.Select(&counts, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, counts)
	return
}

// /api/bible/bookwordcount
type APIBibleBookWordCount struct {
}
func (h *APIBibleBookWordCount) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	counts := []models.Count{}

	sql := `
		SELECT
			b.name value,
			count(*) count
		FROM bible_word_full w
			JOIN bible_book b
				ON w.book_id = b.book_id
		GROUP BY w.book_id;
	`

	err := database.Con.Select(&counts, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, counts)
	return
}

// /api/bible/collectioncount
type APIBibleCollectionCount struct {
}
func (h *APIBibleCollectionCount) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	counts := []models.Count{}

	sql := `
		SELECT 
			c.name value,
			count(b.collection_id) count
		FROM bible_collection c 
			JOIN bible_book b
				ON b.collection_id = c.collection_id
		GROUP BY b.collection_id;	
	`

	err := database.Con.Select(&counts, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, counts)
	return
}

// /api/bible/personwordcount
type APIBiblePersonWordCount struct {
}
func (h *APIBiblePersonWordCount) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	counts := []models.Count{}

	sql := `
		SELECT 
			p.name value,
			count(*) count
		FROM people_location pl
			JOIN people_person p
				ON p.person_id = pl.person_id
		GROUP BY pl.person_id
		ORDER BY count DESC
		LIMIT 10;
	`

	err := database.Con.Select(&counts, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, counts)
	return
}

// /api/bible/wordcount
type APIBibleWordCount struct {
}
func (h *APIBibleWordCount) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	counts := []models.Count{}

	sql := `
		SELECT 
			'unconnected' value,
			count(*) count
		FROM bible_word_locations
		WHERE entity_type = 'word'
		UNION
		SELECT 
			'connected' value,
			count(*) count
		FROM bible_word_locations
		WHERE entity_type != 'word';
	`

	err := database.Con.Select(&counts, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, counts)
	return
}

// /api/books
type APIBooks struct {
}
func (h *APIBooks) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	books := []models.Book{}

	sql := `
		SELECT	book_id, 
				testament, 
				name, 
				name_alternative, 
				name_short, 
				name_long, 
				34 AS visuellidentifier_id 
		FROM bible_book;
	`

	err := database.Con.Select(&books, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, books)
	return
}

// /api/collection
type APICollection struct {
	APICollectionList   *APICollectionList   // route /api/collection/
	APICollectionDetail *APICollectionDetail // route /api/collection/{ID}
}
func (h *APICollection) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	h.APICollectionDetail = new(APICollectionDetail)

	var head string
	head, _ = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APICollectionList.ServeHTTP(res, req)
	default:
		h.APICollectionDetail.ServeHTTP(res, req)
	}
	return
}

// /api/collection/
type APICollectionList struct {
}
func (h *APICollectionList) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	collections := []models.Collection{}

	sql := `
		SELECT	collection_id, 
				name, 
				name_long, 
				35 AS visuellidentifier_id 
		FROM bible_collection;
	`
	err := database.Con.Select(&collections, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, collections)
	return
}

// APICollectionDetail handles collection detail page
type APICollectionDetail struct {
	APICollectionBooks *APICollectionBooks
}
func (h *APICollectionDetail) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	// TODO change it like it is in person/words
	// get the ID from the URL and check if it is a valid INT
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	collectionID, err := strconv.Atoi(head)
	if err != nil {
		wurzel.JSONError(res, fmt.Sprintf("Keine gültige Collection ID: %v", head))
		return
	}

	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "books":
		h.APICollectionBooks.ServeHTTP(res, req, collectionID) // route /api/collection/{ID}/books
	default:
	}

}

// /api/collection/{ID}/books
type APICollectionBooks struct {
}
func (h *APICollectionBooks) ServeHTTP(res http.ResponseWriter, req *http.Request, collectionID int) {

	books := []models.Book{}

	sql := `
		SELECT	book_id, 
				testament, 
				name, 
				name_alternative, 
				name_short, 
				name_long, 
				34 AS visuellidentifier_id 
		FROM bible_book 
		WHERE collection_id = :CollectionID;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	err = stmt.Select(&books, map[string]interface{}{"CollectionID": collectionID})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, books)
	return
}

// /api/instrument
type APIInstrument struct {
	APIInstrumentList   *APIInstrumentList   // route /api/instrument/
	APIInstrumentSelect *APIInstrumentSelect // route /api/instrument/select/{ID}
	APIInstrumentInsert *APIInstrumentInsert // route /api/instrument/insert
	APIInstrumentUpdate *APIInstrumentUpdate // route /api/instrument/update
	APIInstrumentDelete *APIInstrumentDelete // route /api/instrument/delete
	APIInstrumentWord   *APIInstrumentWord   // route /api/instrument/word
}
func (h *APIInstrument) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	h.APIInstrumentWord = new(APIInstrumentWord)

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIInstrumentList.ServeHTTP(res, req)
	case "select":
		h.APIInstrumentSelect.ServeHTTP(res, req)
	case "insert":
		h.APIInstrumentInsert.ServeHTTP(res, req)
	case "update":
		h.APIInstrumentUpdate.ServeHTTP(res, req)
	case "delete":
		h.APIInstrumentDelete.ServeHTTP(res, req)
	case "word":
		h.APIInstrumentWord.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/instrument/
type APIInstrumentList struct {
}
func (h *APIInstrumentList) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	instruments := []models.Instrument{}

	sql := `
		SELECT 
			i.*,
			it.instrument_type_id instrument_type_id,	
			it.name instrument_type_name
		FROM music_instrument i
			JOIN music_instrument_type it
				ON i.instrument_type_id = it.instrument_type_id
		ORDER BY i.name;
	`

	err := database.Con.Select(&instruments, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, instruments)
	return
}

// /api/instrument/select/{ID}
type APIInstrumentSelect struct {
}
func (h *APIInstrumentSelect) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// get the ID from the URL and check if it is a valid INT
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	instrumentID, err := strconv.Atoi(head)
	if err != nil {
		wurzel.JSONError(res, fmt.Sprintf("Keine gültige Instrument ID: %v", head))
		return
	}

	instrument := models.Instrument{}

	sql := `
		SELECT 
			i.*,
			it.instrument_type_id instrument_type_id,	
			it.name instrument_type_name
		FROM music_instrument i
			JOIN music_instrument_type it
				ON i.instrument_type_id = it.instrument_type_id
		WHERE i.instrument_id = :instrument_id;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Get(&instrument, map[string]interface{}{"instrument_id": instrumentID})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, instrument)
	return
}

// /api/instrument/insert
type APIInstrumentInsert struct {
}
func (h *APIInstrumentInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new instrument struct
	instrument := models.Instrument{}
	err := json.NewDecoder(req.Body).Decode(&instrument)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO music_instrument 
			(name, description, visuellidentifier_id, instrument_type_id)
			VALUES(:name, :description, :visuellidentifier_id, :instrument_type_id);
	`

	_, err = database.Con.NamedExec(sql, instrument)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich in der Datenbank gespeichert", instrument.Name))
	return
}

// /api/instrument/update
type APIInstrumentUpdate struct {
}
func (h *APIInstrumentUpdate) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new instrument struct
	instrument := models.Instrument{}
	err := json.NewDecoder(req.Body).Decode(&instrument)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		UPDATE music_instrument 
		SET		name = :name, 
				description = :description, 
				visuellidentifier_id = :visuellidentifier_id, 
				instrument_type_id = :instrument_type_id 
		WHERE instrument_id = :instrument_id;
	`

	_, err = database.Con.NamedExec(sql, instrument)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Änderungen an %v erfolgreich in der Datenbank gespeichert", instrument.Name))
	return
}

// /api/instrument/delete
type APIInstrumentDelete struct {
}
func (h *APIInstrumentDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new instrument struct
	instrument := models.Instrument{}
	err := json.NewDecoder(req.Body).Decode(&instrument)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM music_instrument 
		WHERE instrument_id = :instrument_id;
	`

	_, err = database.Con.NamedExec(sql, instrument)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich gelöscht!", instrument.Name))
	return
}

// /api/instrumenttype
type APIInstrumentType struct {
	APIInstrumentTypeList   *APIInstrumentTypeList   // route /api/instrumenttype/
	APIInstrumentTypeSelect *APIInstrumentTypeSelect // route /api/instrumenttype/select/{ID}
	APIInstrumentTypeInsert *APIInstrumentTypeInsert // route /api/instrumenttype/insert
	APIInstrumentTypeUpdate *APIInstrumentTypeUpdate // route /api/instrumenttype/update
	APIInstrumentTypeDelete *APIInstrumentTypeDelete // route /api/instrumenttype/delete
}
func (h *APIInstrumentType) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIInstrumentTypeList.ServeHTTP(res, req)
	case "select":
		h.APIInstrumentTypeSelect.ServeHTTP(res, req)
	case "insert":
		h.APIInstrumentTypeInsert.ServeHTTP(res, req)
	case "update":
		h.APIInstrumentTypeUpdate.ServeHTTP(res, req)
	case "delete":
		h.APIInstrumentTypeDelete.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/instrumenttype/
type APIInstrumentTypeList struct {
}
func (h *APIInstrumentTypeList) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	instrumenttypes := []models.InstrumentType{}

	sql := `
		SELECT *
		FROM music_instrument_type
		ORDER BY name;
	`

	err := database.Con.Select(&instrumenttypes, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, instrumenttypes)
	return
}

// /api/instrumenttype/select/{ID}
type APIInstrumentTypeSelect struct {
}
func (h *APIInstrumentTypeSelect) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// get the ID from the URL and check if it is a valid INT
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	instrumenttypeID, err := strconv.Atoi(head)
	if err != nil {
		wurzel.JSONError(res, fmt.Sprintf("Keine gültige InstrumentType ID: %v", head))
		return
	}

	instrumenttype := models.InstrumentType{}

	sql := `
		SELECT * 
		FROM music_instrument_type
		WHERE instrument_type_id = :instrument_type_id;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Get(&instrumenttype, map[string]interface{}{"instrument_type_id": instrumenttypeID})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, instrumenttype)
	return
}

// /api/instrumenttype/insert
type APIInstrumentTypeInsert struct {
}
func (h *APIInstrumentTypeInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new instrumenttype struct
	instrumenttype := models.InstrumentType{}
	err := json.NewDecoder(req.Body).Decode(&instrumenttype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO music_instrument_type
			(name, description, visuellidentifier_id)
			VALUES(:name, :description, :visuellidentifier_id);
	`

	_, err = database.Con.NamedExec(sql, instrumenttype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich in der Datenbank gespeichert", instrumenttype.Name))
	return
}

// /api/instrumenttype/update
type APIInstrumentTypeUpdate struct {
}
func (h *APIInstrumentTypeUpdate) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new instrumenttype struct
	instrumenttype := models.InstrumentType{}
	err := json.NewDecoder(req.Body).Decode(&instrumenttype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		UPDATE music_instrument_type 
		SET		name = :name, 
				description = :description ,
				visuellidentifier_id = :visuellidentifier_id
		WHERE instrument_type_id = :instrument_type_id;
	`

	_, err = database.Con.NamedExec(sql, instrumenttype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Änderungen an %v erfolgreich in der Datenbank gespeichert", instrumenttype.Name))
	return
}

// /api/instrumenttype/delete
type APIInstrumentTypeDelete struct {
}
func (h *APIInstrumentTypeDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new instrumenttype struct
	instrumenttype := models.InstrumentType{}
	err := json.NewDecoder(req.Body).Decode(&instrumenttype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM music_instrument_type 
		WHERE instrument_type_id = :instrument_type_id;
	`

	_, err = database.Con.NamedExec(sql, instrumenttype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich gelöscht!", instrumenttype.Name))
	return
}

// /api/instrument/word
type APIInstrumentWord struct {
	APIInstrumentWordList   *APIInstrumentWordList   // route /api/instrument/word/
	APIInstrumentWordInsert *APIInstrumentWordInsert // route /api/instrument/word/insert
	APIInstrumentWordDelete *APIInstrumentWordDelete // route /api/instrument/word/delete
}
func (h *APIInstrumentWord) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIInstrumentWordList.ServeHTTP(res, req)
	case "insert":
		h.APIInstrumentWordInsert.ServeHTTP(res, req)
	case "delete":
		h.APIInstrumentWordDelete.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/instrument/word/
type APIInstrumentWordList struct {
}
func (h *APIInstrumentWordList) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	// get the InstrumentID as Entityid so we can reuse the word vue component
	// and decode the retrived json
	type entity struct {
		Entityid int
	}
	e := new(entity)

	err := json.NewDecoder(req.Body).Decode(&e)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		SELECT	*, 
				36 AS visuellidentifier_id 
		FROM bible_word_locations 
		WHERE entity_type = 'instrument' 
			AND entity_id = :instrumentID 
		ORDER BY vers_id, position;
	`

	words := []models.Word{}
	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Select(&words, map[string]interface{}{"instrumentID": e.Entityid})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, words)
	return
}

// /api/instrument/word/insert
type APIInstrumentWordInsert struct {
}
func (h *APIInstrumentWordInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new InstrumentWord struct
	instrumentword := models.InstrumentWord{}
	err := json.NewDecoder(req.Body).Decode(&instrumentword)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO music_location 
			(word_id, instrument_id)
		VALUES(:word_id, :instrument_id);
	`

	_, err = database.Con.NamedExec(sql, instrumentword)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Erfolgreich in der Datenbank gespeichert"))
	return
}

// /api/instrument/word/delete
type APIInstrumentWordDelete struct {
}
func (h *APIInstrumentWordDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new Word struct
	word := models.Word{}
	err := json.NewDecoder(req.Body).Decode(&word)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM music_location 
		WHERE word_id = :word_id 
			AND instrument_id = :entity_id;
	`

	_, err = database.Con.NamedExec(sql, word)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Erfolgreich aus der Datenbank gelöscht!"))
	return
}

// /api/material
type APIMaterial struct {
	APIMaterialList   *APIMaterialList   // route /api/material/
	APIMaterialSelect *APIMaterialSelect // route /api/material/select/{ID}
	APIMaterialInsert *APIMaterialInsert // route /api/material/insert
	APIMaterialUpdate *APIMaterialUpdate // route /api/material/update
	APIMaterialDelete *APIMaterialDelete // route /api/material/delete
	APIMaterialWord   *APIMaterialWord   // route /api/material/word
}
func (h *APIMaterial) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	h.APIMaterialWord = new(APIMaterialWord)

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIMaterialList.ServeHTTP(res, req)
	case "select":
		h.APIMaterialSelect.ServeHTTP(res, req)
	case "insert":
		h.APIMaterialInsert.ServeHTTP(res, req)
	case "update":
		h.APIMaterialUpdate.ServeHTTP(res, req)
	case "delete":
		h.APIMaterialDelete.ServeHTTP(res, req)
	case "word":
		h.APIMaterialWord.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/material
type APIMaterialList struct {
}
func (h *APIMaterialList) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	materials := []models.Material{}

	sql := `
		SELECT 
			m.*,
			mt.material_type_id material_type_id,	
			mt.name material_type_name
		FROM element_material m
			JOIN element_material_type mt
				ON m.material_type_id = mt.material_type_id
		ORDER BY m.name;
	`

	err := database.Con.Select(&materials, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, materials)
	return
}

// /api/material/select/{ID}
type APIMaterialSelect struct {
}
func (h *APIMaterialSelect) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// get the ID from the URL and check if it is a valid INT
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	materialID, err := strconv.Atoi(head)
	if err != nil {
		wurzel.JSONError(res, fmt.Sprintf("Keine gültige Material ID: %v", head))
		return
	}

	material := models.Material{}

	sql := `
		SELECT 
			m.*,
			mt.material_type_id material_type_id,	
			mt.name material_type_name
		FROM element_material m
			JOIN element_material_type mt
				ON m.material_type_id = mt.material_type_id
		WHERE m.material_id = :material_id;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Get(&material, map[string]interface{}{"material_id": materialID})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, material)
	return
}

// /api/material/insert
type APIMaterialInsert struct {
}
func (h *APIMaterialInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new material struct
	material := models.Material{}
	err := json.NewDecoder(req.Body).Decode(&material)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO element_material 
			(name, description, visuellidentifier_id, material_type_id)
			VALUES(:name, :description, :visuellidentifier_id, :material_type_id);
	`

	_, err = database.Con.NamedExec(sql, material)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich in der Datenbank gespeichert", material.Name))
	return
}

// /api/material/update
type APIMaterialUpdate struct {
}
func (h *APIMaterialUpdate) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new material struct
	material := models.Material{}
	err := json.NewDecoder(req.Body).Decode(&material)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		UPDATE element_material 
		SET		name = :name, 
				description = :description, 
				visuellidentifier_id = :visuellidentifier_id, 
				material_type_id = :material_type_id 
		WHERE material_id = :material_id;
	`

	_, err = database.Con.NamedExec(sql, material)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Änderungen an %v erfolgreich in der Datenbank gespeichert", material.Name))
	return
}

// /api/material/delete
type APIMaterialDelete struct {
}
func (h *APIMaterialDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new material struct
	material := models.Material{}
	err := json.NewDecoder(req.Body).Decode(&material)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM element_material 
		WHERE material_id = :material_id;
	`

	_, err = database.Con.NamedExec(sql, material)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich gelöscht!", material.Name))
	return
}

// /api/materialtype
type APIMaterialType struct {
	APIMaterialTypeList   *APIMaterialTypeList   // route /api/materialtype/
	APIMaterialTypeSelect *APIMaterialTypeSelect // route /api/materialtype/select/{ID}
	APIMaterialTypeInsert *APIMaterialTypeInsert // route /api/materialtype/insert
	APIMaterialTypeUpdate *APIMaterialTypeUpdate // route /api/materialtype/update
	APIMaterialTypeDelete *APIMaterialTypeDelete // route /api/materialtype/delete
}
func (h *APIMaterialType) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIMaterialTypeList.ServeHTTP(res, req)
	case "select":
		h.APIMaterialTypeSelect.ServeHTTP(res, req)
	case "insert":
		h.APIMaterialTypeInsert.ServeHTTP(res, req)
	case "update":
		h.APIMaterialTypeUpdate.ServeHTTP(res, req)
	case "delete":
		h.APIMaterialTypeDelete.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/materialtype
type APIMaterialTypeList struct {
}
func (h *APIMaterialTypeList) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	materialtypes := []models.MaterialType{}

	sql := `
		SELECT *
		FROM element_material_type
		ORDER BY name;
	`

	err := database.Con.Select(&materialtypes, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, materialtypes)
	return
}

// /api/materialtype/select/{ID}
type APIMaterialTypeSelect struct {
}
func (h *APIMaterialTypeSelect) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// get the ID from the URL and check if it is a valid INT
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	materialtypeID, err := strconv.Atoi(head)
	if err != nil {
		wurzel.JSONError(res, fmt.Sprintf("Keine gültige MaterialType ID: %v", head))
		return
	}

	materialtype := models.MaterialType{}

	sql := `
		SELECT * 
		FROM element_material_type
		WHERE material_type_id = :material_type_id;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Get(&materialtype, map[string]interface{}{"material_type_id": materialtypeID})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, materialtype)
	return
}

// /api/materialtype/insert
type APIMaterialTypeInsert struct {
}
func (h *APIMaterialTypeInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new materialtype struct
	materialtype := models.MaterialType{}
	err := json.NewDecoder(req.Body).Decode(&materialtype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO element_material_type
			(name, description, visuellidentifier_id)
			VALUES(:name, :description, :visuellidentifier_id);
	`

	_, err = database.Con.NamedExec(sql, materialtype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich in der Datenbank gespeichert", materialtype.Name))
	return
}

// /api/materialtype/update
type APIMaterialTypeUpdate struct {
}
func (h *APIMaterialTypeUpdate) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new materialtype struct
	materialtype := models.MaterialType{}
	err := json.NewDecoder(req.Body).Decode(&materialtype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		UPDATE element_material_type 
		SET		name = :name, 
				description = :description ,
				visuellidentifier_id = :visuellidentifier_id
		WHERE material_type_id = :material_type_id;
	`

	_, err = database.Con.NamedExec(sql, materialtype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Änderungen an %v erfolgreich in der Datenbank gespeichert", materialtype.Name))
	return
}

// /api/materialtype/delete
type APIMaterialTypeDelete struct {
}
func (h *APIMaterialTypeDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new materialtype struct
	materialtype := models.MaterialType{}
	err := json.NewDecoder(req.Body).Decode(&materialtype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM element_material_type 
		WHERE material_type_id = :material_type_id;
	`

	_, err = database.Con.NamedExec(sql, materialtype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich gelöscht!", materialtype.Name))
	return
}

// /api/material/word
type APIMaterialWord struct {
	APIMaterialWordList   *APIMaterialWordList   // route /api/material/word/
	APIMaterialWordInsert *APIMaterialWordInsert // route /api/material/word/insert
	APIMaterialWordDelete *APIMaterialWordDelete // route /api/material/word/delete
}
func (h *APIMaterialWord) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIMaterialWordList.ServeHTTP(res, req)
	case "insert":
		h.APIMaterialWordInsert.ServeHTTP(res, req)
	case "delete":
		h.APIMaterialWordDelete.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/material/word/
type APIMaterialWordList struct {
}
func (h *APIMaterialWordList) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	// get the MaterialID as Entityid so we can reuse the word vue component
	// and decode the retrived json
	type entity struct {
		Entityid int
	}
	e := new(entity)

	err := json.NewDecoder(req.Body).Decode(&e)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		SELECT	*, 
				36 AS visuellidentifier_id 
		FROM bible_word_locations 
		WHERE entity_type = 'material' 
			AND entity_id = :materialID 
		ORDER BY vers_id, position;
	`

	words := []models.Word{}
	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Select(&words, map[string]interface{}{"materialID": e.Entityid})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, words)
	return
}

// /api/material/word/insert
type APIMaterialWordInsert struct {
}
func (h *APIMaterialWordInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new MaterialWord struct
	materialword := models.MaterialWord{}
	err := json.NewDecoder(req.Body).Decode(&materialword)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO element_location 
			(word_id, material_id)
		VALUES(:word_id, :material_id);
	`

	_, err = database.Con.NamedExec(sql, materialword)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Erfolgreich in der Datenbank gespeichert"))
	return
}

// /api/material/word/delete
type APIMaterialWordDelete struct {
}
func (h *APIMaterialWordDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new Word struct
	word := models.Word{}
	err := json.NewDecoder(req.Body).Decode(&word)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM element_location 
		WHERE word_id = :word_id 
			AND material_id = :entity_id;
	`

	_, err = database.Con.NamedExec(sql, word)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Erfolgreich aus der Datenbank gelöscht!"))
	return
}

// /api/person
type APIPerson struct {
	APIPersonList       *APIPersonList       // route /api/person/
	APIPersonSelect     *APIPersonSelect     // route /api/person/select/{ID}
	APIPersonInsert     *APIPersonInsert     // route /api/person/insert
	APIPersonUpdate     *APIPersonUpdate     // route /api/person/update
	APIPersonDelete     *APIPersonDelete     // route /api/person/delete
	APIPersonWord       *APIPersonWord       // route /api/person/word
	APIPersonFather     *APIPersonFather     // route /api/person/father
	APIPersonMother     *APIPersonMother     // route /api/person/mother
	APIPersonReferences *APIPersonReferences // rout /api/person/references
}
func (h *APIPerson) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	h.APIPersonWord = new(APIPersonWord)
	h.APIPersonFather = new(APIPersonFather)
	h.APIPersonMother = new(APIPersonMother)
	h.APIPersonReferences = new(APIPersonReferences)

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIPersonList.ServeHTTP(res, req)
	case "select":
		h.APIPersonSelect.ServeHTTP(res, req)
	case "insert":
		h.APIPersonInsert.ServeHTTP(res, req)
	case "update":
		h.APIPersonUpdate.ServeHTTP(res, req)
	case "delete":
		h.APIPersonDelete.ServeHTTP(res, req)
	case "word":
		h.APIPersonWord.ServeHTTP(res, req)
	case "father":
		h.APIPersonFather.ServeHTTP(res, req)
	case "mother":
		h.APIPersonMother.ServeHTTP(res, req)
	case "references":
		h.APIPersonReferences.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/person/
type APIPersonList struct {
}
func (h *APIPersonList) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	persons := []models.Person{}

	sql := `
		SELECT * 
		FROM people_person_list 
		ORDER BY name;
	`

	err := database.Con.Select(&persons, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, persons)
	return
}

// /api/person/select/{ID}
type APIPersonSelect struct {
}
func (h *APIPersonSelect) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// get the ID from the URL and check if it is a valid INT
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	personID, err := strconv.Atoi(head)
	if err != nil {
		wurzel.JSONError(res, fmt.Sprintf("Keine gültige Personen ID: %v", head))
		return
	}

	person := models.Person{}

	sql := `
		SELECT * 
		FROM people_person_list 
		WHERE person_id = :person_id;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Get(&person, map[string]interface{}{"person_id": personID})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, person)
	return
}

// /api/person/insert
type APIPersonInsert struct {
}
func (h *APIPersonInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new person struct
	person := models.Person{}
	err := json.NewDecoder(req.Body).Decode(&person)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO people_person 
			(name, description,gender)
		VALUES(:name, :description, :gender);
	`

	_, err = database.Con.NamedExec(sql, person)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich in der Datenbank gespeichert", person.Name))
	return
}

// /api/person/update
type APIPersonUpdate struct {
}
func (h *APIPersonUpdate) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new person struct
	person := models.Person{}
	err := json.NewDecoder(req.Body).Decode(&person)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		UPDATE people_person 
		SET		name = :name, 
				description = :description, 
				gender = :gender 
		WHERE person_id = :person_id;
	`

	_, err = database.Con.NamedExec(sql, person)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Änderungen an %v erfolgreich in der Datenbank gespeichert", person.Name))
	return
}

// /api/person/delete
type APIPersonDelete struct {
}
func (h *APIPersonDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new person struct
	person := models.Person{}
	err := json.NewDecoder(req.Body).Decode(&person)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM people_person 
		WHERE person_id = :person_id;
	`

	_, err = database.Con.NamedExec(sql, person)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich gelöscht!", person.Name))
	return
}

// /api/person/references
type APIPersonReferences struct {
}
func (h *APIPersonReferences) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	personreferences := []models.PersonReference{}

	sql := `
		SELECT * 
		FROM people_person_references;
	`

	err := database.Con.Select(&personreferences, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, personreferences)
	return
}

// /api/person/father
type APIPersonFather struct {
	APIPersonFatherSelect *APIPersonFatherSelect // route /api/person/father/
	APIPersonFatherInsert *APIPersonFatherInsert // route /api/person/father/insert
	APIPersonFatherDelete *APIPersonFatherDelete // route /api/person/father/delete
}
func (h *APIPersonFather) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIPersonFatherSelect.ServeHTTP(res, req)
	case "insert":
		h.APIPersonFatherInsert.ServeHTTP(res, req)
	case "delete":
		h.APIPersonFatherDelete.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/person/father/
type APIPersonFatherSelect struct {
}
func (h *APIPersonFatherSelect) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new person struct
	person := models.Person{}
	err := json.NewDecoder(req.Body).Decode(&person)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		SELECT p.* 
		FROM people_parents pp
			JOIN people_person_list p
				ON pp.father_id = p.person_id 
		WHERE children_id = :person_id;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	father := models.Person{}
	err = stmt.Get(&father, map[string]interface{}{"person_id": person.PersonID})
	if err != nil {
		switch err {
		case sqlng.ErrNoRows:
			return
		default:
			log.Print(err)
			wurzel.JSONError(res, "Datenbankfehler")
			return
		}
	}
	defer stmt.Close()

	wurzel.JSONContent(res, father)
	return
}

// /api/person/father/insert
type APIPersonFatherInsert struct {
}
func (h *APIPersonFatherInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new person struct
	pf := models.PersonFather{}
	err := json.NewDecoder(req.Body).Decode(&pf)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO people_parents 
			(children_id, father_id)
		VALUES(:children_id, :father_id)
			ON DUPLICATE KEY UPDATE father_id = :father_id;
	`

	_, err = database.Con.NamedExec(sql, pf)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Vater erfolgreich in der Datenbank gespeichert"))
	return
}

// /api/person/father/delete
type APIPersonFatherDelete struct {
}
func (h *APIPersonFatherDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new person struct
	pf := models.PersonFather{}
	err := json.NewDecoder(req.Body).Decode(&pf)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
	UPDATE	people_parents 
	SET		father_id = null
	WHERE	children_id = :children_id
	`

	_, err = database.Con.NamedExec(sql, pf)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Vater erfolgreich aus der Datenbank gelöscht!"))
	return
}

// /api/person/mother
type APIPersonMother struct {
	APIPersonMotherSelect *APIPersonMotherSelect // route /api/person/father/
	APIPersonMotherInsert *APIPersonMotherInsert // route /api/person/father/insert
	APIPersonMotherDelete *APIPersonMotherDelete // route /api/person/father/delete
}
func (h *APIPersonMother) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIPersonMotherSelect.ServeHTTP(res, req)
	case "insert":
		h.APIPersonMotherInsert.ServeHTTP(res, req)
	case "delete":
		h.APIPersonMotherDelete.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/person/mother/
type APIPersonMotherSelect struct {
}
func (h *APIPersonMotherSelect) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new person struct
	person := models.Person{}
	err := json.NewDecoder(req.Body).Decode(&person)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		SELECT p.* 
		FROM people_parents pp
			JOIN people_person_list p
				ON pp.mother_id = p.person_id 
		WHERE children_id = :person_id;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	mother := models.Person{}
	err = stmt.Get(&mother, map[string]interface{}{"person_id": person.PersonID})
	if err != nil {
		switch err {
		case sqlng.ErrNoRows:
			return
		default:
			log.Print(err)
			wurzel.JSONError(res, "Datenbankfehler")
			return
		}
	}
	defer stmt.Close()

	wurzel.JSONContent(res, mother)
	return
}

// /api/person/mother/insert
type APIPersonMotherInsert struct {
}
func (h *APIPersonMotherInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new person struct
	pf := models.PersonMother{}
	err := json.NewDecoder(req.Body).Decode(&pf)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO people_parents
			(children_id, mother_id)
		VALUES(:children_id, :mother_id)
			ON DUPLICATE KEY UPDATE mother_id = :mother_id;
	`

	_, err = database.Con.NamedExec(sql, pf)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Mutter erfolgreich in der Datenbank gespeichert"))
	return
}

// /api/person/mother/delete
type APIPersonMotherDelete struct {
}
func (h *APIPersonMotherDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new person struct
	pf := models.PersonMother{}
	err := json.NewDecoder(req.Body).Decode(&pf)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
	UPDATE	people_parents 
	SET		mother_id = null
	WHERE	children_id = :children_id
	`

	_, err = database.Con.NamedExec(sql, pf)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Mutter erfolgreich aus der Datenbank gelöscht!"))
	return
}

// /api/person/word
type APIPersonWord struct {
	APIPersonWordList   *APIPersonWordList   // route /api/person/word/
	APIPersonWordInsert *APIPersonWordInsert // route /api/person/word/insert
	APIPersonWordDelete *APIPersonWordDelete // route /api/person/word/delete
}
func (h *APIPersonWord) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIPersonWordList.ServeHTTP(res, req)
	case "insert":
		h.APIPersonWordInsert.ServeHTTP(res, req)
	case "delete":
		h.APIPersonWordDelete.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/person/word/
type APIPersonWordList struct {
}
func (h *APIPersonWordList) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	// get the PersonID as Entityid so we can reuse the word vue component
	// and decode the retrived json
	type entity struct {
		Entityid int
	}
	e := new(entity)

	err := json.NewDecoder(req.Body).Decode(&e)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		SELECT	*, 
				36 AS visuellidentifier_id 
		FROM bible_word_locations 
		WHERE entity_type = 'person' 
			AND entity_id = :personID 
		ORDER BY vers_id, position;
	`

	words := []models.Word{}
	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Select(&words, map[string]interface{}{"personID": e.Entityid})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, words)
	return
}

// /api/person/word/insert
type APIPersonWordInsert struct {
}
func (h *APIPersonWordInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new PersonWord struct
	personword := models.PersonWord{}
	err := json.NewDecoder(req.Body).Decode(&personword)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO people_location 
			(word_id, person_id)
		VALUES(:word_id, :person_id);
	`

	_, err = database.Con.NamedExec(sql, personword)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Erfolgreich in der Datenbank gespeichert"))
	return
}

// /api/person/word/delete
type APIPersonWordDelete struct {
}
func (h *APIPersonWordDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new Word struct
	word := models.Word{}
	err := json.NewDecoder(req.Body).Decode(&word)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM people_location 
		WHERE word_id = :word_id 
			AND person_id = :entity_id;
	`

	_, err = database.Con.NamedExec(sql, word)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Erfolgreich aus der Datenbank gelöscht!"))
	return
}

// /api/place
type APIPlace struct {
	APIPlaceList   *APIPlaceList   // route /api/place/
	APIPlaceSelect *APIPlaceSelect // route /api/place/select/{ID}
	APIPlaceInsert *APIPlaceInsert // route /api/place/insert
	APIPlaceUpdate *APIPlaceUpdate // route /api/place/update
	APIPlaceDelete *APIPlaceDelete // route /api/place/delete
	APIPlaceWord   *APIPlaceWord   // route /api/place/word
}
func (h *APIPlace) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	h.APIPlaceWord = new(APIPlaceWord)

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIPlaceList.ServeHTTP(res, req)
	case "select":
		h.APIPlaceSelect.ServeHTTP(res, req)
	case "insert":
		h.APIPlaceInsert.ServeHTTP(res, req)
	case "update":
		h.APIPlaceUpdate.ServeHTTP(res, req)
	case "delete":
		h.APIPlaceDelete.ServeHTTP(res, req)
	case "word":
		h.APIPlaceWord.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/place/
type APIPlaceList struct {
}
func (h *APIPlaceList) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	places := []models.Place{}

	sql := `
		SELECT 
			p.*,
			pt.place_type_id,
			pt.name place_type_name,
			pt.visuellidentifier_id
		FROM spot_place p
			JOIN spot_place_type pt
				ON p.place_type_id = pt.place_type_id
		ORDER BY p.name;
	`

	err := database.Con.Select(&places, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, places)
	return
}

// /api/place/select/{ID}
type APIPlaceSelect struct {
}
func (h *APIPlaceSelect) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// get the ID from the URL and check if it is a valid INT
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	placeID, err := strconv.Atoi(head)
	if err != nil {
		wurzel.JSONError(res, fmt.Sprintf("Keine gültige Place ID: %v", head))
		return
	}

	place := models.Place{}

	sql := `
		SELECT 
			p.*,
			pt.place_type_id,
			pt.name place_type_name,
			pt.visuellidentifier_id
		FROM spot_place p
			JOIN spot_place_type pt
				ON p.place_type_id = pt.place_type_id
		WHERE p.place_id = :place_id;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Get(&place, map[string]interface{}{"place_id": placeID})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, place)
	return
}

// /api/place/insert
type APIPlaceInsert struct {
}
func (h *APIPlaceInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new place struct
	place := models.Place{}
	err := json.NewDecoder(req.Body).Decode(&place)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO spot_place 
			(name, description, place_type_id, lat, lon)
			VALUES(:name, :description, :place_type_id, :lat, :lon);
	`

	_, err = database.Con.NamedExec(sql, place)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich in der Datenbank gespeichert", place.Name))
	return
}

// /api/place/update
type APIPlaceUpdate struct {
}
func (h *APIPlaceUpdate) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	// Decode JSON object into a new place struct
	place := models.Place{}
	err := json.NewDecoder(req.Body).Decode(&place)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		UPDATE spot_place 
		SET		name = :name, 
				description = :description, 
				place_type_id = :place_type_id,
				lat = :lat,
				lon = :lon
		WHERE place_id = :place_id;
	`

	_, err = database.Con.NamedExec(sql, place)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Änderungen an %v erfolgreich in der Datenbank gespeichert", place.Name))
	return
}

// /api/place/delete
type APIPlaceDelete struct {
}
func (h *APIPlaceDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new place struct
	place := models.Place{}
	err := json.NewDecoder(req.Body).Decode(&place)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM spot_place 
		WHERE place_id = :place_id;
	`

	_, err = database.Con.NamedExec(sql, place)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich gelöscht!", place.Name))
	return
}

// /api/placetype
type APIPlaceType struct {
	APIPlaceTypeList   *APIPlaceTypeList   // route /api/placetype/
	APIPlaceTypeSelect *APIPlaceTypeSelect // route /api/placetype/select/{ID}
	APIPlaceTypeInsert *APIPlaceTypeInsert // route /api/placetype/insert
	APIPlaceTypeUpdate *APIPlaceTypeUpdate // route /api/placetype/update
	APIPlaceTypeDelete *APIPlaceTypeDelete // route /api/placetype/delete
}
func (h *APIPlaceType) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIPlaceTypeList.ServeHTTP(res, req)
	case "select":
		h.APIPlaceTypeSelect.ServeHTTP(res, req)
	case "insert":
		h.APIPlaceTypeInsert.ServeHTTP(res, req)
	case "update":
		h.APIPlaceTypeUpdate.ServeHTTP(res, req)
	case "delete":
		h.APIPlaceTypeDelete.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/placetype
type APIPlaceTypeList struct {
}
func (h *APIPlaceTypeList) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	placetypes := []models.PlaceType{}

	sql := `
		SELECT *
		FROM spot_place_type
		ORDER BY name;
	`

	err := database.Con.Select(&placetypes, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, placetypes)
	return
}

// /api/placetype/select/{ID}
type APIPlaceTypeSelect struct {
}
func (h *APIPlaceTypeSelect) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// get the ID from the URL and check if it is a valid INT
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	placetypeID, err := strconv.Atoi(head)
	if err != nil {
		wurzel.JSONError(res, fmt.Sprintf("Keine gültige PlaceType ID: %v", head))
		return
	}

	placetype := models.PlaceType{}

	sql := `
		SELECT * 
		FROM spot_place_type
		WHERE place_type_id = :place_type_id;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Get(&placetype, map[string]interface{}{"place_type_id": placetypeID})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, placetype)
	return
}

// /api/placetype/insert
type APIPlaceTypeInsert struct {
}
func (h *APIPlaceTypeInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new placetype struct
	placetype := models.PlaceType{}
	err := json.NewDecoder(req.Body).Decode(&placetype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO spot_place_type 
			(name, description, visuellidentifier_id)
			VALUES(:name, :description, :visuellidentifier_id);
	`

	_, err = database.Con.NamedExec(sql, placetype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich in der Datenbank gespeichert", placetype.Name))
	return
}

// /api/placetype/update
type APIPlaceTypeUpdate struct {
}
func (h *APIPlaceTypeUpdate) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new placetype struct
	placetype := models.PlaceType{}
	err := json.NewDecoder(req.Body).Decode(&placetype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		UPDATE spot_place_type 
		SET		name = :name, 
				description = :description,
				visuellidentifier_id = :visuellidentifier_id
		WHERE place_type_id = :place_type_id;
	`

	_, err = database.Con.NamedExec(sql, placetype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Änderungen an %v erfolgreich in der Datenbank gespeichert", placetype.Name))
	return
}

// /api/placetype/delete
type APIPlaceTypeDelete struct {
}
func (h *APIPlaceTypeDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new placetype struct
	placetype := models.PlaceType{}
	err := json.NewDecoder(req.Body).Decode(&placetype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM spot_place_type 
		WHERE place_type_id = :place_type_id;
	`

	_, err = database.Con.NamedExec(sql, placetype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich gelöscht!", placetype.Name))
	return
}

// /api/place/word
type APIPlaceWord struct {
	APIPlaceWordList   *APIPlaceWordList   // route /api/place/word/
	APIPlaceWordInsert *APIPlaceWordInsert // route /api/place/word/insert
	APIPlaceWordDelete *APIPlaceWordDelete // route /api/place/word/delete
}
func (h *APIPlaceWord) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIPlaceWordList.ServeHTTP(res, req)
	case "insert":
		h.APIPlaceWordInsert.ServeHTTP(res, req)
	case "delete":
		h.APIPlaceWordDelete.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/place/word/
type APIPlaceWordList struct {
}
func (h *APIPlaceWordList) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	// get the PlaceID as Entityid so we can reuse the word vue component
	// and decode the retrived json
	type entity struct {
		Entityid int
	}
	e := new(entity)

	err := json.NewDecoder(req.Body).Decode(&e)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		SELECT	*, 
				36 AS visuellidentifier_id 
		FROM bible_word_locations 
		WHERE entity_type = 'place' 
			AND entity_id = :placeID 
		ORDER BY vers_id, position;
	`

	words := []models.Word{}
	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Select(&words, map[string]interface{}{"placeID": e.Entityid})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, words)
	return
}

// /api/place/word/insert
type APIPlaceWordInsert struct {
}
func (h *APIPlaceWordInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new PlaceWord struct
	placeword := models.PlaceWord{}
	err := json.NewDecoder(req.Body).Decode(&placeword)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO spot_location 
			(word_id, place_id)
		VALUES(:word_id, :place_id);
	`

	_, err = database.Con.NamedExec(sql, placeword)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Erfolgreich in der Datenbank gespeichert"))
	return
}

// /api/place/word/delete
type APIPlaceWordDelete struct {
}
func (h *APIPlaceWordDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new Word struct
	word := models.Word{}
	err := json.NewDecoder(req.Body).Decode(&word)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM spot_location 
		WHERE word_id = :word_id 
			AND place_id = :entity_id;
	`

	_, err = database.Con.NamedExec(sql, word)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Erfolgreich aus der Datenbank gelöscht!"))
	return
}

// /api/plant
type APIPlant struct {
	APIPlantList   *APIPlantList   // route /api/plant/
	APIPlantSelect *APIPlantSelect // route /api/plant/select/{ID}
	APIPlantInsert *APIPlantInsert // route /api/plant/insert
	APIPlantUpdate *APIPlantUpdate // route /api/plant/update
	APIPlantDelete *APIPlantDelete // route /api/plant/delete
	APIPlantWord   *APIPlantWord   // route /api/plant/word
}
func (h *APIPlant) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	h.APIPlantWord = new(APIPlantWord)

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIPlantList.ServeHTTP(res, req)
	case "select":
		h.APIPlantSelect.ServeHTTP(res, req)
	case "insert":
		h.APIPlantInsert.ServeHTTP(res, req)
	case "update":
		h.APIPlantUpdate.ServeHTTP(res, req)
	case "delete":
		h.APIPlantDelete.ServeHTTP(res, req)
	case "word":
		h.APIPlantWord.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/plant
type APIPlantList struct {
}
func (h *APIPlantList) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	plants := []models.Plant{}

	sql := `
		SELECT 
			p.*,
			pt.plant_type_id plant_type_id,	
			pt.name plant_type_name
		FROM botanic_plant p
			JOIN botanic_plant_type pt
				ON p.plant_type_id = pt.plant_type_id
		ORDER BY p.name;
	`

	err := database.Con.Select(&plants, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, plants)
	return
}

// /api/plant/select/{ID}
type APIPlantSelect struct {
}
func (h *APIPlantSelect) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// get the ID from the URL and check if it is a valid INT
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	plantID, err := strconv.Atoi(head)
	if err != nil {
		wurzel.JSONError(res, fmt.Sprintf("Keine gültige Plant ID: %v", head))
		return
	}

	plant := models.Plant{}

	sql := `
		SELECT 
			p.*,
			pt.plant_type_id plant_type_id,	
			pt.name plant_type_name
		FROM botanic_plant p
			JOIN botanic_plant_type pt
				ON p.plant_type_id = pt.plant_type_id
		WHERE plant_id = :plant_id;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Get(&plant, map[string]interface{}{"plant_id": plantID})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, plant)
	return
}

// /api/plant/insert
type APIPlantInsert struct {
}
func (h *APIPlantInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new plant struct
	plant := models.Plant{}
	err := json.NewDecoder(req.Body).Decode(&plant)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO botanic_plant 
			(name, description, visuellidentifier_id, plant_type_id)
			VALUES(:name, :description, :visuellidentifier_id, :plant_type_id);
	`

	_, err = database.Con.NamedExec(sql, plant)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich in der Datenbank gespeichert", plant.Name))
	return
}

// /api/plant/update
type APIPlantUpdate struct {
}
func (h *APIPlantUpdate) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new plant struct
	plant := models.Plant{}
	err := json.NewDecoder(req.Body).Decode(&plant)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		UPDATE botanic_plant 
		SET		name = :name, 
				description = :description, 
				visuellidentifier_id = :visuellidentifier_id, 
				plant_type_id = :plant_type_id 
		WHERE plant_id = :plant_id;
	`

	_, err = database.Con.NamedExec(sql, plant)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Änderungen an %v erfolgreich in der Datenbank gespeichert", plant.Name))
	return
}

// /api/plant/delete
type APIPlantDelete struct {
}
func (h *APIPlantDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new plant struct
	plant := models.Plant{}
	err := json.NewDecoder(req.Body).Decode(&plant)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM botanic_plant 
		WHERE plant_id = :plant_id;
	`

	_, err = database.Con.NamedExec(sql, plant)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich gelöscht!", plant.Name))
	return
}

// /api/planttype
type APIPlantType struct {
	APIPlantTypeList   *APIPlantTypeList   // route /api/planttype/
	APIPlantTypeSelect *APIPlantTypeSelect // route /api/planttype/select/{ID}
	APIPlantTypeInsert *APIPlantTypeInsert // route /api/planttype/insert
	APIPlantTypeUpdate *APIPlantTypeUpdate // route /api/planttype/update
	APIPlantTypeDelete *APIPlantTypeDelete // route /api/planttype/delete
}
func (h *APIPlantType) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIPlantTypeList.ServeHTTP(res, req)
	case "select":
		h.APIPlantTypeSelect.ServeHTTP(res, req)
	case "insert":
		h.APIPlantTypeInsert.ServeHTTP(res, req)
	case "update":
		h.APIPlantTypeUpdate.ServeHTTP(res, req)
	case "delete":
		h.APIPlantTypeDelete.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/planttype
type APIPlantTypeList struct {
}
func (h *APIPlantTypeList) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	planttypes := []models.PlantType{}

	sql := `
		SELECT *
		FROM botanic_plant_type
		ORDER BY name;
	`

	err := database.Con.Select(&planttypes, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, planttypes)
	return
}

// /api/planttype/select/{ID}
type APIPlantTypeSelect struct {
}
func (h *APIPlantTypeSelect) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// get the ID from the URL and check if it is a valid INT
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	planttypeID, err := strconv.Atoi(head)
	if err != nil {
		wurzel.JSONError(res, fmt.Sprintf("Keine gültige PlantType ID: %v", head))
		return
	}

	planttype := models.PlantType{}

	sql := `
		SELECT * 
		FROM botanic_plant_type
		WHERE plant_type_id = :plant_type_id;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Get(&planttype, map[string]interface{}{"plant_type_id": planttypeID})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, planttype)
	return
}

// /api/planttype/insert
type APIPlantTypeInsert struct {
}
func (h *APIPlantTypeInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new planttype struct
	planttype := models.PlantType{}
	err := json.NewDecoder(req.Body).Decode(&planttype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO botanic_plant_type
			(name, description, visuellidentifier_id)
			VALUES(:name, :description, :visuellidentifier_id);
	`

	_, err = database.Con.NamedExec(sql, planttype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich in der Datenbank gespeichert", planttype.Name))
	return
}

// /api/planttype/update
type APIPlantTypeUpdate struct {
}
func (h *APIPlantTypeUpdate) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new planttype struct
	planttype := models.PlantType{}
	err := json.NewDecoder(req.Body).Decode(&planttype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		UPDATE botanic_plant_type 
		SET		name = :name, 
				description = :description ,
				visuellidentifier_id = :visuellidentifier_id
		WHERE plant_type_id = :plant_type_id;
	`

	_, err = database.Con.NamedExec(sql, planttype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Änderungen an %v erfolgreich in der Datenbank gespeichert", planttype.Name))
	return
}

// /api/planttype/delete
type APIPlantTypeDelete struct {
}
func (h *APIPlantTypeDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new planttype struct
	planttype := models.PlantType{}
	err := json.NewDecoder(req.Body).Decode(&planttype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM botanic_plant_type 
		WHERE plant_type_id = :plant_type_id;
	`

	_, err = database.Con.NamedExec(sql, planttype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich gelöscht!", planttype.Name))
	return
}

// /api/plant/word
type APIPlantWord struct {
	APIPlantWordList   *APIPlantWordList   // route /api/plant/word/
	APIPlantWordInsert *APIPlantWordInsert // route /api/plant/word/insert
	APIPlantWordDelete *APIPlantWordDelete // route /api/plant/word/delete
}
func (h *APIPlantWord) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIPlantWordList.ServeHTTP(res, req)
	case "insert":
		h.APIPlantWordInsert.ServeHTTP(res, req)
	case "delete":
		h.APIPlantWordDelete.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/plant/word/
type APIPlantWordList struct {
}
func (h *APIPlantWordList) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	// get the PlantID as Entityid so we can reuse the word vue component
	// and decode the retrived json
	type entity struct {
		Entityid int
	}
	e := new(entity)

	err := json.NewDecoder(req.Body).Decode(&e)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		SELECT	*, 
				36 AS visuellidentifier_id 
		FROM bible_word_locations 
		WHERE entity_type = 'plant' 
			AND entity_id = :plantID 
		ORDER BY vers_id, position;
	`

	words := []models.Word{}
	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Select(&words, map[string]interface{}{"plantID": e.Entityid})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, words)
	return
}

// /api/plant/word/insert
type APIPlantWordInsert struct {
}
func (h *APIPlantWordInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new PlantWord struct
	plantword := models.PlantWord{}
	err := json.NewDecoder(req.Body).Decode(&plantword)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO botanic_location 
			(word_id, plant_id)
		VALUES(:word_id, :plant_id);
	`

	_, err = database.Con.NamedExec(sql, plantword)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Erfolgreich in der Datenbank gespeichert"))
	return
}

// /api/plant/word/delete
type APIPlantWordDelete struct {
}
func (h *APIPlantWordDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new Word struct
	word := models.Word{}
	err := json.NewDecoder(req.Body).Decode(&word)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM botanic_location 
		WHERE word_id = :word_id 
			AND plant_id = :entity_id;
	`

	_, err = database.Con.NamedExec(sql, word)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Erfolgreich aus der Datenbank gelöscht!"))
	return
}

// /api/snippet
type APISnippet struct {
	APISnippetList       *APISnippetList       // route /api/snippet/
	APISnippetObjectList *APISnippetObjectList // route /api/snippet/object
}
func (h *APISnippet) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	var head string
	head, _ = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APISnippetList.ServeHTTP(res, req)
	case "object":
		h.APISnippetObjectList.ServeHTTP(res, req)
	default:
	}
	return
}

// /api/snippet/
type APISnippetList struct {
}
func (h *APISnippetList) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	snippets := []models.Word{}

	sql := `
		SELECT	*, 
				36 AS visuellidentifier_id 
		FROM bible_word_locations 
		WHERE entity_type = 'word' 
		ORDER BY vers_id, position;
	`
	err := database.Con.Select(&snippets, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, snippets)
	return
}

// /api/snippet/object
type APISnippetObjectList struct {
}
func (h *APISnippetObjectList) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	// get the search query
	type query struct {
		Query string
	}
	q := new(query)

	err := json.NewDecoder(req.Body).Decode(&q)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		SELECT * 
		FROM snippet_object_list
		WHERE name LIKE CONCAT('%',:query,'%');
	`

	objects := []models.SnippetObject{}
	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Select(&objects, q)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, objects)
	return
}

// /api/thing
type APIThing struct {
	APIThingList   *APIThingList   // route /api/thing/
	APIThingSelect *APIThingSelect // route /api/thing/select/{ID}
	APIThingInsert *APIThingInsert // route /api/thing/insert
	APIThingUpdate *APIThingUpdate // route /api/thing/update
	APIThingDelete *APIThingDelete // route /api/thing/delete
	APIThingWord   *APIThingWord   // route /api/thing/word
}
func (h *APIThing) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	h.APIThingWord = new(APIThingWord)

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIThingList.ServeHTTP(res, req)
	case "select":
		h.APIThingSelect.ServeHTTP(res, req)
	case "insert":
		h.APIThingInsert.ServeHTTP(res, req)
	case "update":
		h.APIThingUpdate.ServeHTTP(res, req)
	case "delete":
		h.APIThingDelete.ServeHTTP(res, req)
	case "word":
		h.APIThingWord.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/thing
type APIThingList struct {
}
func (h *APIThingList) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	things := []models.Thing{}

	sql := `
		SELECT 
			t.*,
			tt.thing_type_id thing_type_id,	
			tt.name thing_type_name
		FROM object_thing t
			JOIN object_thing_type tt
				ON t.thing_type_id = tt.thing_type_id
		ORDER BY t.name;
	`

	err := database.Con.Select(&things, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, things)
	return
}

// /api/thing/select/{ID}
type APIThingSelect struct {
}
func (h *APIThingSelect) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// get the ID from the URL and check if it is a valid INT
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	thingID, err := strconv.Atoi(head)
	if err != nil {
		wurzel.JSONError(res, fmt.Sprintf("Keine gültige Thing ID: %v", head))
		return
	}

	thing := models.Thing{}

	sql := `
		SELECT 
			t.*,
			tt.thing_type_id thing_type_id,	
			tt.name thing_type_name
		FROM object_thing t
			JOIN object_thing_type tt
				ON t.thing_type_id = tt.thing_type_id
		WHERE t.thing_id = :thing_id;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Get(&thing, map[string]interface{}{"thing_id": thingID})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, thing)
	return
}

// /api/thing/insert
type APIThingInsert struct {
}
func (h *APIThingInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new thing struct
	thing := models.Thing{}
	err := json.NewDecoder(req.Body).Decode(&thing)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO object_thing 
			(name, description, visuellidentifier_id, thing_type_id)
			VALUES(:name, :description, :visuellidentifier_id, :thing_type_id);
	`

	_, err = database.Con.NamedExec(sql, thing)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich in der Datenbank gespeichert", thing.Name))
	return
}

// /api/thing/update
type APIThingUpdate struct {
}
func (h *APIThingUpdate) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new thing struct
	thing := models.Thing{}
	err := json.NewDecoder(req.Body).Decode(&thing)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		UPDATE object_thing 
		SET		name = :name, 
				description = :description, 
				visuellidentifier_id = :visuellidentifier_id, 
				thing_type_id = :thing_type_id 
		WHERE thing_id = :thing_id;
	`

	_, err = database.Con.NamedExec(sql, thing)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Änderungen an %v erfolgreich in der Datenbank gespeichert", thing.Name))
	return
}

// /api/thing/delete
type APIThingDelete struct {
}
func (h *APIThingDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new thing struct
	thing := models.Thing{}
	err := json.NewDecoder(req.Body).Decode(&thing)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM object_thing 
		WHERE thing_id = :thing_id;
	`

	_, err = database.Con.NamedExec(sql, thing)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich gelöscht!", thing.Name))
	return
}

// /api/thingtype
type APIThingType struct {
	APIThingTypeList   *APIThingTypeList   // route /api/thingtype/
	APIThingTypeSelect *APIThingTypeSelect // route /api/thingtype/select/{ID}
	APIThingTypeInsert *APIThingTypeInsert // route /api/thingtype/insert
	APIThingTypeUpdate *APIThingTypeUpdate // route /api/thingtype/update
	APIThingTypeDelete *APIThingTypeDelete // route /api/thingtype/delete
}
func (h *APIThingType) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIThingTypeList.ServeHTTP(res, req)
	case "select":
		h.APIThingTypeSelect.ServeHTTP(res, req)
	case "insert":
		h.APIThingTypeInsert.ServeHTTP(res, req)
	case "update":
		h.APIThingTypeUpdate.ServeHTTP(res, req)
	case "delete":
		h.APIThingTypeDelete.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/thingtype
type APIThingTypeList struct {
}
func (h *APIThingTypeList) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	thingtypes := []models.ThingType{}

	sql := `
		SELECT *
		FROM object_thing_type
		ORDER BY name;
	`

	err := database.Con.Select(&thingtypes, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, thingtypes)
	return
}

// /api/thingtype/select/{ID}
type APIThingTypeSelect struct {
}
func (h *APIThingTypeSelect) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// get the ID from the URL and check if it is a valid INT
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	thingtypeID, err := strconv.Atoi(head)
	if err != nil {
		wurzel.JSONError(res, fmt.Sprintf("Keine gültige ThingType ID: %v", head))
		return
	}

	thingtype := models.ThingType{}

	sql := `
		SELECT * 
		FROM object_thing_type
		WHERE thing_type_id = :thing_type_id;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Get(&thingtype, map[string]interface{}{"thing_type_id": thingtypeID})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, thingtype)
	return
}

// /api/thingtype/insert
type APIThingTypeInsert struct {
}
func (h *APIThingTypeInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new thingtype struct
	thingtype := models.ThingType{}
	err := json.NewDecoder(req.Body).Decode(&thingtype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO object_thing_type
			(name, description, visuellidentifier_id)
			VALUES(:name, :description, :visuellidentifier_id);
	`

	_, err = database.Con.NamedExec(sql, thingtype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich in der Datenbank gespeichert", thingtype.Name))
	return
}

// /api/thingtype/update
type APIThingTypeUpdate struct {
}
func (h *APIThingTypeUpdate) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new thingtype struct
	thingtype := models.ThingType{}
	err := json.NewDecoder(req.Body).Decode(&thingtype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		UPDATE object_thing_type 
		SET		name = :name, 
				description = :description ,
				visuellidentifier_id = :visuellidentifier_id
		WHERE thing_type_id = :thing_type_id;
	`

	_, err = database.Con.NamedExec(sql, thingtype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Änderungen an %v erfolgreich in der Datenbank gespeichert", thingtype.Name))
	return
}

// /api/thingtype/delete
type APIThingTypeDelete struct {
}
func (h *APIThingTypeDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new thingtype struct
	thingtype := models.ThingType{}
	err := json.NewDecoder(req.Body).Decode(&thingtype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM object_thing_type 
		WHERE thing_type_id = :thing_type_id;
	`

	_, err = database.Con.NamedExec(sql, thingtype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich gelöscht!", thingtype.Name))
	return
}

// /api/thing/word
type APIThingWord struct {
	APIThingWordList   *APIThingWordList   // route /api/thing/word/
	APIThingWordInsert *APIThingWordInsert // route /api/thing/word/insert
	APIThingWordDelete *APIThingWordDelete // route /api/thing/word/delete
}
func (h *APIThingWord) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIThingWordList.ServeHTTP(res, req)
	case "insert":
		h.APIThingWordInsert.ServeHTTP(res, req)
	case "delete":
		h.APIThingWordDelete.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/thing/word/
type APIThingWordList struct {
}
func (h *APIThingWordList) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	// get the ThingID as Entityid so we can reuse the word vue component
	// and decode the retrived json
	type entity struct {
		Entityid int
	}
	e := new(entity)

	err := json.NewDecoder(req.Body).Decode(&e)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		SELECT	*, 
				36 AS visuellidentifier_id 
		FROM bible_word_locations 
		WHERE entity_type = 'thing' 
			AND entity_id = :thingID 
		ORDER BY vers_id, position;
	`

	words := []models.Word{}
	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Select(&words, map[string]interface{}{"thingID": e.Entityid})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, words)
	return
}

// /api/thing/word/insert
type APIThingWordInsert struct {
}
func (h *APIThingWordInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new ThingWord struct
	thingword := models.ThingWord{}
	err := json.NewDecoder(req.Body).Decode(&thingword)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO object_location 
			(word_id, thing_id)
		VALUES(:word_id, :thing_id);
	`

	_, err = database.Con.NamedExec(sql, thingword)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Erfolgreich in der Datenbank gespeichert"))
	return
}

// /api/thing/word/delete
type APIThingWordDelete struct {
}
func (h *APIThingWordDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new Word struct
	word := models.Word{}
	err := json.NewDecoder(req.Body).Decode(&word)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM object_location 
		WHERE word_id = :word_id 
			AND thing_id = :entity_id;
	`

	_, err = database.Con.NamedExec(sql, word)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Erfolgreich aus der Datenbank gelöscht!"))
	return
}

// /api/tribe
type APITribe struct {
	APITribeList   *APITribeList   // route /api/tribe/
	APITribeSelect *APITribeSelect // route /api/tribe/select/{ID}
	APITribeInsert *APITribeInsert // route /api/tribe/insert
	APITribeUpdate *APITribeUpdate // route /api/tribe/update
	APITribeDelete *APITribeDelete // route /api/tribe/delete
	APITribeWord   *APITribeWord   // route /api/tribe/word
}
func (h *APITribe) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	h.APITribeWord = new(APITribeWord)

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APITribeList.ServeHTTP(res, req)
	case "select":
		h.APITribeSelect.ServeHTTP(res, req)
	case "insert":
		h.APITribeInsert.ServeHTTP(res, req)
	case "update":
		h.APITribeUpdate.ServeHTTP(res, req)
	case "delete":
		h.APITribeDelete.ServeHTTP(res, req)
	case "word":
		h.APITribeWord.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/tribe
type APITribeList struct {
}
func (h *APITribeList) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	tribes := []models.Tribe{}

	sql := `
		SELECT
			t.*,
			22 AS visuellidentifier_id	
		FROM clan_tribe t
		ORDER BY t.name;
	`

	err := database.Con.Select(&tribes, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, tribes)
	return
}

// /api/tribe/select/{ID}
type APITribeSelect struct {
}
func (h *APITribeSelect) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// get the ID from the URL and check if it is a valid INT
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	tribeID, err := strconv.Atoi(head)
	if err != nil {
		wurzel.JSONError(res, fmt.Sprintf("Keine gültige Tribe ID: %v", head))
		return
	}

	tribe := models.Tribe{}

	sql := `
		SELECT
			t.*,
			22 AS visuellidentifier_id	
		FROM clan_tribe t
		WHERE tribe_id = :tribe_id;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Get(&tribe, map[string]interface{}{"tribe_id": tribeID})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, tribe)
	return
}

// /api/tribe/insert
type APITribeInsert struct {
}
func (h *APITribeInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new tribe struct
	tribe := models.Tribe{}
	err := json.NewDecoder(req.Body).Decode(&tribe)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO clan_tribe 
			(name, description)
			VALUES(:name, :description);
	`

	_, err = database.Con.NamedExec(sql, tribe)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich in der Datenbank gespeichert", tribe.Name))
	return
}

// /api/tribe/update
type APITribeUpdate struct {
}
func (h *APITribeUpdate) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new tribe struct
	tribe := models.Tribe{}
	err := json.NewDecoder(req.Body).Decode(&tribe)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		UPDATE clan_tribe 
		SET		name = :name, 
				description = :description 
		WHERE tribe_id = :tribe_id;
	`

	_, err = database.Con.NamedExec(sql, tribe)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Änderungen an %v erfolgreich in der Datenbank gespeichert", tribe.Name))
	return
}

// /api/tribe/delete
type APITribeDelete struct {
}
func (h *APITribeDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new tribe struct
	tribe := models.Tribe{}
	err := json.NewDecoder(req.Body).Decode(&tribe)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM clan_tribe 
		WHERE tribe_id = :tribe_id;
	`

	_, err = database.Con.NamedExec(sql, tribe)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich gelöscht!", tribe.Name))
	return
}

// /api/tribe/word
type APITribeWord struct {
	APITribeWordList   *APITribeWordList   // route /api/tribe/word/
	APITribeWordInsert *APITribeWordInsert // route /api/tribe/word/insert
	APITribeWordDelete *APITribeWordDelete // route /api/tribe/word/delete
}
func (h *APITribeWord) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APITribeWordList.ServeHTTP(res, req)
	case "insert":
		h.APITribeWordInsert.ServeHTTP(res, req)
	case "delete":
		h.APITribeWordDelete.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/tribe/word/
type APITribeWordList struct {
}
func (h *APITribeWordList) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	// get the TribeID as Entityid so we can reuse the word vue component
	// and decode the retrived json
	type entity struct {
		Entityid int
	}
	e := new(entity)

	err := json.NewDecoder(req.Body).Decode(&e)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		SELECT	*, 
				36 AS visuellidentifier_id 
		FROM bible_word_locations 
		WHERE entity_type = 'tribe' 
			AND entity_id = :tribeID 
		ORDER BY vers_id, position;
	`

	words := []models.Word{}
	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Select(&words, map[string]interface{}{"tribeID": e.Entityid})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, words)
	return
}

// /api/tribe/word/insert
type APITribeWordInsert struct {
}
func (h *APITribeWordInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new TribeWord struct
	tribeword := models.TribeWord{}
	err := json.NewDecoder(req.Body).Decode(&tribeword)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO clan_location 
			(word_id, tribe_id)
		VALUES(:word_id, :tribe_id);
	`

	_, err = database.Con.NamedExec(sql, tribeword)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Erfolgreich in der Datenbank gespeichert"))
	return
}

// /api/tribe/word/delete
type APITribeWordDelete struct {
}
func (h *APITribeWordDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new Word struct
	word := models.Word{}
	err := json.NewDecoder(req.Body).Decode(&word)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM clan_location 
		WHERE word_id = :word_id 
			AND tribe_id = :entity_id;
	`

	_, err = database.Con.NamedExec(sql, word)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Erfolgreich aus der Datenbank gelöscht!"))
	return
}

// /api/unit
type APIUnit struct {
	APIUnitList   *APIUnitList   // route /api/unit/
	APIUnitSelect *APIUnitSelect // route /api/unit/select/{ID}
	APIUnitInsert *APIUnitInsert // route /api/unit/insert
	APIUnitUpdate *APIUnitUpdate // route /api/unit/update
	APIUnitDelete *APIUnitDelete // route /api/unit/delete
	APIUnitWord   *APIUnitWord   // route /api/unit/word
}
func (h *APIUnit) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	h.APIUnitWord = new(APIUnitWord)

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIUnitList.ServeHTTP(res, req)
	case "select":
		h.APIUnitSelect.ServeHTTP(res, req)
	case "insert":
		h.APIUnitInsert.ServeHTTP(res, req)
	case "update":
		h.APIUnitUpdate.ServeHTTP(res, req)
	case "delete":
		h.APIUnitDelete.ServeHTTP(res, req)
	case "word":
		h.APIUnitWord.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/unit
type APIUnitList struct {
}
func (h *APIUnitList) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	units := []models.Unit{}

	sql := `
		SELECT 
			u.*,
			ut.unit_type_id unit_type_id,	
			ut.name unit_type_name
		FROM measurement_unit u
			JOIN measurement_unit_type ut
				ON u.unit_type_id = ut.unit_type_id
		ORDER BY u.name;
	`

	err := database.Con.Select(&units, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, units)
	return
}

// /api/unit/select/{ID}
type APIUnitSelect struct {
}
func (h *APIUnitSelect) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// get the ID from the URL and check if it is a valid INT
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	unitID, err := strconv.Atoi(head)
	if err != nil {
		wurzel.JSONError(res, fmt.Sprintf("Keine gültige Unit ID: %v", head))
		return
	}

	unit := models.Unit{}

	sql := `
		SELECT 
			u.*,
			ut.unit_type_id unit_type_id,	
			ut.name unit_type_name
		FROM measurement_unit u
			JOIN measurement_unit_type ut
				ON u.unit_type_id = ut.unit_type_id
		WHERE u.unit_id = :unit_id;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Get(&unit, map[string]interface{}{"unit_id": unitID})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, unit)
	return
}

// /api/unit/insert
type APIUnitInsert struct {
}
func (h *APIUnitInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new unit struct
	unit := models.Unit{}
	err := json.NewDecoder(req.Body).Decode(&unit)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO measurement_unit 
			(name, description, visuellidentifier_id, unit_type_id)
			VALUES(:name, :description, :visuellidentifier_id, :unit_type_id);
	`

	_, err = database.Con.NamedExec(sql, unit)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich in der Datenbank gespeichert", unit.Name))
	return
}

// /api/unit/update
type APIUnitUpdate struct {
}
func (h *APIUnitUpdate) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new unit struct
	unit := models.Unit{}
	err := json.NewDecoder(req.Body).Decode(&unit)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		UPDATE measurement_unit 
		SET		name = :name, 
				description = :description, 
				visuellidentifier_id = :visuellidentifier_id, 
				unit_type_id = :unit_type_id 
		WHERE unit_id = :unit_id;
	`

	_, err = database.Con.NamedExec(sql, unit)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Änderungen an %v erfolgreich in der Datenbank gespeichert", unit.Name))
	return
}

// /api/unit/delete
type APIUnitDelete struct {
}
func (h *APIUnitDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new unit struct
	unit := models.Unit{}
	err := json.NewDecoder(req.Body).Decode(&unit)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM measurement_unit 
		WHERE unit_id = :unit_id;
	`

	_, err = database.Con.NamedExec(sql, unit)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich gelöscht!", unit.Name))
	return
}

// /api/unittype
type APIUnitType struct {
	APIUnitTypeList   *APIUnitTypeList   // route /api/unittype/
	APIUnitTypeSelect *APIUnitTypeSelect // route /api/unittype/select/{ID}
	APIUnitTypeInsert *APIUnitTypeInsert // route /api/unittype/insert
	APIUnitTypeUpdate *APIUnitTypeUpdate // route /api/unittype/update
	APIUnitTypeDelete *APIUnitTypeDelete // route /api/unittype/delete
}
func (h *APIUnitType) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIUnitTypeList.ServeHTTP(res, req)
	case "select":
		h.APIUnitTypeSelect.ServeHTTP(res, req)
	case "insert":
		h.APIUnitTypeInsert.ServeHTTP(res, req)
	case "update":
		h.APIUnitTypeUpdate.ServeHTTP(res, req)
	case "delete":
		h.APIUnitTypeDelete.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/unittype
type APIUnitTypeList struct {
}
func (h *APIUnitTypeList) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	unittypes := []models.UnitType{}

	sql := `
		SELECT *
		FROM measurement_unit_type
		ORDER BY name;
	`

	err := database.Con.Select(&unittypes, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, unittypes)
	return
}

// /api/unittype/select/{ID}
type APIUnitTypeSelect struct {
}
func (h *APIUnitTypeSelect) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// get the ID from the URL and check if it is a valid INT
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	unittypeID, err := strconv.Atoi(head)
	if err != nil {
		wurzel.JSONError(res, fmt.Sprintf("Keine gültige UnitType ID: %v", head))
		return
	}

	unittype := models.UnitType{}

	sql := `
		SELECT * 
		FROM measurement_unit_type
		WHERE unit_type_id = :unit_type_id;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Get(&unittype, map[string]interface{}{"unit_type_id": unittypeID})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, unittype)
	return
}

// /api/unittype/insert
type APIUnitTypeInsert struct {
}
func (h *APIUnitTypeInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new unittype struct
	unittype := models.UnitType{}
	err := json.NewDecoder(req.Body).Decode(&unittype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO measurement_unit_type
			(name, description, visuellidentifier_id)
			VALUES(:name, :description, :visuellidentifier_id);
	`

	_, err = database.Con.NamedExec(sql, unittype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich in der Datenbank gespeichert", unittype.Name))
	return
}

// /api/unittype/update
type APIUnitTypeUpdate struct {
}
func (h *APIUnitTypeUpdate) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new unittype struct
	unittype := models.UnitType{}
	err := json.NewDecoder(req.Body).Decode(&unittype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		UPDATE measurement_unit_type 
		SET		name = :name, 
				description = :description ,
				visuellidentifier_id = :visuellidentifier_id
		WHERE unit_type_id = :unit_type_id;
	`

	_, err = database.Con.NamedExec(sql, unittype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Änderungen an %v erfolgreich in der Datenbank gespeichert", unittype.Name))
	return
}

// /api/unittype/delete
type APIUnitTypeDelete struct {
}
func (h *APIUnitTypeDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new unittype struct
	unittype := models.UnitType{}
	err := json.NewDecoder(req.Body).Decode(&unittype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM measurement_unit_type 
		WHERE unit_type_id = :unit_type_id;
	`

	_, err = database.Con.NamedExec(sql, unittype)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich gelöscht!", unittype.Name))
	return
}

// /api/unit/word
type APIUnitWord struct {
	APIUnitWordList   *APIUnitWordList   // route /api/unit/word/
	APIUnitWordInsert *APIUnitWordInsert // route /api/unit/word/insert
	APIUnitWordDelete *APIUnitWordDelete // route /api/unit/word/delete
}
func (h *APIUnitWord) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIUnitWordList.ServeHTTP(res, req)
	case "insert":
		h.APIUnitWordInsert.ServeHTTP(res, req)
	case "delete":
		h.APIUnitWordDelete.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/unit/word/
type APIUnitWordList struct {
}
func (h *APIUnitWordList) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	// get the UnitID as Entityid so we can reuse the word vue component
	// and decode the retrived json
	type entity struct {
		Entityid int
	}
	e := new(entity)

	err := json.NewDecoder(req.Body).Decode(&e)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		SELECT	*, 
				36 AS visuellidentifier_id 
		FROM bible_word_locations 
		WHERE entity_type = 'unit' 
			AND entity_id = :unitID 
		ORDER BY vers_id, position;
	`

	words := []models.Word{}
	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Select(&words, map[string]interface{}{"unitID": e.Entityid})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, words)
	return
}

// /api/unit/word/insert
type APIUnitWordInsert struct {
}
func (h *APIUnitWordInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new UnitWord struct
	unitword := models.UnitWord{}
	err := json.NewDecoder(req.Body).Decode(&unitword)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO measurement_location 
			(word_id, unit_id)
		VALUES(:word_id, :unit_id);
	`

	_, err = database.Con.NamedExec(sql, unitword)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Erfolgreich in der Datenbank gespeichert"))
	return
}

// /api/unit/word/delete
type APIUnitWordDelete struct {
}
func (h *APIUnitWordDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new Word struct
	word := models.Word{}
	err := json.NewDecoder(req.Body).Decode(&word)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM measurement_location 
		WHERE word_id = :word_id 
			AND unit_id = :entity_id;
	`

	_, err = database.Con.NamedExec(sql, word)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Erfolgreich aus der Datenbank gelöscht!"))
	return
}

// /api/visuellidentifier
type APIVisuellidentifier struct {
	APIVisuellidentifierList   *APIVisuellidentifierList   // route /api/visuellidentifier/
	APIVisuellidentifierSelect *APIVisuellidentifierSelect // route /api/visuellidentifier/select/{ID}
	APIVisuellidentifierInsert *APIVisuellidentifierInsert // route /api/visuellidentifier/insert
	APIVisuellidentifierUpdate *APIVisuellidentifierUpdate // route /api/visuellidentifier/update
	APIVisuellidentifierDelete *APIVisuellidentifierDelete // route /api/visuellidentifier/delete
}
func (h *APIVisuellidentifier) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIVisuellidentifierList.ServeHTTP(res, req)
	case "select":
		h.APIVisuellidentifierSelect.ServeHTTP(res, req)
	case "insert":
		h.APIVisuellidentifierInsert.ServeHTTP(res, req)
	case "update":
		h.APIVisuellidentifierUpdate.ServeHTTP(res, req)
	case "delete":
		h.APIVisuellidentifierDelete.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/visuellidentifier
type APIVisuellidentifierList struct {
}
func (h *APIVisuellidentifierList) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	visuellidentifiers := []models.Visuellidentifier{}

	sql := `
		SELECT *
		FROM st_visuellidentifier
		ORDER BY system_name;
	`

	err := database.Con.Select(&visuellidentifiers, sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONContent(res, visuellidentifiers)
	return
}

// /api/visuellidentifier/select/{ID}
type APIVisuellidentifierSelect struct {
}
func (h *APIVisuellidentifierSelect) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// get the ID from the URL and check if it is a valid INT
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	visuellidentifierID, err := strconv.Atoi(head)
	if err != nil {
		wurzel.JSONError(res, fmt.Sprintf("Keine gültige Visuellidentifier ID: %v", head))
		return
	}

	visuellidentifier := models.Visuellidentifier{}

	sql := `
		SELECT * 
		FROM st_visuellidentifier
		WHERE visuellidentifier_id = :visuellidentifier_id;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Get(&visuellidentifier, map[string]interface{}{"visuellidentifier_id": visuellidentifierID})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, visuellidentifier)
	return
}

// /api/visuellidentifier/insert
type APIVisuellidentifierInsert struct {
}
func (h *APIVisuellidentifierInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new visuellidentifier struct
	visuellidentifier := models.Visuellidentifier{}
	err := json.NewDecoder(req.Body).Decode(&visuellidentifier)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO st_visuellidentifier 
			(sysname,, license, artist, description, url)
			VALUES(:system_name, :license, :artist, :description, :url);
	`

	_, err = database.Con.NamedExec(sql, visuellidentifier)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich in der Datenbank gespeichert", visuellidentifier.SystemName))
	return
}

// /api/visuellidentifier/update
type APIVisuellidentifierUpdate struct {
}
func (h *APIVisuellidentifierUpdate) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new visuellidentifier struct
	visuellidentifier := models.Visuellidentifier{}
	err := json.NewDecoder(req.Body).Decode(&visuellidentifier)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		UPDATE st_visuellidentifier 
		SET		system_name = :system_name, 
				license = :license, 
				artist = :artist, 
				description = :description, 
				url = :url 
		WHERE visuellidentifier_id = :visuellidentifier_id;
	`

	_, err = database.Con.NamedExec(sql, visuellidentifier)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Änderungen an %v erfolgreich in der Datenbank gespeichert", visuellidentifier.SystemName))
	return
}

// /api/visuellidentifier/delete
type APIVisuellidentifierDelete struct {
}
func (h *APIVisuellidentifierDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object into a new visuellidentifier struct
	visuellidentifier := models.Visuellidentifier{}
	err := json.NewDecoder(req.Body).Decode(&visuellidentifier)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM st_visuellidentifier 
		WHERE visuellidentifier_id = :visuellidentifier_id;
	`

	_, err = database.Con.NamedExec(sql, visuellidentifier)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("%v erfolgreich gelöscht!", visuellidentifier.SystemName))
	return
}

// /api/word
type APIWord struct {
	APIWordList   *APIWordList   // route /api/word/
	APIWordInsert *APIWordInsert // route /api/word/insert
	APIWordSelect *APIWordSelect // route /api/word/select
	APIWordDelete *APIWordDelete // route /api/word/delete
}
func (h *APIWord) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	var head string
	head, _ = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		h.APIWordList.ServeHTTP(res, req)
	case "select":
		h.APIWordSelect.ServeHTTP(res, req)
	case "insert":
		h.APIWordInsert.ServeHTTP(res, req)
	case "delete":
		h.APIWordDelete.ServeHTTP(res, req)
	default:
		http.Error(res, "API Not Found", http.StatusNotFound)
	}
	return
}

// /api/word
type APIWordList struct {
}
func (h *APIWordList) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object which is provided by the client
	type data struct {
		BookID  int
		Chapter int
	}

	d := new(data)
	err := json.NewDecoder(req.Body).Decode(&d)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	words := []models.Word{}

	sql := `
		SELECT * 
		FROM bible_word_locations 
		WHERE book_id = :BookID 
			AND chapter = :Chapter 
		ORDER BY chapter, vers_id, position;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Select(&words, map[string]interface{}{"BookID": d.BookID, "Chapter": d.Chapter})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, words)
	return
}

// /api/word/select
type APIWordSelect struct {
}
func (h *APIWordSelect) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object which is provided by the client
	type data struct {
		VersID int
	}

	d := new(data)
	err := json.NewDecoder(req.Body).Decode(&d)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	words := []models.Word{}

	sql := `
		SELECT * 
		FROM bible_word_locations 
		WHERE vers_id = :VersID 
		ORDER BY chapter, vers_id, position;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	err = stmt.Select(&words, map[string]interface{}{"VersID": d.VersID})
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}
	defer stmt.Close()

	wurzel.JSONContent(res, words)
	return
}

// /api/word/insert
type APIWordInsert struct {
}
func (h *APIWordInsert) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object which is provided by the client
	type data struct {
		VersID   int `db:"vers_id"`
		Position int `db:"position"`
		Length   int `db:"length"`
	}

	d := new(data)

	err := json.NewDecoder(req.Body).Decode(&d)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		INSERT INTO bible_word 
			(vers_id, position, length)
			VALUES(:vers_id, :position, :length);
	`

	_, err = database.Con.NamedExec(sql, d)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Erfolgreich in der Datenbank gespeichert"))
	return
}

// /api/word/delete
type APIWordDelete struct {
}
func (h *APIWordDelete) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Decode JSON object which is provided by the client
	type data struct {
		WordID int `db:"word_id"`
	}

	d := new(data)

	err := json.NewDecoder(req.Body).Decode(&d)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Empfangene Daten fehlerhaft.")
		return
	}

	sql := `
		DELETE FROM bible_word 
		WHERE word_id = :word_id
	`

	_, err = database.Con.NamedExec(sql, d)
	if err != nil {
		log.Print(err)
		wurzel.JSONError(res, "Datenbankfehler")
		return
	}

	wurzel.JSONSuccess(res, fmt.Sprintf("Erfolgreich aus der Datenbank gelöscht!"))
	return
}
