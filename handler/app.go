package handler

import (
	"net/http"
	"fmt"
	"log"
	"strconv"

	"gitlab.com/michaelkrnac/root/router"
	"gitlab.com/michaelkrnac/root/wurzel"
	"gitlab.com/michaelkrnac/root/database"
	"gitlab.com/michaelkrnac/root/models"
)

// /
type APP struct {
	APPHome       *APPHome       // route / --> Homepage
	Static        *Static        // route /static --> Static files like css and js
	API           *API           // route /api --> json api for ajax requests
	APPUser       *APPUser       // route /user --> user pages
	APPPerson     *APPPerson     // route /person --> gui for person
	APPAnimal     *APPAnimal     // route /animal --> gui for animal
	APPPlace      *APPPlace      // route /place --> gui for place
	APPTribe      *APPTribe      // route /place --> gui for tribe
	APPPlant      *APPPlant      // route /plant --> gui for plant
	APPMaterial   *APPMaterial   // route /material --> gui for material
	APPInstrument *APPInstrument // route /instrument --> gui for instrument
	APPUnit       *APPUnit       // route /unit --> gui for unit
	APPThing      *APPThing      // route /thing --> gui for thing
	APPBible      *APPBible      // route /bible --> gui for bible
	APPMaps       *APPMaps       // route /maps --> gui for maps
}
func (h *APP) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	h.APPHome = new(APPHome)
	h.Static = new(Static)
	h.API = new(API)
	h.APPUser = new(APPUser)
	h.APPPerson = new(APPPerson)
	h.APPAnimal = new(APPAnimal)
	h.APPPlace = new(APPPlace)
	h.APPTribe = new(APPTribe)
	h.APPPlant = new(APPPlant)
	h.APPMaterial = new(APPMaterial)
	h.APPInstrument = new(APPInstrument)
	h.APPUnit = new(APPUnit)
	h.APPThing = new(APPThing)
	h.APPBible = new(APPBible)
	h.APPMaps = new(APPMaps)

	ctx := wurzel.NewContext("Michael")
	ctx.AppendBreadcrumb("Wurzel", "/")

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)

	switch head {
	case "":
		h.APPHome.ServeHTTP(res, req, ctx)
	case "static":
		h.Static.ServeHTTP(res, req)
	case "api":
		h.API.ServeHTTP(res, req)
	case "user":
		h.APPUser.ServeHTTP(res, req)
	case "person":
		h.APPPerson.ServeHTTP(res, req, ctx)
	case "animal":
		h.APPAnimal.ServeHTTP(res, req, ctx)
	case "place":
		h.APPPlace.ServeHTTP(res, req, ctx)
	case "tribe":
		h.APPTribe.ServeHTTP(res, req, ctx)
	case "plant":
		h.APPPlant.ServeHTTP(res, req, ctx)
	case "material":
		h.APPMaterial.ServeHTTP(res, req, ctx)
	case "instrument":
		h.APPInstrument.ServeHTTP(res, req, ctx)
	case "unit":
		h.APPUnit.ServeHTTP(res, req, ctx)
	case "thing":
		h.APPThing.ServeHTTP(res, req, ctx)
	case "bible":
		h.APPBible.ServeHTTP(res, req, ctx)
	case "maps":
		h.APPMaps.ServeHTTP(res, req, ctx)
	default:
		http.Error(res, "Not Found", http.StatusNotFound)
	}
	return
}

// //
type APPHome struct {
}
func (h *APPHome) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	data := map[string]interface{}{
		"ctx": ctx,
	}

	wurzel.RenderTemplate(data, res, "home/home.html")
}

// /animal
type APPAnimal struct {
	APPAnimalList   *APPAnimalList   // route /animal/
	APPAnimalDetail *APPAnimalDetail // route /animal/{ID}
	APPAnimalCreate *APPAnimalCreate // route /animal/create
}
func (h *APPAnimal) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	ctx.AppendBreadcrumb("Tiere", "/animal")

	var head string
	head, _ = router.ShiftPath(req.URL.Path)

	switch head {
	case "":
		h.APPAnimalList.ServeHTTP(res, req, ctx)
	case "create":
		h.APPAnimalCreate.ServeHTTP(res, req, ctx)
	default:
		h.APPAnimalDetail.ServeHTTP(res, req, ctx)
	}
	return
}

// /animal/
type APPAnimalList struct {
}
func (h *APPAnimalList) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	data := map[string]interface{}{
		"ctx": ctx,
	}

	wurzel.RenderTemplate(data, res, "animal/list.html")
}

// /animal/create
type APPAnimalCreate struct {
}
func (h *APPAnimalCreate) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {
	animal := models.Animal{}

	ctx.AppendBreadcrumb("Neues Tier Anlegen", "/animal/create")

	data := map[string]interface{}{
		"ctx":    ctx,
		"animal": animal,
	}

	wurzel.RenderTemplate(data, res, "animal/create.html", "animal/form.html")
}

// /animal/{ID}
type APPAnimalDetail struct {
}
func (h *APPAnimalDetail) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	animalID, err := strconv.Atoi(head)
	if err != nil {
		http.Error(res, fmt.Sprintf("Invalid animal id %q", head), http.StatusBadRequest)
		return
	}

	animal := models.Animal{}

	sql := `
		SELECT 
			a.*,
			c.name class_name	
		FROM brute_animal a
			JOIN brute_class c
				ON c.class_id = a.class_id
		WHERE animal_id = :AnimalID;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	err = stmt.Get(&animal, map[string]interface{}{"AnimalID": animalID})
	if err != nil {
		log.Print(err)
	}
	defer stmt.Close()

	ctx.AppendBreadcrumb(animal.Name, "/animal/"+strconv.Itoa(animalID))

	data := map[string]interface{}{
		"ctx":    ctx,
		"animal": animal,
	}

	wurzel.RenderTemplate(data, res, "animal/form.html", "animal/detail.html")
}

// /bible
type APPBible struct {
	APPBibleList  *APPBibleList  // route /bible/
	APPBook       *APPBook       // route /bible/book
	APPCollection *APPCollection // route /bible/collection
	APPSnippet    *APPSnippet    // route /bible/snippet
	APPSnippetNG  *APPSnippetNG  // route /bible/snippet
}
func (h *APPBible) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {
	h.APPBook = new(APPBook)
	h.APPCollection = new(APPCollection)
	h.APPSnippet = new(APPSnippet)
	h.APPSnippetNG = new(APPSnippetNG)

	ctx.AppendBreadcrumb("Bibel", "/bible")

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)

	switch head {
	case "":
		h.APPBibleList.ServeHTTP(res, req, ctx)
	case "book":
		h.APPBook.ServeHTTP(res, req, ctx)
	case "collection":
		h.APPCollection.ServeHTTP(res, req, ctx)
	case "snippet":
		h.APPSnippet.ServeHTTP(res, req, ctx)
	case "snippetng":
		h.APPSnippetNG.ServeHTTP(res, req, ctx)
	default:
	}
	return
}

// /bible/
type APPBibleList struct {
}
func (h *APPBibleList) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	data := map[string]interface{}{
		"ctx": ctx,
	}
	wurzel.RenderTemplate(data, res, "bible/list.html")
	return
}

// /bible/book
type APPBook struct {
	APPBookList   *APPBookList   // route /bible/book/
	APPBookDetail *APPBookDetail // route /bible/book/{ID}
}
func (h *APPBook) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {
	h.APPBookDetail = new(APPBookDetail)

	ctx.AppendBreadcrumb("Bücher", "/bible/book")

	var head string
	head, _ = router.ShiftPath(req.URL.Path)

	switch head {
	case "":
		h.APPBookList.ServeHTTP(res, req, ctx)
	default:
		h.APPBookDetail.ServeHTTP(res, req, ctx)
	}
}

// /bible/book/
type APPBookList struct {
}
func (h *APPBookList) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	data := map[string]interface{}{
		"ctx": ctx,
	}
	wurzel.RenderTemplate(data, res, "book/list.html")
}

// /bible/book/{ID}
type APPBookDetail struct {
	APPChapter *APPChapter
}
func (h *APPBookDetail) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	bookID, err := strconv.Atoi(head)
	if err != nil {
		http.Error(res, fmt.Sprintf("Invalid book id %q", head), http.StatusBadRequest)
		return
	}

	book := models.Book{}

	sql := `
		SELECT	book_id, 
				testament, 
				name, 
				name_alternative, 
				name_short, 
				name_long, 
				chapter_count, 
				34 AS visuellidentifier_id 
		FROM bible_book_full 
		WHERE book_id = :BookID;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	if err != nil {
		log.Print(err)
	}
	err = stmt.Get(&book, map[string]interface{}{"BookID": bookID})
	if err != nil {
		log.Print(err)
	}

	defer stmt.Close()

	ctx.AppendBreadcrumb(*book.Name, "/bible/book/"+strconv.Itoa(bookID))

	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	switch head {
	case "":
		var chapters []int
		c, _ := strconv.Atoi(*book.ChapterCount)
		for i := 1; i <= c; i++ {
			chapters = append(chapters, i)
		}

		data := map[string]interface{}{
			"ctx":      ctx,
			"book":     book,
			"chapters": chapters,
		}

		wurzel.RenderTemplate(data, res, "book/detail.html")
	case "chapter":
		h.APPChapter.ServeHTTP(res, req, ctx, book)
	default:
	}
}

// /bible/book/{ID}/chapter/{ID}
type APPChapter struct {
}
func (h *APPChapter) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context, book models.Book) {

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	chapterID, err := strconv.Atoi(head)
	if err != nil {
		http.Error(res, fmt.Sprintf("Invalid chapter id %q", head), http.StatusBadRequest)
		return
	}
	ctx.AppendBreadcrumb("Kapitel "+strconv.Itoa(chapterID), "/bible/book//chapter/"+strconv.Itoa(chapterID))

	verses := []models.Vers{}

	sql := `
		SELECT	vers_id, 
				book_id, 
				chapter, 
				number, 
				words 
		FROM bible_vers 
		WHERE book_id = :BookID 
			AND chapter = :Chapter;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	err = stmt.Select(&verses, map[string]interface{}{"BookID": book.BookID, "Chapter": chapterID})
	if err != nil {
		log.Print(err)
	}

	defer stmt.Close()

	chapterCount, _ := strconv.Atoi(*book.ChapterCount)
	var chapters []int
	for i := 1; i <= chapterCount; i++ {
		chapters = append(chapters, i)
	}

	data := map[string]interface{}{
		"ctx":          ctx,
		"book":         book,
		"chapter":      chapterID,
		"chapterCount": chapterCount,
		"prevchapter":  chapterID - 1,
		"nextchapter":  chapterID + 1,
		"verses":       verses,
		"chapters":		chapters,
	}
	wurzel.RenderTemplate(data, res, "chapter/detail.html")
}

// /bible/collection
type APPCollection struct {
	APPCollectionDetail *APPCollectionDetail
	APPCollectionList   *APPCollectionList
}
func (h *APPCollection) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	ctx.AppendBreadcrumb("Sammlungen", "/bible/collection")

	var head string
	head, _ = router.ShiftPath(req.URL.Path)

	switch head {
	case "":
		h.APPCollectionList.ServeHTTP(res, req, ctx)
	default:
		h.APPCollectionDetail.ServeHTTP(res, req, ctx)
	}
}

// /bible/collection/
type APPCollectionList struct {
}
func (h *APPCollectionList) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	data := map[string]interface{}{
		"ctx": ctx,
	}
	wurzel.RenderTemplate(data, res, "collection/list.html")
}

// /bible/collection/{ID}
type APPCollectionDetail struct {
}
func (h *APPCollectionDetail) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	collectionID, err := strconv.Atoi(head)
	if err != nil {
		http.Error(res, fmt.Sprintf("Invalid collection id %q", head), http.StatusBadRequest)
		return
	}

	collection := models.Collection{}

	sql := `
		SELECT	collection_id, 
				name, 
				name_long, 
				35 AS visuellidentifier_id 
		FROM bible_collection 
		WHERE collection_id = :CollectionID;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	err = stmt.Get(&collection, map[string]interface{}{"CollectionID": collectionID})
	if err != nil {
		log.Print(err)
	}

	defer stmt.Close()

	ctx.AppendBreadcrumb(*collection.Name, "/bible/collection/"+strconv.Itoa(collectionID))

	data := map[string]interface{}{
		"ctx":        ctx,
		"collection": collection,
	}

	wurzel.RenderTemplate(data, res, "collection/detail.html")
}

// /instrument
type APPInstrument struct {
	APPInstrumentList   *APPInstrumentList   // route /instrument/
	APPInstrumentDetail *APPInstrumentDetail // route /instrument/{ID}
	APPInstrumentCreate *APPInstrumentCreate // route /instrument/create
}
func (h *APPInstrument) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	ctx.AppendBreadcrumb("Instrumente", "/instrument")

	var head string
	head, _ = router.ShiftPath(req.URL.Path)

	switch head {
	case "":
		h.APPInstrumentList.ServeHTTP(res, req, ctx)
	case "create":
		h.APPInstrumentCreate.ServeHTTP(res, req, ctx)
	default:
		h.APPInstrumentDetail.ServeHTTP(res, req, ctx)
	}
	return
}

// /instrument/
type APPInstrumentList struct {
}
func (h *APPInstrumentList) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	data := map[string]interface{}{
		"ctx": ctx,
	}

	wurzel.RenderTemplate(data, res, "instrument/list.html")
}

// /instrument/create
type APPInstrumentCreate struct {
}
func (h *APPInstrumentCreate) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {
	instrument := models.Instrument{}

	ctx.AppendBreadcrumb("Neues Instrument Anlegen", "/instrument/create")

	data := map[string]interface{}{
		"ctx":        ctx,
		"instrument": instrument,
	}

	wurzel.RenderTemplate(data, res, "instrument/create.html", "instrument/form.html")
}

// /instrument/{ID}
type APPInstrumentDetail struct {
}
func (h *APPInstrumentDetail) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	instrumentID, err := strconv.Atoi(head)
	if err != nil {
		http.Error(res, fmt.Sprintf("Invalid instrument id %q", head), http.StatusBadRequest)
		return
	}

	instrument := models.Instrument{}

	sql := `
		SELECT 
			i.*,
			it.instrument_type_id instrument_type_id,	
			it.name instrument_type_name
		FROM music_instrument i
			JOIN music_instrument_type it
				ON i.instrument_type_id = it.instrument_type_id
		WHERE i.instrument_id = :InstrumentID;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	err = stmt.Get(&instrument, map[string]interface{}{"InstrumentID": instrumentID})
	if err != nil {
		log.Print(err)
	}
	defer stmt.Close()

	ctx.AppendBreadcrumb(instrument.Name, "/instrument/"+strconv.Itoa(instrumentID))

	data := map[string]interface{}{
		"ctx":        ctx,
		"instrument": instrument,
	}

	wurzel.RenderTemplate(data, res, "instrument/form.html", "instrument/detail.html")
}

// /maps
type APPMaps struct {
	APPMapsList       *APPMapsList       // route /maps/
	APPMapsAllPlaces  *APPMapsAllPlaces  // route /maps/allplaces
	APPMapsFamilyTree *APPMapsFamilyTree // route /maps/familytree
}
func (h *APPMaps) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {
	h.APPMapsList = new(APPMapsList)
	h.APPMapsAllPlaces = new(APPMapsAllPlaces)
	h.APPMapsFamilyTree = new(APPMapsFamilyTree)

	ctx.AppendBreadcrumb("Karten", "/maps")

	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)

	switch head {
	case "":
		h.APPMapsList.ServeHTTP(res, req, ctx)
	case "allplaces":
		h.APPMapsAllPlaces.ServeHTTP(res, req, ctx)
	case "familytree":
		h.APPMapsFamilyTree.ServeHTTP(res, req, ctx)
	default:
	}
	return
}

// /maps/
type APPMapsList struct {
}
func (h *APPMapsList) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	data := map[string]interface{}{
		"ctx": ctx,
	}

	wurzel.RenderTemplate(data, res, "maps/list.html")
}

// /maps/allplaces
type APPMapsAllPlaces struct {
}
func (h *APPMapsAllPlaces) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	ctx.AppendBreadcrumb("Karte mit allen Orten", "/maps/allplaces")

	data := map[string]interface{}{
		"ctx": ctx,
	}

	wurzel.RenderTemplate(data, res, "maps/allplaces.html")
}

// /maps/familytree
type APPMapsFamilyTree struct {
}
func (h *APPMapsFamilyTree) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	ctx.AppendBreadcrumb("Stammbaum", "/maps/familytree")

	data := map[string]interface{}{
		"ctx": ctx,
	}

	wurzel.RenderTemplate(data, res, "maps/familytree.html")
}

// /material
type APPMaterial struct {
	APPMaterialList   *APPMaterialList   // route /material/
	APPMaterialDetail *APPMaterialDetail // route /material/{ID}
	APPMaterialCreate *APPMaterialCreate // route /material/create
}
func (h *APPMaterial) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	ctx.AppendBreadcrumb("Materialien", "/material")

	var head string
	head, _ = router.ShiftPath(req.URL.Path)

	switch head {
	case "":
		h.APPMaterialList.ServeHTTP(res, req, ctx)
	case "create":
		h.APPMaterialCreate.ServeHTTP(res, req, ctx)
	default:
		h.APPMaterialDetail.ServeHTTP(res, req, ctx)
	}
	return
}

// /material/
type APPMaterialList struct {
}
func (h *APPMaterialList) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	data := map[string]interface{}{
		"ctx": ctx,
	}

	wurzel.RenderTemplate(data, res, "material/list.html")
}

// /material/create
type APPMaterialCreate struct {
}
func (h *APPMaterialCreate) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {
	material := models.Material{}

	ctx.AppendBreadcrumb("Neues Material Anlegen", "/material/create")

	data := map[string]interface{}{
		"ctx":      ctx,
		"material": material,
	}

	wurzel.RenderTemplate(data, res, "material/create.html", "material/form.html")
}

// /material/{ID}
type APPMaterialDetail struct {
}
func (h *APPMaterialDetail) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	materialID, err := strconv.Atoi(head)
	if err != nil {
		http.Error(res, fmt.Sprintf("Invalid material id %q", head), http.StatusBadRequest)
		return
	}

	material := models.Material{}

	sql := `
		SELECT 
			m.*,
			mt.material_type_id material_type_id,	
			mt.name material_type_name
		FROM element_material m
			JOIN element_material_type mt
				ON m.material_type_id = mt.material_type_id
		WHERE material_id = :MaterialID;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	err = stmt.Get(&material, map[string]interface{}{"MaterialID": materialID})
	if err != nil {
		log.Print(err)
	}
	defer stmt.Close()

	ctx.AppendBreadcrumb(material.Name, "/material/"+strconv.Itoa(materialID))

	data := map[string]interface{}{
		"ctx":      ctx,
		"material": material,
	}

	wurzel.RenderTemplate(data, res, "material/form.html", "material/detail.html")
}

// /person
type APPPerson struct {
	APPPersonList   *APPPersonList   // route /person/
	APPPersonDetail *APPPersonDetail // route /person/{ID}
	APPPersonCreate *APPPersonCreate // route /person/create
}
func (h *APPPerson) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	ctx.AppendBreadcrumb("Personen", "/person")

	var head string
	head, _ = router.ShiftPath(req.URL.Path)

	switch head {
	case "":
		h.APPPersonList.ServeHTTP(res, req, ctx)
	case "create":
		h.APPPersonCreate.ServeHTTP(res, req, ctx)
	default:
		h.APPPersonDetail.ServeHTTP(res, req, ctx)
	}
	return
}

// /person/
type APPPersonList struct {
}
func (h *APPPersonList) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	data := map[string]interface{}{
		"ctx": ctx,
	}

	wurzel.RenderTemplate(data, res, "person/list.html")
}

// /person/create
type APPPersonCreate struct {
}
func (h *APPPersonCreate) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {
	person := models.Person{}

	ctx.AppendBreadcrumb("Neue Person Anlegen", "/person/create")

	data := map[string]interface{}{
		"ctx":    ctx,
		"person": person,
	}

	wurzel.RenderTemplate(data, res, "person/create.html", "person/form.html")
}

// /person/{ID}
type APPPersonDetail struct {
}
func (h *APPPersonDetail) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	personID, err := strconv.Atoi(head)
	if err != nil {
		http.Error(res, fmt.Sprintf("Invalid person id %q", head), http.StatusBadRequest)
		return
	}

	person := models.Person{}

	sql := `
		SELECT * 
		FROM people_person_list 
		WHERE person_id = :PersonID;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	err = stmt.Get(&person, map[string]interface{}{"PersonID": personID})
	if err != nil {
		log.Print(err)
	}
	defer stmt.Close()

	ctx.AppendBreadcrumb(person.Name, "/person/"+strconv.Itoa(personID))

	data := map[string]interface{}{
		"ctx":    ctx,
		"person": person,
	}

	wurzel.RenderTemplate(data, res, "person/form.html", "person/detail.html")
}

// /place
type APPPlace struct {
	APPPlaceList   *APPPlaceList   // route /place/
	APPPlaceDetail *APPPlaceDetail // route /place/{ID}
	APPPlaceCreate *APPPlaceCreate // route /place/create
}
func (h *APPPlace) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	ctx.AppendBreadcrumb("Orte", "/place")

	var head string
	head, _ = router.ShiftPath(req.URL.Path)

	switch head {
	case "":
		h.APPPlaceList.ServeHTTP(res, req, ctx)
	case "create":
		h.APPPlaceCreate.ServeHTTP(res, req, ctx)
	default:
		h.APPPlaceDetail.ServeHTTP(res, req, ctx)
	}
	return
}

// /place/
type APPPlaceList struct {
}
func (h *APPPlaceList) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	data := map[string]interface{}{
		"ctx": ctx,
	}

	wurzel.RenderTemplate(data, res, "place/list.html")
}

// /place/create
type APPPlaceCreate struct {
}
func (h *APPPlaceCreate) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {
	place := models.Place{}

	ctx.AppendBreadcrumb("Neuen Ort Anlegen", "/place/create")

	data := map[string]interface{}{
		"ctx":   ctx,
		"place": place,
	}

	wurzel.RenderTemplate(data, res, "place/create.html", "place/form.html")
}

// /place/{ID}
type APPPlaceDetail struct {
}
func (h *APPPlaceDetail) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	placeID, err := strconv.Atoi(head)
	if err != nil {
		http.Error(res, fmt.Sprintf("Invalid place id %q", head), http.StatusBadRequest)
		return
	}

	place := models.Place{}

	sql := `
		SELECT 
			p.*,
			pt.place_type_id,
			pt.name place_type_name,
			pt.visuellidentifier_id
		FROM spot_place p
			JOIN spot_place_type pt
				ON p.place_type_id = pt.place_type_id
		WHERE p.place_id = :PlaceID;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	err = stmt.Get(&place, map[string]interface{}{"PlaceID": placeID})
	if err != nil {
		log.Print(err)
	}
	defer stmt.Close()

	ctx.AppendBreadcrumb(place.Name, "/place/"+strconv.Itoa(placeID))

	data := map[string]interface{}{
		"ctx":   ctx,
		"place": place,
	}

	wurzel.RenderTemplate(data, res, "place/form.html", "place/detail.html")
}

// /plant
type APPPlant struct {
	APPPlantList   *APPPlantList   // route /plant/
	APPPlantDetail *APPPlantDetail // route /plant/{ID}
	APPPlantCreate *APPPlantCreate // route /plant/create
}
func (h *APPPlant) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	ctx.AppendBreadcrumb("Pflanzen", "/plant")

	var head string
	head, _ = router.ShiftPath(req.URL.Path)

	switch head {
	case "":
		h.APPPlantList.ServeHTTP(res, req, ctx)
	case "create":
		h.APPPlantCreate.ServeHTTP(res, req, ctx)
	default:
		h.APPPlantDetail.ServeHTTP(res, req, ctx)
	}
	return
}

// /plant/
type APPPlantList struct {
}
func (h *APPPlantList) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	data := map[string]interface{}{
		"ctx": ctx,
	}

	wurzel.RenderTemplate(data, res, "plant/list.html")
}

// /plant/create 
type APPPlantCreate struct {
}
func (h *APPPlantCreate) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {
	plant := models.Plant{}

	ctx.AppendBreadcrumb("Neue Pflanze Anlegen", "/plant/create")

	data := map[string]interface{}{
		"ctx":   ctx,
		"plant": plant,
	}

	wurzel.RenderTemplate(data, res, "plant/create.html", "plant/form.html")
}

// /plant/{ID}
type APPPlantDetail struct {
}
func (h *APPPlantDetail) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	plantID, err := strconv.Atoi(head)
	if err != nil {
		http.Error(res, fmt.Sprintf("Invalid plant id %q", head), http.StatusBadRequest)
		return
	}

	plant := models.Plant{}

	sql := `
		SELECT 
			p.*,
			pt.plant_type_id plant_type_id,	
			pt.name plant_type_name
		FROM botanic_plant p
			JOIN botanic_plant_type pt
				ON p.plant_type_id = pt.plant_type_id
		WHERE plant_id = :PlantID;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	err = stmt.Get(&plant, map[string]interface{}{"PlantID": plantID})
	if err != nil {
		log.Print(err)
	}
	defer stmt.Close()

	ctx.AppendBreadcrumb(plant.Name, "/plant/"+strconv.Itoa(plantID))

	data := map[string]interface{}{
		"ctx":   ctx,
		"plant": plant,
	}

	wurzel.RenderTemplate(data, res, "plant/form.html", "plant/detail.html")
}

// /bible/snippet
type APPSnippet struct {
	APPSnippetList *APPSnippetList
}
func (h *APPSnippet) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	ctx.AppendBreadcrumb("Ausschnitte", "/bible/snippet")

	var head string
	head, _ = router.ShiftPath(req.URL.Path)

	switch head {
	case "":
		h.APPSnippetList.ServeHTTP(res, req, ctx)
	default:
	}
}

// /bible/snippet/
type APPSnippetList struct {
}
func (h *APPSnippetList) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	data := map[string]interface{}{
		"ctx": ctx,
	}
	wurzel.RenderTemplate(data, res, "snippet/list.html")
}

// /bible/snippetng
type APPSnippetNG struct {
	APPSnippetListNG *APPSnippetListNG
}
func (h *APPSnippetNG) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	ctx.AppendBreadcrumb("AusschnitteNG", "/bible/snippetng")

	var head string
	head, _ = router.ShiftPath(req.URL.Path)

	switch head {
	case "":
		h.APPSnippetListNG.ServeHTTP(res, req, ctx)
	default:
	}
}

// /bible/snippetng/
type APPSnippetListNG struct {
}
func (h *APPSnippetListNG) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	data := map[string]interface{}{
		"ctx": ctx,
	}
	wurzel.RenderTemplate(data, res, "snippetng/list.html")
}

// /thing
type APPThing struct {
	APPThingList   *APPThingList   // route /thing/
	APPThingDetail *APPThingDetail // route /thing/{ID}
	APPThingCreate *APPThingCreate // route /thing/create
}
func (h *APPThing) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	ctx.AppendBreadcrumb("Objekte", "/thing")

	var head string
	head, _ = router.ShiftPath(req.URL.Path)

	switch head {
	case "":
		h.APPThingList.ServeHTTP(res, req, ctx)
	case "create":
		h.APPThingCreate.ServeHTTP(res, req, ctx)
	default:
		h.APPThingDetail.ServeHTTP(res, req, ctx)
	}
	return
}

// /thing/
type APPThingList struct {
}
func (h *APPThingList) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	data := map[string]interface{}{
		"ctx": ctx,
	}

	wurzel.RenderTemplate(data, res, "thing/list.html")
}

// /thing/create
type APPThingCreate struct {
}
func (h *APPThingCreate) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {
	thing := models.Thing{}

	ctx.AppendBreadcrumb("Neues Objekt Anlegen", "/thing/create")

	data := map[string]interface{}{
		"ctx":   ctx,
		"thing": thing,
	}

	wurzel.RenderTemplate(data, res, "thing/create.html", "thing/form.html")
}

// /thing/{ID}
type APPThingDetail struct {
}
func (h *APPThingDetail) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	thingID, err := strconv.Atoi(head)
	if err != nil {
		http.Error(res, fmt.Sprintf("Invalid thing id %q", head), http.StatusBadRequest)
		return
	}

	thing := models.Thing{}

	sql := `
		SELECT 
			t.*,
			tt.thing_type_id thing_type_id,	
			tt.name thing_type_name
		FROM object_thing t
			JOIN object_thing_type tt
				ON t.thing_type_id = tt.thing_type_id
		WHERE t.thing_id = :ThingID;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	err = stmt.Get(&thing, map[string]interface{}{"ThingID": thingID})
	if err != nil {
		log.Print(err)
	}
	defer stmt.Close()

	ctx.AppendBreadcrumb(thing.Name, "/thing/"+strconv.Itoa(thingID))

	data := map[string]interface{}{
		"ctx":   ctx,
		"thing": thing,
	}

	wurzel.RenderTemplate(data, res, "thing/form.html", "thing/detail.html")
}

// /tribe
type APPTribe struct {
	APPTribeList   *APPTribeList   // route /tribe/
	APPTribeDetail *APPTribeDetail // route /tribe/{ID}
	APPTribeCreate *APPTribeCreate // route /tribe/create
}
func (h *APPTribe) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	ctx.AppendBreadcrumb("Stämme", "/tribe")

	var head string
	head, _ = router.ShiftPath(req.URL.Path)

	switch head {
	case "":
		h.APPTribeList.ServeHTTP(res, req, ctx)
	case "create":
		h.APPTribeCreate.ServeHTTP(res, req, ctx)
	default:
		h.APPTribeDetail.ServeHTTP(res, req, ctx)
	}
	return
}

// /tribe
type APPTribeList struct {
}
func (h *APPTribeList) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	data := map[string]interface{}{
		"ctx": ctx,
	}

	wurzel.RenderTemplate(data, res, "tribe/list.html")
}

// /tribe/create
type APPTribeCreate struct {
}
func (h *APPTribeCreate) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {
	tribe := models.Tribe{}

	ctx.AppendBreadcrumb("Neuen Stamm Anlegen", "/tribe/create")

	data := map[string]interface{}{
		"ctx":   ctx,
		"tribe": tribe,
	}

	wurzel.RenderTemplate(data, res, "tribe/create.html", "tribe/form.html")
}

// /tribe/{ID}
type APPTribeDetail struct {
}
func (h *APPTribeDetail) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	tribeID, err := strconv.Atoi(head)
	if err != nil {
		http.Error(res, fmt.Sprintf("Invalid tribe id %q", head), http.StatusBadRequest)
		return
	}

	tribe := models.Tribe{}

	sql := `
		SELECT
			t.*,
			22 AS visuellidentifier_id	
		FROM clan_tribe t
		WHERE tribe_id = :TribeID;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	err = stmt.Get(&tribe, map[string]interface{}{"TribeID": tribeID})
	if err != nil {
		log.Print(err)
	}
	defer stmt.Close()

	ctx.AppendBreadcrumb(tribe.Name, "/tribe/"+strconv.Itoa(tribeID))

	data := map[string]interface{}{
		"ctx":   ctx,
		"tribe": tribe,
	}

	wurzel.RenderTemplate(data, res, "tribe/form.html", "tribe/detail.html")
}

// /unit
type APPUnit struct {
	APPUnitList   *APPUnitList   // route /unit/
	APPUnitDetail *APPUnitDetail // route /unit/{ID}
	APPUnitCreate *APPUnitCreate // route /unit/create
}
func (h *APPUnit) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	ctx.AppendBreadcrumb("Maßeinheiten", "/unit")

	var head string
	head, _ = router.ShiftPath(req.URL.Path)

	switch head {
	case "":
		h.APPUnitList.ServeHTTP(res, req, ctx)
	case "create":
		h.APPUnitCreate.ServeHTTP(res, req, ctx)
	default:
		h.APPUnitDetail.ServeHTTP(res, req, ctx)
	}
	return
}

// /unit/
type APPUnitList struct {
}
func (h *APPUnitList) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {

	data := map[string]interface{}{
		"ctx": ctx,
	}

	wurzel.RenderTemplate(data, res, "unit/list.html")
}

// /unit/create
type APPUnitCreate struct {
}
func (h *APPUnitCreate) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {
	unit := models.Unit{}

	ctx.AppendBreadcrumb("Neue Maßeinheit Anlegen", "/unit/create")

	data := map[string]interface{}{
		"ctx":  ctx,
		"unit": unit,
	}

	wurzel.RenderTemplate(data, res, "unit/create.html", "unit/form.html")
}

// /unit/{ID}
type APPUnitDetail struct {
}
func (h *APPUnitDetail) ServeHTTP(res http.ResponseWriter, req *http.Request, ctx *wurzel.Context) {
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	unitID, err := strconv.Atoi(head)
	if err != nil {
		http.Error(res, fmt.Sprintf("Invalid unit id %q", head), http.StatusBadRequest)
		return
	}

	unit := models.Unit{}

	sql := `
		SELECT 
			u.*,
			ut.unit_type_id unit_type_id,	
			ut.name unit_type_name
		FROM measurement_unit u
			JOIN measurement_unit_type ut
				ON u.unit_type_id = ut.unit_type_id
		WHERE u.unit_id = :UnitID;
	`

	stmt, err := database.Con.PrepareNamed(sql)
	err = stmt.Get(&unit, map[string]interface{}{"UnitID": unitID})
	if err != nil {
		log.Print(err)
	}
	defer stmt.Close()

	ctx.AppendBreadcrumb(unit.Name, "/unit/"+strconv.Itoa(unitID))

	data := map[string]interface{}{
		"ctx":  ctx,
		"unit": unit,
	}

	wurzel.RenderTemplate(data, res, "unit/form.html", "unit/detail.html")
}

// /user
type APPUser struct {
	APPUserProfile *APPUserProfile
}
func (h *APPUser) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	var head string
	head, req.URL.Path = router.ShiftPath(req.URL.Path)
	id, err := strconv.Atoi(head)
	if err != nil {
		http.Error(res, fmt.Sprintf("Invalid user id %q", head), http.StatusBadRequest)
		return
	}

	if req.URL.Path != "/" {
		head, tail := router.ShiftPath(req.URL.Path)
		switch head {
		case "profile":
			// We can't just make ProfileHandler an http.Handler; it needs the
			// user id. Let's instead…
			log.Print(tail)
			h.APPUserProfile.Handler(id).ServeHTTP(res, req)
		case "account":
			var (
				name string
			)
			stmt, err := database.Con.Prepare("select name from bible_book")
			if err != nil {
				log.Fatal(err)
			}
			defer stmt.Close()
			rows, err := stmt.Query()
			if err != nil {
				log.Fatal(err)
			}
			defer rows.Close()
			for rows.Next() {
				err := rows.Scan(&name)
				if err != nil {
					log.Fatal(err)
				}
				log.Println(name)
			}
			if err = rows.Err(); err != nil {
				log.Fatal(err)
			}

		default:
			http.Error(res, "Not Found", http.StatusNotFound)
		}
		return
	}
	// As before
}

// /user/{ID}/profile
type APPUserProfile struct {
}
func (h *APPUserProfile) Handler(id int) http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		// Do whatever
	})
}
