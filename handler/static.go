package handler

import (
	"net/http"
	"path"
)

// Static handles route /static and serves static files like CSS and JS
type Static struct {
}

func (h *Static) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	p := path.Clean(path.Join("resources/static", req.URL.Path))
	http.ServeFile(res, req, p)
}
