// Package handler includes all http request handlers.
//
// Some dispatch requests and some are endpoints.
//
// The most important handlers are APP which handles the html GUI.
//
// And API which fullfills all AJAX requests made by the GUI.
package handler
