CREATE VIEW bible_chapter_count 
AS
SELECT 
	book_id, MAX(chapter) chapter_count
FROM
	bible_vers
GROUP BY book_id
