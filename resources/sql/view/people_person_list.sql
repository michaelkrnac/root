CREATE VIEW people_person_list 
AS
SELECT 
	person_id,
    name,
    description,
    gender,
    CASE
		WHEN gender = 'männlich' THEN 6
		WHEN gender = 'weiblich' THEN 7
		ELSE 3
	END visuellidentifier_id
FROM people_person
