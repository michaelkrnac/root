CREATE VIEW people_person_references 
AS
SELECT 
	father_id startperson,
    children_id targetperson,
    'father' reference_type
FROM people_parents
WHERE father_id IS NOT NULL
UNION
SELECT 
	mother_id startperson,
    children_id targetperson,
    'mother' reference_type
FROM people_parents
WHERE mother_id IS NOT NULL
UNION
SELECT DISTINCT
	father_id startperson,
    mother_id targetperson,
    'married' reference_type
FROM people_parents
WHERE father_id IS NOT NULL
	AND mother_id IS NOT NULL
