CREATE OR REPLACE VIEW snippet_object_list AS
SELECT 
	person_id id,
	name name,
    'person' type,
	'Person' typename
FROM people_person
UNION
SELECT
	animal_id id,
    name name,
    'animal' type,
	'Tier' typename
FROM brute_animal
UNION
SELECT
	place_id id,
    name name,
    'place' type,
	'Ort' typename
FROM spot_place
UNION
SELECT
	tribe_id id,
    name name,
    'tribe' type,
	'Stamm' typename
FROM clan_tribe
UNION
SELECT
	plant_id id,
    name name,
    'plant' type,
	'Pflanze' typename
FROM botanic_plant
UNION
SELECT
	material_id id,
    name name,
    'material' type,
	'Material' typename
FROM element_material
UNION
SELECT
	unit_id id,
    name name,
    'unit' type,
	'Maß' typename
FROM measurement_unit
UNION
SELECT
	instrument_id id,
    name name,
    'instrument' type,
	'Instrument' typename
FROM music_instrument
UNION
SELECT
	thing_id id,
    name name,
    'thing' type,
	'Objekt' typename
FROM object_thing
