CREATE VIEW bible_word_full 
AS
SELECT 
	w.word_id AS word_id,
	v.book_id AS book_id,
	v.chapter AS chapter,
	v.number AS vers_number,
	v.vers_id AS vers_id,
	w.position AS position,
	w.length AS length,
	SUBSTR(v.words, w.position, w.length) AS word
FROM bible_word w
	JOIN bible_vers v 
    ON w.vers_id = v.vers_id
ORDER BY w.vers_id , w.position
