CREATE VIEW bible_word_locations AS
SELECT 
	w.*,
    v.words,
	b.name book_name,
    CASE
		WHEN we.entity_id IS NULL THEN 'word'
		ELSE we.entity_type
	END AS entity_type,
	CASE
		WHEN we.entity_id IS NULL THEN w.word_id
		ELSE we.entity_id
	END AS entity_id
FROM bible_word_full w 
	JOIN bible_book b
		ON b.book_id = w.book_id
	JOIN bible_vers v 
		ON v.vers_id = w.vers_id
	LEFT JOIN bible_word_entity we
		ON we.word_id = w.word_id
