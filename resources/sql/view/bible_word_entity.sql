CREATE VIEW bible_word_entity 
AS
SELECT 
	pl.word_id,
    'person' entity_type,
    pl.person_id entity_id
FROM people_location pl
UNION
SELECT 
	bl.word_id,
    'animal' entity_type,
    bl.animal_id entity_id
FROM brute_location bl
UNION
SELECT 
	sl.word_id,
    'place' entity_type,
    sl.place_id entity_id
FROM spot_location sl
UNION
SELECT 
	cl.word_id,
    'tribe' entity_type,
    cl.tribe_id entity_id
FROM clan_location cl
UNION
SELECT 
	bl.word_id,
    'plant' entity_type,
    bl.plant_id entity_id
FROM botanic_location bl
UNION
SELECT 
	el.word_id,
    'material' entity_type,
    el.material_id entity_id
FROM element_location el
UNION
SELECT 
	ml.word_id,
    'unit' entity_type,
    ml.unit_id entity_id
FROM measurement_location ml
UNION
SELECT 
	ml.word_id,
    'instrument' entity_type,
    ml.instrument_id entity_id
FROM music_location ml
UNION
SELECT 
	ol.word_id,
    'thing' entity_type,
    ol.thing_id entity_id
FROM object_location ol
