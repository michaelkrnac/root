CREATE VIEW bible_book_full AS
SELECT 
	b.*,
    cc.chapter_count,
    c.name collection_name,
    c.name_long collection_name_long
FROM bible_book b
	JOIN bible_chapter_count cc
		ON b.book_id = cc.book_id
    JOIN bible_collection c
		ON b.collection_id = c.collection_id
