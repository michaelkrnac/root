CREATE VIEW spot_place_v AS
SELECT 
	p.place_id place_id,
    p.name name,
    p.description description,
    p.place_type_id place_type_id,
    pt.name pt_name,
    pt.description pt_description,
    pt.visuellidentifier_id
FROM spot_place p
LEFT JOIN spot_place_type pt
	ON p.place_type_id = pt.place_type_id
