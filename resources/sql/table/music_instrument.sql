CREATE TABLE music_instrument (
	instrument_id			INT NOT NULL AUTO_INCREMENT,
	name				VARCHAR(255) NOT NULL,
	description			VARCHAR(1000),
	visuellidentifier_id	INT NOT NULL,
	instrument_type_id		INT NOT NULL
) ENGINE = InnoDB;

ALTER TABLE music_instrument ADD PRIMARY KEY (instrument_id);

ALTER TABLE music_instrument 
ADD CONSTRAINT music_instrument_fk1
FOREIGN KEY (instrument_type_id)
REFERENCES music_instrument_type (instrument_type_id);

ALTER TABLE music_instrument 
ADD CONSTRAINT music_instrument_fk2
FOREIGN KEY (visuellidentifier_id)
REFERENCES st_visuellidentifier (visuellidentifier_id);

CREATE INDEX music_instrument_fk1_idx ON music_instrument (instrument_type_id);
CREATE INDEX music_instrument_fk2_idx ON music_instrument (visuellidentifier_id);
CREATE UNIQUE INDEX music_instrument_name_idx ON music_instrument (name);

