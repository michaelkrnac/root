CREATE TABLE element_location (
	word_id		INT NOT NULL,
	material_id	INT NOT NULL
) ENGINE = InnoDB;

ALTER TABLE element_location ADD PRIMARY KEY (word_id, material_id);

ALTER TABLE element_location 
ADD CONSTRAINT element_location_fk1
FOREIGN KEY (word_id)
REFERENCES bible_word (word_id);

ALTER TABLE element_location 
ADD CONSTRAINT element_location_fk2
FOREIGN KEY (material_id)
REFERENCES element_material (material_id);

CREATE INDEX element_location_fk1_idx ON element_location (word_id);
CREATE INDEX element_location_fk2_idx ON element_location (material_id);
CREATE UNIQUE INDEX element_location_word_id_idx ON element_location (word_id);
