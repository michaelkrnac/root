CREATE TABLE botanic_plant (
	plant_id			INT NOT NULL AUTO_INCREMENT,
	name				VARCHAR(255) NOT NULL,
	description			VARCHAR(1000),
	visuellidentifier_id	INT NOT NULL,
	plant_type_id		INT NOT NULL
) ENGINE = InnoDB;

ALTER TABLE botanic_plant ADD PRIMARY KEY (plant_id);

ALTER TABLE botanic_plant 
ADD CONSTRAINT botanic_plant_fk1
FOREIGN KEY (plant_type_id)
REFERENCES botanic_plant_type (plant_type_id);

ALTER TABLE botanic_plant 
ADD CONSTRAINT botanic_plant_fk2
FOREIGN KEY (visuellidentifier_id)
REFERENCES st_visuellidentifier (visuellidentifier_id);

CREATE INDEX botanic_plant_fk1_idx ON botanic_plant (plant_type_id);
CREATE INDEX botanic_plant_fk2_idx ON botanic_plant (visuellidentifier_id);
CREATE UNIQUE INDEX botanic_plant_name_idx ON botanic_plant (name);

