CREATE TABLE people_parents (
	children_id		INT NOT NULL,
	father_id		INT NULL,
	mother_id		INT NULL
) ENGINE = InnoDB;

ALTER TABLE people_parents ADD PRIMARY KEY (children_id);

ALTER TABLE people_parents 
ADD CONSTRAINT people_parents_fk1
FOREIGN KEY (children_id)
REFERENCES people_person (person_id)
ON DELETE CASCADE;

ALTER TABLE people_parents 
ADD CONSTRAINT people_parents_fk2
FOREIGN KEY (father_id)
REFERENCES people_person (person_id);

ALTER TABLE people_parents 
ADD CONSTRAINT people_parents_fk3
FOREIGN KEY (mother_id)
REFERENCES people_person (person_id);

CREATE INDEX people_parents_fk1_idx ON people_parents (children_id);
CREATE INDEX people_parents_fk2_idx ON people_parents (father_id);
CREATE INDEX people_parents_fk3_idx ON people_parents (mother_id);
