CREATE TABLE brute_animal (
	animal_id	INT NOT NULL AUTO_INCREMENT,
	name		VARCHAR(255) NOT NULL,
	description VARCHAR(1000),
	visuellidentifier_id	INT NOT NULL,
	class_id	INT NOT NULL
) ENGINE = InnoDB;

ALTER TABLE brute_animal ADD PRIMARY KEY (animal_id);

ALTER TABLE brute_animal 
ADD CONSTRAINT brute_animal_fk1
FOREIGN KEY (class_id)
REFERENCES brute_class (class_id);

ALTER TABLE brute_animal 
ADD CONSTRAINT brute_animal_fk2
FOREIGN KEY (visuellidentifier_id)
REFERENCES st_visuellidentifier (visuellidentifier_id);

CREATE INDEX brute_animal_fk1_idx ON brute_animal (class_id);
CREATE INDEX brute_animal_fk2_idx ON brute_animal (visuellidentifier_id);
CREATE UNIQUE INDEX brute_animal_name_idx ON brute_animal (name);

