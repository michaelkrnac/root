CREATE TABLE element_material (
	material_id			INT NOT NULL AUTO_INCREMENT,
	name				VARCHAR(255) NOT NULL,
	description			VARCHAR(1000),
	visuellidentifier_id	INT NOT NULL,
	material_type_id		INT NOT NULL
) ENGINE = InnoDB;

ALTER TABLE element_material ADD PRIMARY KEY (material_id);

ALTER TABLE element_material 
ADD CONSTRAINT element_material_fk1
FOREIGN KEY (material_type_id)
REFERENCES element_material_type (material_type_id);

ALTER TABLE element_material 
ADD CONSTRAINT element_material_fk2
FOREIGN KEY (visuellidentifier_id)
REFERENCES st_visuellidentifier (visuellidentifier_id);

CREATE INDEX element_material_fk1_idx ON element_material (material_type_id);
CREATE INDEX element_material_fk2_idx ON element_material (visuellidentifier_id);
CREATE UNIQUE INDEX element_material_name_idx ON element_material (name);

