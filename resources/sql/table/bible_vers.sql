CREATE TABLE bible_vers (
	vers_id		INT NOT NULL AUTO_INCREMENT,
	book_id		INT NOT NULL,
	chapter		INT NOT NULL,
	number		INT NOT NULL,
	words		TEXT NULL
) ENGINE = InnoDB;

ALTER TABLE bible_vers ADD PRIMARY KEY (vers_id);

ALTER TABLE bible_vers 
ADD CONSTRAINT bible_vers_fk1
FOREIGN KEY (book_id)
REFERENCES bible_book (book_id);

CREATE INDEX bible_vers_fk1_idx ON bible_vers (book_id);
CREATE FULLTEXT INDEX bible_vers_words_idx ON bible_vers (words);
