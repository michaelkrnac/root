CREATE TABLE clan_tribe (
	tribe_id	INT NOT NULL AUTO_INCREMENT,
	name		VARCHAR(255) NOT NULL,
	description VARCHAR(1000) NULL
) ENGINE = InnoDB;

ALTER TABLE clan_tribe ADD PRIMARY KEY (tribe_id);

CREATE UNIQUE INDEX clan_tribe_name_idx ON clan_tribe (name);
