CREATE TABLE measurement_unit_type (
	unit_type_id	INT NOT NULL AUTO_INCREMENT,
	name			VARCHAR(255) NOT NULL,
	description		VARCHAR(1000),
	visuellidentifier_id INT NOT NULL
) ENGINE = InnoDB;

ALTER TABLE measurement_unit_type ADD PRIMARY KEY (unit_type_id);

ALTER TABLE measurement_unit_type 
ADD CONSTRAINT measurement_unit_type_fk1
FOREIGN KEY (visuellidentifier_id)
REFERENCES st_visuellidentifier (visuellidentifier_id);

CREATE INDEX measurement_unit_type_fk1_idx ON measurement_unit_type (visuellidentifier_id);
CREATE UNIQUE INDEX measurement_unit_type_name_idx ON measurement_unit_type (name);
