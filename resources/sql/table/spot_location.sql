CREATE TABLE spot_location (
	word_id		INT NOT NULL,
	place_id	INT NOT NULL
) ENGINE = InnoDB;

ALTER TABLE spot_location ADD PRIMARY KEY (word_id, place_id);

ALTER TABLE spot_location 
ADD CONSTRAINT spot_location_fk1
FOREIGN KEY (word_id)
REFERENCES bible_word (word_id);

ALTER TABLE spot_location 
ADD CONSTRAINT spot_location_fk2
FOREIGN KEY (place_id)
REFERENCES spot_place (place_id);

CREATE INDEX spot_location_fk1_idx ON spot_location (word_id);
CREATE INDEX spot_location_fk2_idx ON spot_location (place_id);
CREATE UNIQUE INDEX spot_location_word_id_idx ON spot_location (word_id);
