CREATE TABLE measurement_location (
	word_id		INT NOT NULL,
	unit_id	INT NOT NULL
) ENGINE = InnoDB;

ALTER TABLE measurement_location ADD PRIMARY KEY (word_id, unit_id);

ALTER TABLE measurement_location 
ADD CONSTRAINT measurement_location_fk1
FOREIGN KEY (word_id)
REFERENCES bible_word (word_id);

ALTER TABLE measurement_location 
ADD CONSTRAINT measurement_location_fk2
FOREIGN KEY (unit_id)
REFERENCES measurement_unit (unit_id);

CREATE INDEX measurement_location_fk1_idx ON measurement_location (word_id);
CREATE INDEX measurement_location_fk2_idx ON measurement_location (unit_id);
CREATE UNIQUE INDEX measurement_location_word_id_idx ON measurement_location (word_id);
