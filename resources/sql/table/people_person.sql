CREATE TABLE people_person (
	person_id	INT NOT NULL AUTO_INCREMENT,
	name		VARCHAR(255) NOT NULL,
	gender		VARCHAR(45),
	description VARCHAR(1000)
) ENGINE = InnoDB;

ALTER TABLE people_person ADD PRIMARY KEY (person_id);

CREATE UNIQUE INDEX people_person_name_idx ON people_person (name);
