CREATE TABLE botanic_plant_type (
	plant_type_id	INT NOT NULL AUTO_INCREMENT,
	name			VARCHAR(255) NOT NULL,
	description		VARCHAR(1000),
	visuellidentifier_id INT NOT NULL
) ENGINE = InnoDB;

ALTER TABLE botanic_plant_type ADD PRIMARY KEY (plant_type_id);

ALTER TABLE botanic_plant_type 
ADD CONSTRAINT botanic_plant_type_fk1
FOREIGN KEY (visuellidentifier_id)
REFERENCES st_visuellidentifier (visuellidentifier_id);

CREATE INDEX botanic_plant_type_fk1_idx ON botanic_plant_type (visuellidentifier_id);
CREATE UNIQUE INDEX botanic_plant_type_name_idx ON botanic_plant_type (name);
