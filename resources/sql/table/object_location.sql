CREATE TABLE object_location (
	word_id		INT NOT NULL,
	thing_id	INT NOT NULL
) ENGINE = InnoDB;

ALTER TABLE object_location ADD PRIMARY KEY (word_id, thing_id);

ALTER TABLE object_location 
ADD CONSTRAINT object_location_fk1
FOREIGN KEY (word_id)
REFERENCES bible_word (word_id);

ALTER TABLE object_location 
ADD CONSTRAINT object_location_fk2
FOREIGN KEY (thing_id)
REFERENCES object_thing (thing_id);

CREATE INDEX object_location_fk1_idx ON object_location (word_id);
CREATE INDEX object_location_fk2_idx ON object_location (thing_id);
CREATE UNIQUE INDEX object_location_word_id_idx ON object_location (word_id);
