CREATE TABLE object_thing (
	thing_id			INT NOT NULL AUTO_INCREMENT,
	name				VARCHAR(255) NOT NULL,
	description			VARCHAR(1000),
	visuellidentifier_id	INT NOT NULL,
	thing_type_id		INT NOT NULL
) ENGINE = InnoDB;

ALTER TABLE object_thing ADD PRIMARY KEY (thing_id);

ALTER TABLE object_thing 
ADD CONSTRAINT object_thing_fk1
FOREIGN KEY (thing_type_id)
REFERENCES object_thing_type (thing_type_id);

ALTER TABLE object_thing 
ADD CONSTRAINT object_thing_fk2
FOREIGN KEY (visuellidentifier_id)
REFERENCES st_visuellidentifier (visuellidentifier_id);

CREATE INDEX object_thing_fk1_idx ON object_thing (thing_type_id);
CREATE INDEX object_thing_fk2_idx ON object_thing (visuellidentifier_id);
CREATE UNIQUE INDEX object_thing_name_idx ON object_thing (name);

