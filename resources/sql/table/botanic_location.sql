CREATE TABLE botanic_location (
	word_id		INT NOT NULL,
	plant_id	INT NOT NULL
) ENGINE = InnoDB;

ALTER TABLE botanic_location ADD PRIMARY KEY (word_id, plant_id);

ALTER TABLE botanic_location 
ADD CONSTRAINT botanic_location_fk1
FOREIGN KEY (word_id)
REFERENCES bible_word (word_id);

ALTER TABLE botanic_location 
ADD CONSTRAINT botanic_location_fk2
FOREIGN KEY (plant_id)
REFERENCES botanic_plant (plant_id);

CREATE INDEX botanic_location_fk1_idx ON botanic_location (word_id);
CREATE INDEX botanic_location_fk2_idx ON botanic_location (plant_id);
CREATE UNIQUE INDEX botanic_location_word_id_idx ON botanic_location (word_id);
