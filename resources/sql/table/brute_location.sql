CREATE TABLE brute_location (
	word_id		INT NOT NULL,
	animal_id	INT NOT NULL
) ENGINE = InnoDB;

ALTER TABLE brute_location ADD PRIMARY KEY (word_id, animal_id);

ALTER TABLE brute_location 
ADD CONSTRAINT brute_location_fk1
FOREIGN KEY (word_id)
REFERENCES bible_word (word_id);

ALTER TABLE brute_location 
ADD CONSTRAINT brute_location_fk2
FOREIGN KEY (animal_id)
REFERENCES brute_animal (animal_id);

CREATE INDEX brute_location_fk1_idx ON brute_location (word_id);
CREATE INDEX brute_location_fk2_idx ON brute_location (animal_id);
CREATE UNIQUE INDEX brute_location_word_id_idx ON brute_location (word_id);
