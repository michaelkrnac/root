CREATE TABLE people_location (
	word_id		INT NOT NULL,
	person_id	INT NOT NULL
) ENGINE = InnoDB;

ALTER TABLE people_location ADD PRIMARY KEY (word_id, person_id);

ALTER TABLE people_location 
ADD CONSTRAINT people_location_fk1
FOREIGN KEY (word_id)
REFERENCES bible_word (word_id);

ALTER TABLE people_location 
ADD CONSTRAINT people_location_fk2
FOREIGN KEY (person_id)
REFERENCES people_person (person_id);

CREATE INDEX people_location_fk1_idx ON people_location (word_id);
CREATE INDEX people_location_fk2_idx ON people_location (person_id);
CREATE UNIQUE INDEX people_location_word_id_idx ON people_location (word_id);
