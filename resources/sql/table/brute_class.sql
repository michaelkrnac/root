CREATE TABLE brute_class (
	class_id	INT NOT NULL AUTO_INCREMENT,
	name		VARCHAR(255) NOT NULL,
	description VARCHAR(1000)
) ENGINE = InnoDB;

ALTER TABLE brute_class ADD PRIMARY KEY (class_id);

CREATE UNIQUE INDEX brute_class_name_idx ON brute_class (name);
