CREATE TABLE bible_collection (
	collection_id	INT NOT NULL AUTO_INCREMENT,
	name			VARCHAR(1000) NULL,
	name_long		VARCHAR(1000) NULL
) ENGINE = InnoDB;

ALTER TABLE bible_collection ADD PRIMARY KEY (collection_id);
