CREATE TABLE st_visuellidentifier (
	visuellidentifier_id	INT NOT NULL AUTO_INCREMENT,
	system_name			VARCHAR(255) NOT NULL,
	license				VARCHAR(1000),
	artist				VARCHAR(1000),
	description			VARCHAR(1000),
	url					VARCHAR(2000)
) ENGINE = InnoDB;

ALTER TABLE st_visuellidentifier ADD PRIMARY KEY (visuellidentifier_id);

CREATE UNIQUE INDEX st_visuellidentifier_systenm_name_idx ON st_visuellidentifier (visuellidentifier_id);
