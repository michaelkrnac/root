CREATE TABLE bible_book (
	book_id				INT NOT NULL AUTO_INCREMENT,
	testament			VARCHAR(3),
	name				VARCHAR(1000),
	name_alternative	VARCHAR(1000),
	name_short			VARCHAR(1000),
	name_long			VARCHAR(1000),
	collection_id		INT NOT NULL
) ENGINE = InnoDB;

ALTER TABLE bible_book ADD PRIMARY KEY (book_id);

ALTER TABLE bible_book 
ADD CONSTRAINT bible_book_fk1
FOREIGN KEY (collection_id)
REFERENCES bible_collection (collection_id);

CREATE INDEX bible_book_fk1_idx ON bible_book (collection_id);


