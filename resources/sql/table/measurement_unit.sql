CREATE TABLE measurement_unit (
	unit_id			INT NOT NULL AUTO_INCREMENT,
	name				VARCHAR(255) NOT NULL,
	description			VARCHAR(1000),
	visuellidentifier_id	INT NOT NULL,
	unit_type_id		INT NOT NULL
) ENGINE = InnoDB;

ALTER TABLE measurement_unit ADD PRIMARY KEY (unit_id);

ALTER TABLE measurement_unit 
ADD CONSTRAINT measurement_unit_fk1
FOREIGN KEY (unit_type_id)
REFERENCES measurement_unit_type (unit_type_id);

ALTER TABLE measurement_unit 
ADD CONSTRAINT measurement_unit_fk2
FOREIGN KEY (visuellidentifier_id)
REFERENCES st_visuellidentifier (visuellidentifier_id);

CREATE INDEX measurement_unit_fk1_idx ON measurement_unit (unit_type_id);
CREATE INDEX measurement_unit_fk2_idx ON measurement_unit (visuellidentifier_id);
CREATE UNIQUE INDEX measurement_unit_name_idx ON measurement_unit (name);

