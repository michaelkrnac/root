CREATE TABLE spot_place (
	place_id		INT NOT NULL AUTO_INCREMENT,
	name			VARCHAR(255) NOT NULL,
	description		VARCHAR(1000),
	place_type_id	INT NOT NULL,
	lat				DECIMAL(10,6),
	lon				DECIMAL(10,6)
) ENGINE = InnoDB;

ALTER TABLE spot_place ADD PRIMARY KEY (place_id);

ALTER TABLE spot_place 
ADD CONSTRAINT spot_place_fk1
FOREIGN KEY (place_type_id)
REFERENCES spot_place_type (place_type_id);

CREATE INDEX spot_place_fk1_idx ON spot_place (place_type_id);
CREATE UNIQUE INDEX spot_place_name_idx ON spot_place (name);
