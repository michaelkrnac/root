CREATE TABLE bible_word (
	word_id		INT NOT NULL AUTO_INCREMENT,
	vers_id		INT NOT NULL,
	position	INT UNSIGNED NOT NULL,
	length		INT UNSIGNED NOT NULL
) ENGINE = InnoDB;

ALTER TABLE bible_word ADD PRIMARY KEY (word_id);

ALTER TABLE bible_word 
ADD CONSTRAINT bible_word_fk1
FOREIGN KEY (vers_id)
REFERENCES bible_vers (vers_id);

CREATE INDEX bible_word_fk1_idx ON bible_word (vers_id);
