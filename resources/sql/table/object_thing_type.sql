CREATE TABLE object_thing_type (
	thing_type_id	INT NOT NULL AUTO_INCREMENT,
	name			VARCHAR(255) NOT NULL,
	description		VARCHAR(1000),
	visuellidentifier_id INT NOT NULL
) ENGINE = InnoDB;

ALTER TABLE object_thing_type ADD PRIMARY KEY (thing_type_id);

ALTER TABLE object_thing_type 
ADD CONSTRAINT object_thing_type_fk1
FOREIGN KEY (visuellidentifier_id)
REFERENCES st_visuellidentifier (visuellidentifier_id);

CREATE INDEX object_thing_type_fk1_idx ON object_thing_type (visuellidentifier_id);
CREATE UNIQUE INDEX object_thing_type_name_idx ON object_thing_type (name);
