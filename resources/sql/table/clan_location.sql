CREATE TABLE clan_location (
	word_id		INT NOT NULL,
	tribe_id	INT NOT NULL
) ENGINE = InnoDB;

ALTER TABLE clan_location ADD PRIMARY KEY (word_id, tribe_id);

ALTER TABLE clan_location 
ADD CONSTRAINT clan_location_fk1
FOREIGN KEY (word_id)
REFERENCES bible_word (word_id);

ALTER TABLE clan_location 
ADD CONSTRAINT clan_location_fk2
FOREIGN KEY (tribe_id)
REFERENCES clan_tribe (tribe_id);

CREATE INDEX clan_location_fk1_idx ON clan_location (word_id);
CREATE INDEX clan_location_fk2_idx ON clan_location (tribe_id);
CREATE UNIQUE INDEX clan_location_word_id_idx ON clan_location (word_id);
