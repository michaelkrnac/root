CREATE TABLE music_location (
	word_id		INT NOT NULL,
	instrument_id	INT NOT NULL
) ENGINE = InnoDB;

ALTER TABLE music_location ADD PRIMARY KEY (word_id, instrument_id);

ALTER TABLE music_location 
ADD CONSTRAINT music_location_fk1
FOREIGN KEY (word_id)
REFERENCES bible_word (word_id);

ALTER TABLE music_location 
ADD CONSTRAINT music_location_fk2
FOREIGN KEY (instrument_id)
REFERENCES music_instrument (instrument_id);

CREATE INDEX music_location_fk1_idx ON music_location (word_id);
CREATE INDEX music_location_fk2_idx ON music_location (instrument_id);
CREATE UNIQUE INDEX music_location_word_id_idx ON music_location (word_id);
