CREATE TABLE element_material_type (
	material_type_id	INT NOT NULL AUTO_INCREMENT,
	name			VARCHAR(255) NOT NULL,
	description		VARCHAR(1000),
	visuellidentifier_id INT NOT NULL
) ENGINE = InnoDB;

ALTER TABLE element_material_type ADD PRIMARY KEY (material_type_id);

ALTER TABLE element_material_type 
ADD CONSTRAINT element_material_type_fk1
FOREIGN KEY (visuellidentifier_id)
REFERENCES st_visuellidentifier (visuellidentifier_id);

CREATE INDEX element_material_type_fk1_idx ON element_material_type (visuellidentifier_id);
CREATE UNIQUE INDEX element_material_type_name_idx ON element_material_type (name);
