CREATE TABLE spot_place_type (
	place_type_id	INT NOT NULL AUTO_INCREMENT,
	name			VARCHAR(255) NOT NULL,
	description		VARCHAR(1000),
	visuellidentifier_id INT NOT NULL
) ENGINE = InnoDB;

ALTER TABLE spot_place_type ADD PRIMARY KEY (place_type_id);

ALTER TABLE spot_place_type 
ADD CONSTRAINT spot_place_type_fk1
FOREIGN KEY (visuellidentifier_id)
REFERENCES st_visuellidentifier (visuellidentifier_id);

CREATE INDEX spot_place_type_fk1_idx ON spot_place_type (visuellidentifier_id);
CREATE UNIQUE INDEX spot_place_type_name_idx ON spot_place_type (name);
