package wurzel

import (
	"encoding/json"
	"html/template"
	"log"
	"net/http"
)

// Context stores the global context variables
type Context struct {
	User        User
	Page        Page
	Breadcrumbs []Breadcrumb
}

// NewContext returns a new context
func NewContext(username string) (ctx *Context) {

	c := new(Context)
	c.User = User{
		Name: username,
	}

	return c
}

// AppendBreadcrumb appends a breadcrum to thecontext
func (ctx *Context) AppendBreadcrumb(title string, url string) {
	ctx.Page = Page{
		Title: title,
	}

	b := Breadcrumb{
		Name: title,
		URL:  url,
	}

	ctx.Breadcrumbs = append(ctx.Breadcrumbs, b)
}

// Breadcrumb stores one breadcrumb
type Breadcrumb struct {
	Name string
	URL  string
}

// User stores user attributes
type User struct {
	Name string
}

// Page stores website page attributes
type Page struct {
	Title string
}

// RenderTemplate renders a template in the default layout
func RenderTemplate(data map[string]interface{}, res http.ResponseWriter, templatePaths ...string) {
	var tpl []string
	tpl = append(tpl, "resources/templates/main.html")
	tpl = append(tpl, "resources/templates/navbar.html")
	tpl = append(tpl, "resources/templates/breadcrumb.html")
	tpl = append(tpl, "resources/templates/footer.html")

	for _, templatePath := range templatePaths {
		tpl = append(tpl, "resources/templates/"+templatePath)
	}

	t, err := template.New("main.html").Funcs(TemplateFuncs).ParseFiles(tpl...)
	if err != nil {
		log.Print(err)
	}

	err = t.Execute(res, data)
	if err != nil {
		log.Print(err)
	}
}

// TemplateFuncs plus1 calculate the last breadcrumb
var TemplateFuncs = template.FuncMap{
	"plus1": func(x int) int {
		return x + 1
	},
}

const (
	// Success msg type
	Success = "success"
	// Error msg type
	Error = "error"
	// Info msg type
	Info = "info"
	// Warning msg type
	Warning = "warning"
)

// Msg represents a json message send to the client
type Msg struct {
	Type   string
	Msg    string
	Status int
}

// JSONError sends a JSON Error Message
func JSONError(res http.ResponseWriter, msg string) {
	m := Msg{
		Type:   Error,
		Msg:    msg,
		Status: http.StatusBadRequest,
	}
	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(http.StatusBadRequest)
	json.NewEncoder(res).Encode(m)
}

// JSONSuccess sends a JSON Success Message
func JSONSuccess(res http.ResponseWriter, msg string) {
	m := Msg{
		Type:   Success,
		Msg:    msg,
		Status: http.StatusOK,
	}
	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(http.StatusOK)
	json.NewEncoder(res).Encode(m)
}

//JSONContent encodes any structur end sends it with the correct header
func JSONContent(res http.ResponseWriter, i interface{}) {
	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(http.StatusOK)
	json.NewEncoder(res).Encode(i)
}
