package configuration

import (
	"encoding/json"
	"log"
	"os"
)

var Conf *Configuration

// Configuration defines the structure of the configuration file
type Configuration struct {
	Port string
	DSN  string
}

// Load Config loads the configuration from a configuration file
func Load(path string) {

	log.Printf("Load %s", path)
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}

	decoder := json.NewDecoder(file)
	err = decoder.Decode(&Conf)
	if err != nil {
		log.Fatal(err)
	}

}
