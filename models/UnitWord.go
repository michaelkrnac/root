package models

// UnitWord defines the structur of the unit to word reference table
type UnitWord struct {
	WordID int `db:"word_id"`
	UnitID int `db:"unit_id"`
}
