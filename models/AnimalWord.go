package models

// AnimalWord defines the structur of ther person word reference table
type AnimalWord struct {
	WordID   int `db:"word_id"`
	AnimalID int `db:"animal_id"`
}
