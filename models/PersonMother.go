package models

// PersonMother defines the structur of ther person mother reference table
type PersonMother struct {
	ChildrenID int `db:"children_id"`
	MotherID   int `db:"mother_id"`
}
