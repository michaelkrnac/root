package models

// InstrumentWord defines the structur of the instrument to word reference table
type InstrumentWord struct {
	WordID       int `db:"word_id"`
	InstrumentID int `db:"instrument_id"`
}
