package models

// UnitType represents the unit_type table
type UnitType struct {
	UnitTypeID          int     `db:"unit_type_id"`
	Name                string  `db:"name"`
	Description         *string `db:"description"`
	VisuellidentifierID *int    `db:"visuellidentifier_id"`
}
