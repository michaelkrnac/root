package models

// PlantType represents the plant_type table
type PlantType struct {
	PlantTypeID         int     `db:"plant_type_id"`
	Name                string  `db:"name"`
	Description         *string `db:"description"`
	VisuellidentifierID *int    `db:"visuellidentifier_id"`
}
