package models

// AnimalClass represents the animal class table
type AnimalClass struct {
	ClassID     int     `db:"class_id"`
	Name        string  `db:"name"`
	Description *string `db:"description"`
}
