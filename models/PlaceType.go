package models

// PlaceType represents the place type table
type PlaceType struct {
	PlaceTypeID         int     `db:"place_type_id"`
	Name                string  `db:"name"`
	Description         *string `db:"description"`
	VisuellidentifierID *int    `db:"visuellidentifier_id"`
}
