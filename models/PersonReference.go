package models

// PersonReference defines the structur of the person to person reference
type PersonReference struct {
	StartPerson   int    `db:"startperson"`
	TargetPerson  int    `db:"targetperson"`
	ReferenceType string `db:"reference_type"`
}
