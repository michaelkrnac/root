package models

// ThingType represents the thing_type table
type ThingType struct {
	ThingTypeID         int     `db:"thing_type_id"`
	Name                string  `db:"name"`
	Description         *string `db:"description"`
	VisuellidentifierID *int    `db:"visuellidentifier_id"`
}
