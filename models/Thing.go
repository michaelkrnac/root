package models

// Thing represents the thing table
type Thing struct {
	ThingID             int     `db:"thing_id"`
	Name                string  `db:"name"`
	Description         *string `db:"description"`
	VisuellidentifierID *int    `db:"visuellidentifier_id"`
	ThingTypeID         *int    `db:"thing_type_id"`
	ThingTypeName       *string `db:"thing_type_name"`
}
