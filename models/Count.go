package models

// Count represents counts
type Count struct {
	Value				*string `db:"value"`
	Count				*int `db:"count"`
}
