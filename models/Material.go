package models

// Material represents the material table
type Material struct {
	MaterialID          int     `db:"material_id"`
	Name                string  `db:"name"`
	Description         *string `db:"description"`
	VisuellidentifierID *int    `db:"visuellidentifier_id"`
	MaterialTypeID      *int    `db:"material_type_id"`
	MaterialTypeName    *string `db:"material_type_name"`
}
