package models

// PersonFather defines the structur of ther person father reference table
type PersonFather struct {
	ChildrenID int `db:"children_id"`
	FatherID   int `db:"father_id"`
}
