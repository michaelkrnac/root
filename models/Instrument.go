package models

// Instrument represents the instrument table
type Instrument struct {
	InstrumentID        int     `db:"instrument_id"`
	Name                string  `db:"name"`
	Description         *string `db:"description"`
	VisuellidentifierID *int    `db:"visuellidentifier_id"`
	InstrumentTypeID    *int    `db:"instrument_type_id"`
	InstrumentTypeName  *string `db:"instrument_type_name"`
}
