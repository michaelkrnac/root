package models

// TribeWord defines the structur of th tribe word reference table
type TribeWord struct {
	WordID  int `db:"word_id"`
	TribeID int `db:"tribe_id"`
}
