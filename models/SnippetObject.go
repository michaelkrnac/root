package models

// SnippetObject defines structrue of marked words
type SnippetObject struct {
	ID       int    `db:"id"`
	Name     string `db:"name"`
	Type     string `db:"type"`
	TypeName string `db:"typename"`
}
