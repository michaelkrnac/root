package models

// Book represents the person table
type Book struct {
	BookID              int     `db:"book_id"`
	Testament           *string `db:"testament"`
	Name                *string `db:"name"`
	NameAlternative     *string `db:"name_alternative"`
	NameShort           *string `db:"name_short"`
	NameLong            *string `db:"name_long"`
	ChapterCount        *string `db:"chapter_count"`
	VisuellidentifierID *int    `db:"visuellidentifier_id"`
}
