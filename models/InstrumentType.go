package models

// InstrumentType represents the instrument_type table
type InstrumentType struct {
	InstrumentTypeID    int     `db:"instrument_type_id"`
	Name                string  `db:"name"`
	Description         *string `db:"description"`
	VisuellidentifierID *int    `db:"visuellidentifier_id"`
}
