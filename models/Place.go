package models

// Place represents the place table
type Place struct {
	PlaceID             int      `db:"place_id"`
	Name                string   `db:"name"`
	Description         *string  `db:"description"`
	PlaceTypeID         *int     `db:"place_type_id"`
	PlaceTypeName       *string  `db:"place_type_name"`
	VisuellidentifierID *int     `db:"visuellidentifier_id"`
	Lat                 *float64 `db:"lat"`
	Lon                 *float64 `db:"lon"`
}
