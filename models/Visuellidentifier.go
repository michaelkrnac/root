package models

// Visuellidentifier represents the visuellidentifer table
type Visuellidentifier struct {
	VisuellidentiferID int     `db:"visuellidentifier_id"`
	SystemName         string  `db:"system_name"`
	License            *string `db:"license"`
	Artist             *string `db:"artist"`
	Description        *string `db:"description"`
	URL                *string `db:"url"`
}
