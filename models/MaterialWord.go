package models

// MaterialWord defines the structur of the material to word reference table
type MaterialWord struct {
	WordID     int `db:"word_id"`
	MaterialID int `db:"material_id"`
}
