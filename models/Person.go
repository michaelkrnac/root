package models

// Person represents the person table
type Person struct {
	PersonID            int     `db:"person_id"`
	Name                string  `db:"name"`
	Gender              *string `db:"gender"`
	Description         *string `db:"description"`
	VisuellidentifierID *int    `db:"visuellidentifier_id"`
}
