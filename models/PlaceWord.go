package models

// PlaceWord defines the structur of the place word reference table
type PlaceWord struct {
	WordID  int `db:"word_id"`
	PlaceID int `db:"place_id"`
}
