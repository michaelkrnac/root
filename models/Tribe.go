package models

// Tribe represents the tribe table
type Tribe struct {
	TribeID             int     `db:"tribe_id"`
	Name                string  `db:"name"`
	Description         *string `db:"description"`
	VisuellidentifierID *int    `db:"visuellidentifier_id"`
}
