package models

// PersonWord defines the structur of ther person word reference table
type PersonWord struct {
	WordID   int `db:"word_id"`
	PersonID int `db:"person_id"`
}
