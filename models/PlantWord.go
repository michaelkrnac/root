package models

// PlantWord defines the structur of the plant to word reference table
type PlantWord struct {
	WordID  int `db:"word_id"`
	PlantID int `db:"plant_id"`
}
