package models

// MaterialType represents the material_type table
type MaterialType struct {
	MaterialTypeID      int     `db:"material_type_id"`
	Name                string  `db:"name"`
	Description         *string `db:"description"`
	VisuellidentifierID *int    `db:"visuellidentifier_id"`
}
