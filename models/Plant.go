package models

// Plant represents the plant table
type Plant struct {
	PlantID             int     `db:"plant_id"`
	Name                string  `db:"name"`
	Description         *string `db:"description"`
	VisuellidentifierID *int    `db:"visuellidentifier_id"`
	PlantTypeID         *int    `db:"plant_type_id"`
	PlantTypeName       *string `db:"plant_type_name"`
}
