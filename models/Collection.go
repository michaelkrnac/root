package models

// Collection represents the bible_collection table
type Collection struct {
	CollectionID        int     `db:"collection_id"`
	Name                *string `db:"name"`
	NameLong            *string `db:"name_long"`
	VisuellidentifierID *int    `db:"visuellidentifier_id"`
}
