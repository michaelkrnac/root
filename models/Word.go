package models

// Word defines structrue of marked words
type Word struct {
	WordID              int    `db:"word_id"`
	BookID              int    `db:"book_id"`
	Chapter             int    `db:"chapter"`
	VersNumber          int    `db:"vers_number"`
	VersID              int    `db:"vers_id"`
	Position            int    `db:"position"`
	Length              int    `db:"length"`
	Word                string `db:"word"`
	Words               string `db:"words"`
	BookName            string `db:"book_name"`
	EntityType          string `db:"entity_type"`
	EntityID            int    `db:"entity_id"`
	VisuellIdentifierID string `db:"visuellidentifier_id"`
}
