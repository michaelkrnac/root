package models

// Animal represents the animal table
type Animal struct {
	AnimalID            int     `db:"animal_id"`
	Name                string  `db:"name"`
	Description         *string `db:"description"`
	VisuellidentifierID *int    `db:"visuellidentifier_id"`
	ClassID             *int    `db:"class_id"`
	ClassName           *string `db:"class_name"`
}
