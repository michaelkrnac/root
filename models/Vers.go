package models

// Vers represents the vers table
type Vers struct {
	VersID  int    `db:"vers_id"`
	BookID  int    `db:"book_id"`
	Chapter int    `db:"chapter"`
	Number  int    `db:"number"`
	Words   string `db:"words"`
}
