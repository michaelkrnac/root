package models

// ThingWord defines the structur of the thing to word reference table
type ThingWord struct {
	WordID  int `db:"word_id"`
	ThingID int `db:"thing_id"`
}
