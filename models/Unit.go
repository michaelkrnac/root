package models

// Unit represents the unit table
type Unit struct {
	UnitID              int     `db:"unit_id"`
	Name                string  `db:"name"`
	Description         *string `db:"description"`
	VisuellidentifierID *int    `db:"visuellidentifier_id"`
	UnitTypeID          *int    `db:"unit_type_id"`
	UnitTypeName        *string `db:"unit_type_name"`
}
