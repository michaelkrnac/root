const path = require('path');
const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
	mode: "development", 
	entry: {
		home: './webpack/home/index.js',
		bible: './webpack/bible/index.js',
		book: './webpack/book/index.js',
		bookdetail: './webpack/book/detail.js',
		collections: './webpack/collections/index.js',
		chapter: './webpack/chapter/index.js',
		snippet: './webpack/snippet/index.js',
		snippetng: './webpack/snippetng/index.js',
		personlist: './webpack/person/list.js',
		persondetail: './webpack/person/detail.js',
		personcreate: './webpack/person/create.js',
		animallist: './webpack/animal/list.js',
		animaldetail: './webpack/animal/detail.js',
		animalcreate: './webpack/animal/create.js',
		placelist: './webpack/place/list.js',
		placedetail: './webpack/place/detail.js',
		placecreate: './webpack/place/create.js',
		tribelist: './webpack/tribe/list.js',
		tribedetail: './webpack/tribe/detail.js',
		tribecreate: './webpack/tribe/create.js',
		plantlist: './webpack/plant/list.js',
		plantdetail: './webpack/plant/detail.js',
		plantcreate: './webpack/plant/create.js',
		materiallist: './webpack/material/list.js',
		materialdetail: './webpack/material/detail.js',
		materialcreate: './webpack/material/create.js',
		instrumentlist: './webpack/instrument/list.js',
		instrumentdetail: './webpack/instrument/detail.js',
		instrumentcreate: './webpack/instrument/create.js',
		unitlist: './webpack/unit/list.js',
		unitdetail: './webpack/unit/detail.js',
		unitcreate: './webpack/unit/create.js',
		thinglist: './webpack/thing/list.js',
		thingdetail: './webpack/thing/detail.js',
		thingcreate: './webpack/thing/create.js',
		mapsallplaces: './webpack/mapsallplaces/index.js',
		mapsfamilytree: './webpack/mapsfamilytree/index.js',
	},
	output: {
		filename: '[name].js',
		path: path.resolve(__dirname, './resources/static/dist')
	},
	module: {
		rules: [
			{
				test: /\.css$/,
				use: [
					'style-loader',
					'css-loader'
				]
			},
			{
				test: /\.scss$/,
				use: [
					'style-loader',
					'css-loader',
					'sass-loader'
				]
			},
			{
				test: /\.vue$/,
				use: [
					'vue-loader'
				]
			}
		]
	},
	plugins: [
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery',
			Popper: ['popper.js', 'default']
		}),
	/*	new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: '"production"'
			}
		}),
		new UglifyJsPlugin()*/
	],
	resolve: {
		alias: {
			vue: 'vue/dist/vue.js'
		}
	}
};
