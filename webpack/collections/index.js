import '../style.scss';
import '../style.css';
import 'bootstrap';
import Vue from 'vue';
import * as toastr from 'toastr';
import collections from '../components/collections.vue'
import '../w3-theme-blue-grey.css';
import 'w3-css/w3.css';



toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-bottom-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

var vm = new Vue({
	el: '#collections',
	components: {
		'collections': collections
	}
});

