import '../style.scss';
import '../style.css';
import 'bootstrap';
import Vue from 'vue';
import VueResource from 'vue-resource';
import vSelect from "vue-select"
import * as alertify from 'alertifyjs';
import word from '../components/word.vue'
import '../w3-theme-blue-grey.css';
import 'w3-css/w3.css';



Vue.use(VueResource);

var update = new Vue({
	delimiters: ['${', '}'],
	el: '#animalDetail',
	components: {
		'word': word,
		'vSelect': vSelect
	},
	data: {
		animal: {},
		formvisible: false,
		animalclasses: [],
		animalclass: null,
		visuellidentifiers: [],
		visuellidentifier: null

	},
	mounted: function() {
		this.animal = $.extend(true, {}, startanimal);
	},
	methods: {
		toggle: function (event) {
			this.formvisible = !this.formvisible;
			if (this.formvisible) {
				this.$nextTick(() => this.$refs.name.focus());
					
				// load animal clases
				this.$http.get('/api/animalclass').then(response => {
					this.animalclasses = response.body;
				}, response => {
					alertify.error(response.body.Msg);
				});
				
				// load visuellidentifiers
				this.$http.get('/api/visuellidentifier').then(response => {
					this.visuellidentifiers = response.body;
					
					this.visuellidentifiers.forEach( function (v){
						v.imageurl = '/static/img/vi/'+v.VisuellidentiferID+'.svg'
					});
				}, response => {
					alertify.error(response.body.Msg);
				});
			}
		},
		askdel: function (event) {
			var _this = this;
			alertify.confirm(
				"Tier \"" + _this.animal.Name + "\" löschen?", 
				"Bist du dir sicher, dass du das Tier \"" + _this.animal.Name + "\" löschen möchtest?", 
				function(){
					_this.del();
				}, 
				function(){
					alertify.warning('Löschen von Tier "' + _this.animal.Name + '" abgegbrochen!')
				}
			)
		},
		del: function (event) {
			this.$http.post('/api/animal/delete', this.animal).then (response => {
				alertify.notify(response.body.Msg, 'success', 2, function(){
					window.location.replace("/animal");
				})
			}, response => {
				alertify.error(response.body.Msg)
			});
		},
		cancel: function (event) {
			this.$http.get('/api/animal/select/'+this.animal.AnimalID, this.animal).then (response => {
				this.toggle();
				this.animal = response.body;
				alertify.warning("Abgebrochen!")
			}, response => {
				alertify.error(response.body.Msg);
			});
		},
		save: function (event) {
			this.$http.post('/api/animal/update', this.animal).then (response => {
				this.toggle();
				alertify.success(response.body.Msg)
			}, response => {
				alertify.error(response.body.Msg)
			});
		}
	},
	watch: {
		'animal.Name': function(val,oldval){
			document.title = this.animal.Name;
			$("li.breadcrumb-item.active").text(this.animal.Name);
		},
		animalclass: function(val){
			this.animal.ClassID = this.animalclass.ClassID;
			this.animal.ClassName = this.animalclass.Name;
		},
		visuellidentifier: function(val){
			this.animal.VisuellidentifierID = this.visuellidentifier.VisuellidentiferID;
		}

	}
});
