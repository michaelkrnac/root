import '../style.scss';
import '../style.css';
import 'bootstrap';
import Vue from 'vue';
import VueResource from 'vue-resource'
import animallist from '../components/animallist.vue'
import '../w3-theme-blue-grey.css';
import 'w3-css/w3.css';

Vue.use(VueResource);

var avm = new Vue({
	el: '#animals',
	components: {
		'animallist': animallist
	}
});


