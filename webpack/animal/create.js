import '../style.scss';
import '../style.css';
import 'bootstrap';
import Vue from 'vue';
import VueResource from 'vue-resource';
import vSelect from "vue-select"
import * as alertify from 'alertifyjs';
import '../w3-theme-blue-grey.css';
import 'w3-css/w3.css';



Vue.use(VueResource);

var create = new Vue({
	delimiters: ['${', '}'],
	el: '#createForm',
	components: {vSelect},
	data: {
		animal: {},
		animalclasses: [],
		animalclass: null,
		visuellidentifiers: [],
		visuellidentifier: null
	},
	mounted: function() {
		this.animal = $.extend(true, {}, startanimal);
		this.$refs.name.focus();
		
		// load animal clases
		this.$http.get('/api/animalclass').then(response => {
			this.animalclasses = response.body;
		}, response => {
			alertify.error(response.body.Msg);
		});
		
		// load visuellidentifiers
		this.$http.get('/api/visuellidentifier').then(response => {
			this.visuellidentifiers = response.body;
			
			this.visuellidentifiers.forEach( function (v){
				v.imageurl = '/static/img/vi/'+v.VisuellidentiferID+'.svg'
			});
		}, response => {
			alertify.error(response.body.Msg);
		});


	},
	methods: {
		cancel: function (event) {
			window.location.replace("/animal");
		},
		save: function (event) {
			this.$http.post('/api/animal/insert', this.animal).then (response => {
				window.location.replace("/animal");
			}, response => {
				alertify.error(response.body.Msg)
			});
		}
	},
	watch: {
		animalclass: function(val){
			this.animal.ClassID = this.animalclass.ClassID;
		},
		visuellidentifier: function(val){
			this.animal.VisuellidentifierID = this.visuellidentifier.VisuellidentiferID;
		}
	}

})
