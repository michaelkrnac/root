export function makeDelay(ms) {
    var timer = 0;
    return function(callback){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
};

export function sendPayload(url, payload, success, error) {
	var xhr = new XMLHttpRequest();
	xhr.open('POST', url);
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.onload = function() {	
		var data = JSON.parse(xhr.responseText);
		if (xhr.status === 200) {
			success(data);
		} else {
			error(data);
		}	
	};
	xhr.send(JSON.stringify(payload));
}

export function getJSONFromRemote(url, success, error) {
	var xhr = new XMLHttpRequest();
	xhr.open('GET', url);
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.onload = function() {	
		var data = JSON.parse(xhr.responseText);
		if (xhr.status === 200) {
			success(data);
		} else {
			error(data);
		}	
	};
	xhr.send();
}
