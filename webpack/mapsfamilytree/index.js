import '../style.css';
import '../w3-theme-blue-grey.css';
import 'w3-css/w3.css';
import * as d3 from "d3";

var persons = null;
var references = [];


// LOAD DATA
$.ajax({
	url: '/api/person',
	contentType: 'application/json',
	success: function(data){
		$.each(data, function(index, d) {
		});
		persons=data;
		$.ajax({
			url: '/api/person/references',
			contentType: 'application/json',
			success: function(data){
				$.each(data, function(index, d) {
					references.push({
						source: d.StartPerson,
						target: d.TargetPerson,
						referencetype: d.ReferenceType
					});
				});
				draw();
			}	
		});
	}	
});

// DRAW DIAGRAMM
function draw()  {
var container = document.getElementById("container"); // get the div container where we render everything into

var width = container.clientWidth;      // get the width of the container which is equal to the window with
var height = container.clientHeight;    // get the height of the container which is equal to the window height

// create the svg in the size of the window
var svg = d3.select(container)
    .append("svg")
    .attr("width", width)
    .attr("height", height)

var view = svg.append("g")
    .attr("class", "view");

// ****************************************************
// ZOOM
// define the zoom functionality
// ****************************************************
var zoom = d3.zoom()
    .on("zoom", zoomed);

function zoomed() {
    view.attr("transform", d3.event.transform);
}

svg.call(zoom);
svg.on("dblclick.zoom", null);
zoom.scaleTo(svg, 0.5);

// ****************************************************
// SVG DEFINITIONS
// define global svg stuff like markers
// ****************************************************

var defs = svg.append('defs');

defs.append('marker')
        .attr('id', 'marker')
        .attr('viewBox','-5 -5 10 10')
        .attr('refX',0)
        .attr('refY',0)
        .attr('orient','auto')
        .attr('markerWidth',5)
        .attr('markerHeight',5)
        .attr('xoverflow','visible')
        .append('svg:path')
            .attr('d', 'M 0,0 m -5,-5 L 5,0 L -5,5 Z')
            .attr('fill', "#000")

// ****************************************************
// FORCE SIMULATION CONFIUGRATION
// set up the force simulation
// ****************************************************
var forceCollide = d3.forceCollide()
    .radius(function(d) {
        var n = d3.select("#id_"+d.PersonID).node();
		var h = n.getBBox().height;
		var w = n.getBBox().width;
		// calculate diagonale and use the half + offset
		return (Math.sqrt(Math.pow(h,2)+Math.pow(w,2))/2)+50
    });

var simulation = d3.forceSimulation()        
    .velocityDecay(0.3)
    .force("charge", d3.forceManyBody())  
    //.force("center", d3.forceCenter(width /2, height /2))
    //.force("x", d3.forceX().strength(0.1))
    //.force("y", d3.forceY().strength(0.1))
    .force("collide", forceCollide)     
	.force("link", d3.forceLink().id(function(d) { return d.PersonID; }).distance(1).strength(1.5))  

var reference = view
    .append("g")
    .attr("class", "references")
    .selectAll(".reference")
    .data(references)
    .enter()
    .append("path")
    .attr("class", "reference")  
    .attr("id", function(d){return "id_"+d.StartPerson+"---"+d.TargetPerson})
    .style("stroke", "#000")
    .attr("fill", "none")
	.attr('marker-mid', "url(#marker)")

// add group for each person
var person = view
	.append("g")
	.attr("class", "persons")
	.selectAll(".person")
	.data(persons)
	.enter().append("g")
	.attr("class", "person")
	.attr("id", function(d){return "id_"+d.PersonID;})
	.on('click', function(d){})

// add circle to center of entity
var circle = person.append("circle")
    .attr("r", 5)		
    .attr("fill", "#000")

// add rectangle to entity
var rect = person.append("rect")
    .attr("rx",2)
    .attr("ry",2)
    .attr("fill", function(d){
		if (d.Gender == "männlich")
			return "#92D7EF"
		if (d.Gender == "weiblich")
			return "#F9ACBB"
		if (d.Gender === null)
			return "#FFF"
	})
    .attr("stroke", "#000")

// add the title
var title = person.append("a")
    .attr("xlink:href", function(d){return "/person/" + d.PersonID})
    .attr("target", "_blank")
    .append("text")
    .attr("x", 36)
    .attr("y", 23)
    .attr("class", "entitytitle")
    .text(function(d) { return d.Name })

// recalculate the size of the rectangle
person.selectAll('rect')
    .attr("width", function(d) {return this.parentNode.getBBox().width +36;})
    .attr("height", function(d) {return this.parentNode.getBBox().height + 20;})

// recalculate the position of the circle
person.selectAll("circle")
    .attr("cx", function(d) {return this.parentNode.getBBox().width/2-2.5;})
    .attr("cy", function(d) {return this.parentNode.getBBox().height/2-2.5;})

simulation.nodes(persons)
simulation.force("link").links(references)
simulation.on("tick", ticked);

function ticked() {

	reference.attr("d", function(d) {
        // Total difference in x and y from source to target
       let diffX = d.target.x - d.source.x;
       let diffY = d.target.y - d.source.y;
		
	let sourceCenterX = d.source.x + d3.select("#id_"+d.source.PersonID).node().getBBox().width/2;
	let sourceCenterY = d.source.y + d3.select("#id_"+d.source.PersonID).node().getBBox().height/2;
	let targetCenterX = d.target.x + d3.select("#id_"+d.target.PersonID).node().getBBox().width/2;
	let targetCenterY = d.target.y + d3.select("#id_"+d.target.PersonID).node().getBBox().height/2;

	    // Length of path from center of source node to center of target node
	    let pathLength = Math.sqrt((diffX * diffX) + (diffY * diffY));
	    // x and y distances from center to outside edge of target node
	    let offsetX = (diffX * d3.select("#id_"+d.target.PersonID).node().getBBox().width) / pathLength;
	    let offsetY = (diffY * d3.select("#id_"+d.target.PersonID).node().getBBox().height) / pathLength;
	    var lineData = [ 
	        { "x": sourceCenterX ,   "y": sourceCenterY },  
		{ "x": (d.target.x-offsetX) ,   "y": (d.target.y-offsetY) },  
		{ "x":  targetCenterX,  "y":  targetCenterY }  
	    ];
 			
        var lineFunction = d3.line()
            .x(function(d) { return d.x; })
            .y(function(d) { return d.y; })
	    .curve(d3.curveBasis);
	return lineFunction(lineData)
    });
    person.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
}

}

