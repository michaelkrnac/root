export var objectTypes = [
	{
		type: "person",
		url: "/person/create",
		name: "Person",
		icon: "/static/img/vi/3.svg"
	},
	{
		type: "animal",
		url: "/animal/create",
		name: "Tier",
		icon: "/static/img/vi/4.svg"
	},
	{
		type: "place",
		url: "/place/create",
		name: "Ort",
		icon: "/static/img/vi/5.svg"
	},
	{
		type: "tribe",
		url: "/tribe/create",
		name: "Stamm",
		icon: "/static/img/vi/22.svg"
	},
	{
		type: "plant",
		url: "/plant/create",
		name: "Pflanze",
		icon: "/static/img/vi/37.svg"
	},
	{
		type: "material",
		url: "/material/create",
		name: "Material",
		icon: "/static/img/vi/38.svg"
	},
	{
		type: "instrument",
		url: "/instrument/create",
		name: "Instrument",
		icon: "/static/img/vi/40.svg"
	},
	{
		type: "unit",
		url: "/unit/create",
		name: "Maßeinheit",
		icon: "/static/img/vi/39.svg"
	},
	{
		type: "thing",
		url: "/thing/create",
		name: "Objekt",
		icon: "/static/img/vi/41.svg"
	}
];

