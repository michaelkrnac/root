import '../style.css';
import '../w3-theme-blue-grey.css';
import * as toastr from 'toastr';
import 'w3-css/w3.css';
import * as loni from '../loni.js'
import * as wurzel from '../wurzel.js'

toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-bottom-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "1500",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

var snippets = {
	objectTypes : [],
	activeobject: 0,
	objects: [],
	active: 0,
	todos: [],
	done: [],
	init: function(snippets) {
		this.setEvents();
		this.todos = snippets;
		this.renderProgress();
		this.renderSnippet(this.todos[this.active])
		document.querySelector("#search").focus();
	},
	loadFromServer: function() {
		var xhr = new XMLHttpRequest();
		xhr.open('GET', '/api/snippet');
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.onload = function() {
			if (xhr.status === 200) {
				snippets.init(JSON.parse(xhr.responseText));
			} else {
				toastr.error('Ich konnte die Auschnitte nicht laden!', 'Fehler!');
			}
		};
		xhr.send();
	},
	setEvents: function() {
		document.querySelector("#bt-next").addEventListener('click', function (){
			snippets.nextSnippet();
		});

		document.querySelector("#bt-prev").addEventListener('click', function (){
			snippets.prevSnippet();
		});

		document.querySelector("#autocomplete").addEventListener('keyup', function (e){
			switch (e.keyCode) {
				case 40: //down
					snippets.nextObject();
					break;
				case 38: //up
					snippets.prevObject();
					break;
				case 27: //ESC
					snippets.clearsearch();
					break;
				case 13: //Enter
					snippets.connect(e.target.id, e.target.childNodes[1].classList[3]);
					break;
			}
		});

		document.querySelector("#autocomplete").addEventListener('click', function (e){
			snippets.connect(e.target.parentElement.id, e.target.classList[3]);
		});

		document.querySelector("#search").addEventListener('keyup', function (e){
			switch (e.keyCode) { 
				case 38: //up
					// do nothing to prevent form reloading
					break;
				case 40: //down
					if (e.ctrlKey){
						var s = document.querySelector("#search");
						s.value = document.querySelector(".snippetword").textContent; 
						snippets.search(s.value);

					} else {
						snippets.firstObject();
					}
					break;
				case 27: //ESC
					snippets.clearsearch();
					break;
				default:
					var query = document.querySelector("#search").value;
					snippets.search(query);
					break;
			}
		});

		window.onkeydown = function(e) {
			switch (e.keyCode) {
				case 37: //left
					snippets.prevSnippet();
					break;
				case 39: //right
					snippets.nextSnippet();
					break;
			}
		};
	},
	renderProgress: function() {
		var todocount = this.todos.length;
		var donecount = this.done.length;
		var allcount = todocount + donecount;
		
		if (allcount === 0)
			var percent = 0;
		else
			var percent = Math.round(donecount / (allcount/100));

		var pb = document.querySelector(".progress-bar");
		pb.style.width = percent+"%";
		pb.innerHTML = percent+"%";
		
		var pc = document.querySelector(".progress-count");
		pc.innerHTML = donecount + " von " + allcount;
	},
	renderSnippet: function(s) {
		var word = document.querySelector(".snippetword");
		word.innerHTML = s.Word;
		word.setAttribute("id", s.WordID);
		document.querySelector("#vers").innerHTML = this.highlightWord(s);	
		var verslink = document.querySelector("#verslink");
		verslink.innerHTML = s.BookName + " " + s.Chapter + "," + s.VersNumber;
		verslink.href = "/bible/book/" + s.BookID + "/chapter/" + s.Chapter+"#"+s.VersID;
		var search = document.querySelector("#search");
		search.value = '';
		search.focus();
	},
	nextSnippet: function(){
		if (this.todos.length === 0) {
			var element = document.getElementById("activesnippet");	
			element.parentNode.removeChild(element);
		} else {
			if (this.active == this.todos.length - 1) {
				this.active = 0;
			} else {
				this.active++ ;
			}
			this.renderSnippet(this.todos[this.active]);
		}
	},
	prevSnippet: function(){
		if (this.active == 0) {
			this.active = this.todos.length - 1;
		} else {
			this.active--;
		}
		this.renderSnippet(this.todos[this.active]);	
	},
	activeSnippetDone: function(){
		this.done.push(this.todos[this.active]);
		this.todos.splice(this.active, 1);
		this.active--;
		this.activeobject = 0;
		this.renderProgress();
		this.nextSnippet();
		this.clearsearch();
	},
	search: function(query){
		if (query.length >=2) {

			var search = {Query:query};
			
			loni.sendPayload(
				'/api/snippet/object', 
				search, 
				function(data){
					snippets.setObjects(data);
					snippets.activeobject = 0;
					var ac = document.getElementById("autocomplete");	
					ac.innerHTML = "";
					var t = document.getElementById('listli');
					for (var i = 0, len = data.length; i < len; i++) {
						snippets.renderObject(ac, t, data[i]);					
					}
					snippets.renderNewLinks(ac, t);
				}, 
				function(data){
					toastr.error(data.Msg, 'Fehler!');
				}
			);
		} else {
			snippets.removeObjects();
			var ac = document.getElementById("autocomplete");	
			ac.innerHTML = "";
		}
	},
	clearsearch: function() {
		var s = document.getElementById("search");	
		s.value = "";
		s.focus();
		var ac = document.getElementById("autocomplete");	
		ac.innerHTML = "";
	},
	renderObject: function(ul, t, d) {
		var c = document.importNode(t.content, true);
		c.querySelector("li").setAttribute("id", d.ID);
						
		var oname = c.querySelector(".oname");
		oname.innerHTML = d.Name;
		oname.classList.add(d.Type);
						
		var otag = c.querySelector(".otag");
		otag.innerHTML = d.TypeName;
		otag.classList.add(d.Type + "-bg");
		
		ul.appendChild(c);
	},
	renderNewLinks: function(ul, t) {
		for (var i = 0, len = this.objectTypes.length; i < len; i++) {
			snippets.renderLink(ul, t, this.objectTypes[i]);					
		}
	},
	renderLink(ul, t, l) {
		var c = document.importNode(t.content, true);
		c.querySelector("li").setAttribute("id", l.type);
						
		var oname = c.querySelector(".oname");
		oname.innerHTML = l.name + " neu anlegen";
		oname.classList.add("link");
						
		var otag = c.querySelector(".otag");
		otag.innerHTML = l.name;
		otag.classList.add(l.type + "-bg");
		
		ul.appendChild(c);
	},
	firstObject: function() {
		var lis = document.querySelectorAll("#autocomplete > li");
		lis[this.activeobject].classList.add("w3-light-grey");
		lis[this.activeobject].focus();
	},
	nextObject: function() {
		var lis = document.querySelectorAll("#autocomplete > li");
		if (this.activeobject == this.objects.length + this.objectTypes.length - 1) { 
			var s = document.getElementById("search");
			s.focus();				
			s.scrollIntoView({
				behavior: 'auto',
				block: 'center',
				inline: 'center'
			});
			lis[this.activeobject].classList.remove("w3-light-grey");
			this.activeobject = 0;
		} else {
			lis[this.activeobject].classList.remove("w3-light-grey");
			this.activeobject++;
			lis[this.activeobject].classList.add("w3-light-grey");
			lis[this.activeobject].focus();
		}
	},
	prevObject: function() {
		var lis = document.querySelectorAll("#autocomplete > li");
		if (this.activeobject === 0) { 
			var s = document.getElementById("search");
			s.focus();				
			lis[this.activeobject].classList.remove("w3-light-grey");
		} else {
			lis[this.activeobject].classList.remove("w3-light-grey");
			this.activeobject--;
			lis[this.activeobject].classList.add("w3-light-grey");
			lis[this.activeobject].focus();
		}
	},
	connect: function(objectid, type) {
		var word = document.querySelector(".snippetword");
		switch(type) {
			case "person":
				var payload = {
					'PersonID': parseInt(objectid),
					'WordID': parseInt(word.id)
				};
				
				loni.sendPayload(
					'/api/person/word/insert', 
					payload, 
					function(data){
						toastr.success(data.Msg, 'Gespeichert!');
						snippets.activeSnippetDone();
					}, 
					function(data){
						toastr.error(data.Msg, 'Fehler!');
					}
				);
				break;
			case "animal":
				var payload = {
					'AnimalID': parseInt(objectid),
					'WordID': parseInt(word.id)
				};
				
				loni.sendPayload(
					'/api/animal/word/insert', 
					payload, 
					function(data){
						toastr.success(data.Msg, 'Gespeichert!');
						snippets.activeSnippetDone();
					}, 
					function(data){
						toastr.error(data.Msg, 'Fehler!');
					}
				);
				break;
			case "place":
				var payload = {
					'PlaceID': parseInt(objectid),
					'WordID': parseInt(word.id)
				};
				
				loni.sendPayload(
					'/api/place/word/insert', 
					payload, 
					function(data){
						toastr.success(data.Msg, 'Gespeichert!');
						snippets.activeSnippetDone();
					}, 
					function(data){
						toastr.error(data.Msg, 'Fehler!');
					}
				);
				break;
			case "tribe":
				var payload = {
					'TribeID': parseInt(objectid),
					'WordID': parseInt(word.id)
				};
				
				loni.sendPayload(
					'/api/tribe/word/insert', 
					payload, 
					function(data){
						toastr.success(data.Msg, 'Gespeichert!');
						snippets.activeSnippetDone();
					}, 
					function(data){
						toastr.error(data.Msg, 'Fehler!');
					}
				);
				break;
			case "plant":
				var payload = {
					'PlantID': parseInt(objectid),
					'WordID': parseInt(word.id)
				};
				
				loni.sendPayload(
					'/api/plant/word/insert', 
					payload, 
					function(data){
						toastr.success(data.Msg, 'Gespeichert!');
						snippets.activeSnippetDone();
					}, 
					function(data){
						toastr.error(data.Msg, 'Fehler!');
					}
				);
				break;
			case "material":
				var payload = {
					'MaterialID': parseInt(objectid),
					'WordID': parseInt(word.id)
				};
				
				loni.sendPayload(
					'/api/material/word/insert', 
					payload, 
					function(data){
						toastr.success(data.Msg, 'Gespeichert!');
						snippets.activeSnippetDone();
					}, 
					function(data){
						toastr.error(data.Msg, 'Fehler!');
					}
				);
				break;
			case "instrument":
				var payload = {
					'InstrumentID': parseInt(objectid),
					'WordID': parseInt(word.id)
				};
				
				loni.sendPayload(
					'/api/instrument/word/insert', 
					payload, 
					function(data){
						toastr.success(data.Msg, 'Gespeichert!');
						snippets.activeSnippetDone();
					}, 
					function(data){
						toastr.error(data.Msg, 'Fehler!');
					}
				);
				break;
			case "unit":
				var payload = {
					'UnitID': parseInt(objectid),
					'WordID': parseInt(word.id)
				};
				
				loni.sendPayload(
					'/api/unit/word/insert', 
					payload, 
					function(data){
						toastr.success(data.Msg, 'Gespeichert!');
						snippets.activeSnippetDone();
					}, 
					function(data){
						toastr.error(data.Msg, 'Fehler!');
					}
				);
				break;
			case "thing":
				var payload = {
					'ThingID': parseInt(objectid),
					'WordID': parseInt(word.id)
				};
				
				loni.sendPayload(
					'/api/thing/word/insert', 
					payload, 
					function(data){
						toastr.success(data.Msg, 'Gespeichert!');
						snippets.activeSnippetDone();
					}, 
					function(data){
						toastr.error(data.Msg, 'Fehler!');
					}
				);
				break;
			case "link":
				var result = this.objectTypes.filter(obj => {
					return obj.type === objectid
				})
				window.open(result[0].url, '_blank');
				break;
		}
	},
	highlightWord: function (snippet){
		var before_tag = '<span class ="w3-yellow" style="font-weight:bold;">';
		var after_tag = '</span>';
  
		var span = before_tag + snippet.Word + after_tag;
		var reg = "(^.{" + (snippet.Position - 1) + "})(.{" + snippet.Length + "})(.*$)";
		var regex = new RegExp(reg, "g");
		var match = regex.exec(snippet.Words);
		var vers_new = match[1] + span + match[3]; 
		return vers_new;
	},
	setObjects: function (data) {
		this.objects = data;
	},
	removeObjects: function () {
		this.objects = []
	}
};

snippets.objectTypes = wurzel.objectTypes;
snippets.loadFromServer();
var delay = loni.makeDelay(250);
