import '../style.scss';
import '../style.css';
import 'bootstrap';
import Vue from 'vue';
import VueResource from 'vue-resource';
import * as alertify from 'alertifyjs';
import word from '../components/word.vue'
import Multiselect from 'vue-multiselect'
import 'vue-multiselect/dist/vue-multiselect.min.css'
import '../w3-theme-blue-grey.css';
import 'w3-css/w3.css';



Vue.use(VueResource);

var update = new Vue({
	delimiters: ['${', '}'],
	el: '#personDetail',
	components: {
		'word': word,
		Multiselect
	},
	data: {
		person: {},
		formvisible: false,
		persons: [],
		mother: null,
		father: null,
	},
	mounted: function() {
		this.person = $.extend(true, {}, startperson);
		
		// load persons
		this.$http.get('/api/person').then(response => {
			this.persons = response.body;
		}, response => {
			alertify.error(response.body.Msg);
		});
		
		// load father
		this.$http.post('/api/person/father', this.person).then(response => {
			this.father = response.body;
		}, response => {
			alertify.error(response.body.Msg);
		});
		
		// load mother
		this.$http.post('/api/person/mother', this.person).then(response => {
			this.mother = response.body;
		}, response => {
			alertify.error(response.body.Msg);
		});

	},
	methods: {
		toggle: function (event) {
			this.formvisible = !this.formvisible;
			if (this.formvisible)
				this.$nextTick(() => this.$refs.name.focus());
		},
		askdel: function (event) {
			var _this = this;
			alertify.confirm(
				"Person \"" + _this.person.Name + "\" löschen?", 
				"Bist du dir sicher, dass du die Person \"" + _this.person.Name + "\" löschen möchtest?", 
				function(){
					_this.del();
				}, 
				function(){
					alertify.warning('Löschen von Person "' + _this.person.Name + '" abgegbrochen!')
				}
			)
		},
		del: function (event) {
			this.$http.post('/api/person/delete', this.person).then (response => {
				alertify.notify(response.body.Msg, 'success', 2, function(){
					window.location.replace("/person");
				})
			}, response => {
				alertify.error(response.body.Msg)
			});
		},
		cancel: function (event) {
			this.$http.get('/api/person/select/'+this.person.PersonID, this.person).then (response => {
				this.toggle();
				this.person = response.body;
				alertify.warning("Abgebrochen!")
			}, response => {
				alertify.error(response.body.Msg);
			});
		},
		save: function (event) {
			this.$http.post('/api/person/update', this.person).then (response => {
				this.toggle();
				alertify.success(response.body.Msg)
			}, response => {
				alertify.error(response.body.Msg)
			});
		},
		connectfather: function(selected){
			let payload = {
				'ChildrenID': this.person.PersonID,
				'FatherID': selected.PersonID
			};

			// store father
			this.$http.post('/api/person/father/insert', payload).then(response => {
				alertify.success(response.body.Msg);
			}, response => {
				alertify.error(response.body.Msg);
			});

		},
		removefather: function(removed){
			let payload = {
				'ChildrenID': this.person.PersonID,
				'FatherID': removed.PersonID
			};
			
			// delete father
			this.$http.post('/api/person/father/delete', payload).then(response => {
				alertify.success(response.body.Msg);
			}, response => {
				alertify.error(response.body.Msg);
			});
		},
		connectmother: function(selected){
			let payload = {
				'ChildrenID': this.person.PersonID,
				'MotherID': selected.PersonID
			};

			// store mother
			this.$http.post('/api/person/mother/insert', payload).then(response => {
				alertify.success(response.body.Msg);
			}, response => {
				alertify.error(response.body.Msg);
			});

		},
		removemother: function(removed){
			let payload = {
				'ChildrenID': this.person.PersonID,
				'MotherID': removed.PersonID
			};
			
			// delete father
			this.$http.post('/api/person/mother/delete', payload).then(response => {
				alertify.success(response.body.Msg);
			}, response => {
				alertify.error(response.body.Msg);
			});
		}

	},
	watch: {
		'person.Gender': function(val, oldval){
			switch (val) {
				case '':
					this.person.VisuellidentifierID = 3;
					break;
				case 'männlich':
					this.person.VisuellidentifierID = 6; 
					break;
				case 'weiblich':
					this.person.VisuellidentifierID = 7; 
					break;
			}
		},
		'person.Name': function(val,oldval){
			document.title = this.person.Name;
			$("li.breadcrumb-item.active").text(this.person.Name);
		}
	}
});
