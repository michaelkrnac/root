import '../style.scss';
import '../style.css';
import 'bootstrap';
import Vue from 'vue';
import VueResource from 'vue-resource';
import * as alertify from 'alertifyjs';
import '../w3-theme-blue-grey.css';
import 'w3-css/w3.css';



Vue.use(VueResource);

var create = new Vue({
	delimiters: ['${', '}'],
	el: '#createForm',
	data: {
		person: {}
	},
	mounted: function() {
		this.person = $.extend(true, {}, startperson);
		this.$refs.name.focus();
	},
	methods: {
		cancel: function (event) {
			window.location.replace("/person");
		},
		save: function (event) {
			this.$http.post('/api/person/insert', this.person).then (response => {
				window.location.replace("/person");
			}, response => {
				alertify.error(response.body.Msg)
			});
		}
	},
})
