import '../style.css';
import '../w3-theme-blue-grey.css';
import 'w3-css/w3.css';
import 'chart.js';
import * as loni from '../loni.js'
import * as wurzel from '../wurzel.js'

var atBox = document.getElementById("at");
var ntBox = document.getElementById("nt");
loni.getJSONFromRemote('/api/books', function(data){
	var ot = data.filter(book => book.Testament == 'ot');
	ot.forEach((e, key, arr) => {
		var a = document.createElement('a');
		var linkText = document.createTextNode(e.Name);
		a.appendChild(linkText);
		a.href = "/bible/book/"+e.BookID+"/chapter/1";
		atBox.appendChild(a);
		
		if (!Object.is(arr.length - 1, key)) {
			var c = document.createTextNode(", ");
			atBox.appendChild(c);
		}

	});
	var nt = data.filter(book => book.Testament == 'nt');
	nt.forEach((e, key, arr) => {
		var a = document.createElement('a');
		var linkText = document.createTextNode(e.Name);
		a.appendChild(linkText);
		a.href = "/bible/book/"+e.BookID+"/chapter/1";
		ntBox.appendChild(a);
		
		if (!Object.is(arr.length - 1, key)) {
			var c = document.createTextNode(", ");
			ntBox.appendChild(c);
		}
	});
});

var t = document.querySelector('#objectrow');
var ot = document.querySelector("#objecttable");

loni.getJSONFromRemote('/api/bible/objectscount', function(data){
	wurzel.objectTypes.forEach((e, key, arr) => {
		var count = data.filter(c => c.Value == e.type)[0];
		var clone = document.importNode(t.content, true);
		var td = clone.querySelectorAll("td");
		td[0].querySelector('img').src = e.icon;
		td[1].textContent = e.name;
		td[1].classList.add(e.type);
		td[2].textContent = count.Count;
		ot.appendChild(clone);
	});
});

var ctx = document.getElementById("personChart");
var personChart = new Chart(ctx, {
    type: 'bar',
    data: {
        datasets: [{
            label: 'Anzahl der Nennungen',
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true,
                }
            }],
			xAxes: [{
                ticks: {
					autoSkip: false
                }
            }]

        }
    }
});

loni.getJSONFromRemote('/api/bible/personwordcount', function(data){
	data.forEach((e, key, array) => {
		personChart.data.labels.push(e.Value);
		personChart.data.datasets[0].data.push(e.Count);
	});
    personChart.update();
});

var ctx2 = document.getElementById("ausschnitteChart");
var ausschnitteChart = new Chart(ctx2, {
    type: 'bar',
    data: {
        datasets: [{
            label: 'Ausschnitte pro Buch',
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});

loni.getJSONFromRemote('/api/bible/bookwordcount', function(data){
	data.forEach((e, key, array) => {
		ausschnitteChart.data.labels.push(e.Value);
		ausschnitteChart.data.datasets[0].data.push(e.Count);
	});
    ausschnitteChart.update();
});

loni.getJSONFromRemote('/api/bible/wordcount', function(data){
	data.forEach((e, key, array) => {
		document.getElementById(e.Value).innerHTML = e.Count;
	});
});

loni.getJSONFromRemote('/api/bible/collectioncount', function(data){
	data.forEach((e, key, array) => {
		collectionChart.data.labels.push(e.Value);
		collectionChart.data.datasets[0].data.push(e.Count);
	});
    collectionChart.update();
});

var ctx2 = document.getElementById("collectionChart");
var collectionChart = new Chart(ctx2, {
    type: 'bar',
    data: {
        datasets: [{
            label: 'Bücher in Sammlung',
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }],
			xAxes: [{
                ticks: {
					autoSkip: false
                }
            }]
        }
    }
});

