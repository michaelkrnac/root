import '../style.scss';
import '../style.css';
import 'bootstrap';
import Vue from 'vue';
import VueResource from 'vue-resource';
import vSelect from "vue-select"
import * as alertify from 'alertifyjs';
import word from '../components/word.vue'
import '../w3-theme-blue-grey.css';
import 'w3-css/w3.css';



Vue.use(VueResource);

var update = new Vue({
	delimiters: ['${', '}'],
	el: '#placeDetail',
	components: {
		'word': word,
		'vSelect': vSelect
	},
	data: {
		place: {},
		formvisible: false,
		placetypes: [],
		placetype: null,

	},
	mounted: function() {
		this.place = $.extend(true, {}, startplace);
	},
	methods: {
		toggle: function (event) {
			this.formvisible = !this.formvisible;
			if (this.formvisible) {
				this.$nextTick(() => this.$refs.name.focus());
			
				// load place types
				this.$http.get('/api/placetype').then(response => {
					this.placetypes = response.body;
				}, response => {
					alertify.error(response.body.Msg);
				});
			}
		},
		askdel: function (event) {
			var _this = this;
			alertify.confirm(
				"Ort \"" + _this.place.Name + "\" löschen?", 
				"Bist du dir sicher, dass du den Ort \"" + _this.place.Name + "\" löschen möchtest?", 
				function(){
					_this.del();
				}, 
				function(){
					alertify.warning('Löschen von Ort "' + _this.place.Name + '" abgegbrochen!')
				}
			)
		},
		del: function (event) {
			this.$http.post('/api/place/delete', this.place).then (response => {
				alertify.notify(response.body.Msg, 'success', 2, function(){
					window.location.replace("/place");
				})
			}, response => {
				alertify.error(response.body.Msg)
			});
		},
		cancel: function (event) {
			this.$http.get('/api/place/select/'+this.place.PlaceID, this.place).then (response => {
				this.toggle();
				this.place = response.body;
				alertify.warning("Abgebrochen!")
			}, response => {
				alertify.error(response.body.Msg);
			});
		},
		save: function (event) {
			this.$http.post('/api/place/update', this.place).then (response => {
				this.toggle();
				alertify.success(response.body.Msg)
			}, response => {
				alertify.error(response.body.Msg)
			});
		}
	},
	watch: {
		'place.Name': function(val,oldval){
			document.title = this.place.Name;
			$("li.breadcrumb-item.active").text(this.place.Name);
		},
		placetype: function(val){
			this.place.PlaceTypeID = this.placetype.PlaceTypeID;
			this.place.PlaceTypeName = this.placetype.Name;
			this.place.VisuellidentifierID = this.placetype.VisuellidentifierID;
		},

	}
});
