import '../style.scss';
import '../style.css';
import 'bootstrap';
import Vue from 'vue';
import VueResource from 'vue-resource';
import vSelect from "vue-select"
import * as alertify from 'alertifyjs';
import '../w3-theme-blue-grey.css';
import 'w3-css/w3.css';



Vue.use(VueResource);

var create = new Vue({
	delimiters: ['${', '}'],
	el: '#createForm',
	components: {vSelect},
	data: {
		place: {},
		placetypes: [],
		placetype: null,
	},
	mounted: function() {
		this.place = $.extend(true, {}, startplace);
		this.$refs.name.focus();
		
		// load place types
		this.$http.get('/api/placetype').then(response => {
			this.placetypes = response.body;
		}, response => {
			alertify.error(response.body.Msg);
		});
	},
	methods: {
		cancel: function (event) {
			window.location.replace("/place");
		},
		save: function (event) {
			this.$http.post('/api/place/insert', this.place).then (response => {
				window.location.replace("/place");
			}, response => {
				alertify.error(response.body.Msg)
			});
		}
	},
	watch: {
		placetype: function(val){
			this.place.PlaceTypeID = this.placetype.PlaceTypeID;
		}
	}

})
