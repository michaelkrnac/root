import '../style.scss';
import '../style.css';
import 'bootstrap';
import Vue from 'vue';
import VueResource from 'vue-resource';
import vSelect from "vue-select"
import * as alertify from 'alertifyjs';
import word from '../components/word.vue'
import '../w3-theme-blue-grey.css';
import 'w3-css/w3.css';



Vue.use(VueResource);

var update = new Vue({
	delimiters: ['${', '}'],
	el: '#thingDetail',
	components: {
		'word': word,
		'vSelect': vSelect
	},
	data: {
		thing: {},
		formvisible: false,
		thingtypes: [],
		thingtype: null,
		visuellidentifiers: [],
		visuellidentifier: null

	},
	mounted: function() {
		this.thing = $.extend(true, {}, startthing);
	},
	methods: {
		toggle: function (event) {
			this.formvisible = !this.formvisible;
			if (this.formvisible) {
				this.$nextTick(() => this.$refs.name.focus());
					
				// load thing types
				this.$http.get('/api/thingtype').then(response => {
					this.thingtypes = response.body;
				}, response => {
					alertify.error(response.body.Msg);
				});
				
				// load visuellidentifiers
				this.$http.get('/api/visuellidentifier').then(response => {
					this.visuellidentifiers = response.body;
					
					this.visuellidentifiers.forEach( function (v){
						v.imageurl = '/static/img/vi/'+v.VisuellidentiferID+'.svg'
					});
				}, response => {
					alertify.error(response.body.Msg);
				});
			}
		},
		askdel: function (event) {
			var _this = this;
			alertify.confirm(
				"Objekt \"" + _this.thing.Name + "\" löschen?", 
				"Bist du dir sicher, dass du das Objekt \"" + _this.thing.Name + "\" löschen möchtest?", 
				function(){
					_this.del();
				}, 
				function(){
					alertify.warning('Löschen von Objekt "' + _this.thing.Name + '" abgegbrochen!')
				}
			)
		},
		del: function (event) {
			this.$http.post('/api/thing/delete', this.thing).then (response => {
				alertify.notify(response.body.Msg, 'success', 2, function(){
					window.location.replace("/thing");
				})
			}, response => {
				alertify.error(response.body.Msg)
			});
		},
		cancel: function (event) {
			this.$http.get('/api/thing/select/'+this.thing.ThingID, this.thing).then (response => {
				this.toggle();
				this.thing = response.body;
				alertify.warning("Abgebrochen!")
			}, response => {
				alertify.error(response.body.Msg);
			});
		},
		save: function (event) {
			this.$http.post('/api/thing/update', this.thing).then (response => {
				this.toggle();
				alertify.success(response.body.Msg)
			}, response => {
				alertify.error(response.body.Msg)
			});
		}
	},
	watch: {
		'thing.Name': function(val,oldval){
			document.title = this.thing.Name;
			$("li.breadcrumb-item.active").text(this.thing.Name);
		},
		thingtype: function(val){
			this.thing.ThingTypeID = this.thingtype.ThingTypeID;
			this.thing.ThingTypeName = this.thingtype.ThingTypeName;
		},
		visuellidentifier: function(val){
			this.thing.VisuellidentifierID = this.visuellidentifier.VisuellidentiferID;
		}

	}
});
