import '../style.scss';
import '../style.css';
import 'bootstrap';
import Vue from 'vue';
import VueResource from 'vue-resource';
import vSelect from "vue-select"
import * as alertify from 'alertifyjs';
import '../w3-theme-blue-grey.css';
import 'w3-css/w3.css';



Vue.use(VueResource);

var create = new Vue({
	delimiters: ['${', '}'],
	el: '#createForm',
	components: {vSelect},
	data: {
		thing: {},
		thingtypes: [],
		thingtype: null,
		visuellidentifiers: [],
		visuellidentifier: null
	},
	mounted: function() {
		this.thing = $.extend(true, {}, startthing);
		this.$refs.name.focus();
		
		// load thing types
		this.$http.get('/api/thingtype').then(response => {
			this.thingtypes = response.body;
		}, response => {
			alertify.error(response.body.Msg);
		});
		
		// load visuellidentifiers
		this.$http.get('/api/visuellidentifier').then(response => {
			this.visuellidentifiers = response.body;
			
			this.visuellidentifiers.forEach( function (v){
				v.imageurl = '/static/img/vi/'+v.VisuellidentiferID+'.svg'
			});
		}, response => {
			alertify.error(response.body.Msg);
		});


	},
	methods: {
		cancel: function (event) {
			window.location.replace("/thing");
		},
		save: function (event) {
			this.$http.post('/api/thing/insert', this.thing).then (response => {
				window.location.replace("/thing");
			}, response => {
				alertify.error(response.body.Msg)
			});
		}
	},
	watch: {
		thingtype: function(val){
			this.thing.ThingTypeID = this.thingtype.ThingTypeID;
		},
		visuellidentifier: function(val){
			this.thing.VisuellidentifierID = this.visuellidentifier.VisuellidentiferID;
		}
	}

})
