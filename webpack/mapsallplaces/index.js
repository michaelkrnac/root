import '../style.css';
import '../w3-theme-blue-grey.css';
import 'w3-css/w3.css';
import 'ol/ol.css';
import Map from 'ol/map';
import View from 'ol/view';
import TileLayer from 'ol/layer/tile';
import Stamen from 'ol/source/stamen';
import Control from 'ol/control/control';
import FullScreen from 'ol/control/fullscreen';
import OverviewMap from 'ol/control/overviewmap';
import ScaleLine from 'ol/control/scaleline';
import proj from 'ol/proj';
import Feature from 'ol/feature';
import Point from 'ol/geom/point';
import Style from 'ol/style/style';
import Icon from 'ol/style/icon';
import VectorSource from 'ol/source/vector';
import VectorLayer from 'ol/layer/vector';
import Overlay from 'ol/overlay';

var iconFeatures = [];

var iconStyle = new Style({
	image: new Icon(/** @type {olx.style.IconOptions} */ ({
		anchor: [0.5, 32],
		anchorXUnits: 'fraction',
		anchorYUnits: 'pixels',
		src: '/static/img/map-marker.svg',
		imgSize:[30,30]
	}))
});

var vectorSource = new VectorSource();

$.ajax({
	url: '/api/place',
	contentType: 'application/json',
	success: function(data){
		$.each(data, function(index, d) {
			if(d.Lon != null && d.Lat != null) {
				var iconFeature = new Feature({
					geometry: new Point(proj.fromLonLat([d.Lon, d.Lat])),
					Name: d.Name,
					PlaceTypeName: d.PlaceTypeName,
					Description: d.Description,
					ID: d.PlaceID
				});
				iconFeature.setStyle(iconStyle);
				iconFeatures.push(iconFeature);
			}
		});
		vectorSource.addFeatures(iconFeatures);
	}
});


var vectorLayer = new VectorLayer({
	source: vectorSource
});

var rasterLayer = new TileLayer({
	source: new Stamen({
				layer: 'terrain'
            })
});

var map = new Map({
	layers: [rasterLayer, vectorLayer],
    target: document.getElementById('map'),
    view: new View({
		center: proj.fromLonLat([33.960707, 31.601374]),
        zoom: 6,	
		minZoom: 1,
		maxZoom: 16
    }),
});

map.addControl(new FullScreen());
map.addControl(new OverviewMap({
	collapsed:false,
	layers: [rasterLayer]
}));
map.addControl(new ScaleLine());

var element = document.getElementById('tooltip');

var popup = new Overlay({
	element: element,
    positioning: 'bottom-center',
    stopEvent: false,
    offset: [-7, -32]
});
map.addOverlay(popup);

// display popup on click
map.on('click', function(evt) {
	let feature = map.forEachFeatureAtPixel(evt.pixel,
		function(feature) {
			return feature;
		}
	);
        
	if (feature) {
		var coordinates = feature.getGeometry().getCoordinates();
        popup.setPosition(coordinates);
		popup.element.querySelector("span").innerHTML = "<b>" + feature.get('PlaceTypeName')+ '</b> <a target="_blank" href="/place/'+feature.get('ID')+'">' + feature.get('Name') + "</a><br/>"+ (feature.get('Description') ? feature.get('Description') : '');
		popup.element.style.visibility = "visible";
    } else {
		popup.element.style.visibility = "hidden";
    }
});

// change mouse cursor when over marker
map.on('pointermove', function(e) {
	if (e.dragging) {
		$(element).popover('dispose');
        return;
    }
	var pixel = map.getEventPixel(e.originalEvent);
    var hit = map.hasFeatureAtPixel(pixel);
    map.getTarget().style.cursor = hit ? 'pointer' : '';
});
