import '../style.scss';
import '../style.css';
import 'bootstrap';
import Vue from 'vue';
import VueResource from 'vue-resource';
import vSelect from "vue-select"
import * as alertify from 'alertifyjs';
import word from '../components/word.vue'
import '../w3-theme-blue-grey.css';
import 'w3-css/w3.css';



Vue.use(VueResource);

var update = new Vue({
	delimiters: ['${', '}'],
	el: '#tribeDetail',
	components: {
		'word': word,
		'vSelect': vSelect
	},
	data: {
		tribe: {},
		formvisible: false,
	},
	mounted: function() {
		this.tribe = $.extend(true, {}, starttribe);
	},
	methods: {
		toggle: function (event) {
			this.formvisible = !this.formvisible;
			if (this.formvisible) {
				this.$nextTick(() => this.$refs.name.focus());
			}
		},
		askdel: function (event) {
			var _this = this;
			alertify.confirm(
				"Stamm \"" + _this.tribe.Name + "\" löschen?", 
				"Bist du dir sicher, dass du den Stamm \"" + _this.tribe.Name + "\" löschen möchtest?", 
				function(){
					_this.del();
				}, 
				function(){
					alertify.warning('Löschen von Stamm "' + _this.tribe.Name + '" abgegbrochen!')
				}
			)
		},
		del: function (event) {
			this.$http.post('/api/tribe/delete', this.tribe).then (response => {
				alertify.notify(response.body.Msg, 'success', 2, function(){
					window.location.replace("/tribe");
				})
			}, response => {
				alertify.error(response.body.Msg)
			});
		},
		cancel: function (event) {
			this.$http.get('/api/tribe/select/'+this.tribe.TribeID, this.tribe).then (response => {
				this.toggle();
				this.tribe = response.body;
				alertify.warning("Abgebrochen!")
			}, response => {
				alertify.error(response.body.Msg);
			});
		},
		save: function (event) {
			this.$http.post('/api/tribe/update', this.tribe).then (response => {
				this.toggle();
				alertify.success(response.body.Msg)
			}, response => {
				alertify.error(response.body.Msg)
			});
		}
	},
	watch: {
		'tribe.Name': function(val,oldval){
			document.title = this.tribe.Name;
			$("li.breadcrumb-item.active").text(this.tribe.Name);
		}
	}
});
