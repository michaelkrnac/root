import '../style.scss';
import '../style.css';
import 'bootstrap';
import Vue from 'vue';
import VueResource from 'vue-resource';
import vSelect from "vue-select"
import * as alertify from 'alertifyjs';
import '../w3-theme-blue-grey.css';
import 'w3-css/w3.css';



Vue.use(VueResource);

var create = new Vue({
	delimiters: ['${', '}'],
	el: '#createForm',
	components: {vSelect},
	data: {
		tribe: {},
	},
	mounted: function() {
		this.tribe = $.extend(true, {}, starttribe);
		this.$refs.name.focus();
	},
	methods: {
		cancel: function (event) {
			window.location.replace("/tribe");
		},
		save: function (event) {
			this.$http.post('/api/tribe/insert', this.tribe).then (response => {
				window.location.replace("/tribe");
			}, response => {
				alertify.error(response.body.Msg)
			});
		}
	},
	watch: {
	}
})
