import '../style.scss';
import '../style.css';
import 'bootstrap';
import Vue from 'vue';
import VueResource from 'vue-resource';
import vSelect from "vue-select"
import * as alertify from 'alertifyjs';
import '../w3-theme-blue-grey.css';
import 'w3-css/w3.css';



Vue.use(VueResource);

var create = new Vue({
	delimiters: ['${', '}'],
	el: '#createForm',
	components: {vSelect},
	data: {
		plant: {},
		planttypes: [],
		planttype: null,
		visuellidentifiers: [],
		visuellidentifier: null
	},
	mounted: function() {
		this.plant = $.extend(true, {}, startplant);
		this.$refs.name.focus();
		
		// load plant types
		this.$http.get('/api/planttype').then(response => {
			this.planttypes = response.body;
		}, response => {
			alertify.error(response.body.Msg);
		});
		
		// load visuellidentifiers
		this.$http.get('/api/visuellidentifier').then(response => {
			this.visuellidentifiers = response.body;
			
			this.visuellidentifiers.forEach( function (v){
				v.imageurl = '/static/img/vi/'+v.VisuellidentiferID+'.svg'
			});
		}, response => {
			alertify.error(response.body.Msg);
		});


	},
	methods: {
		cancel: function (event) {
			window.location.replace("/plant");
		},
		save: function (event) {
			this.$http.post('/api/plant/insert', this.plant).then (response => {
				window.location.replace("/plant");
			}, response => {
				alertify.error(response.body.Msg)
			});
		}
	},
	watch: {
		planttype: function(val){
			this.plant.PlantTypeID = this.planttype.PlantTypeID;
		},
		visuellidentifier: function(val){
			this.plant.VisuellidentifierID = this.visuellidentifier.VisuellidentiferID;
		}
	}

})
