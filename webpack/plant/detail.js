import '../style.scss';
import '../style.css';
import 'bootstrap';
import Vue from 'vue';
import VueResource from 'vue-resource';
import vSelect from "vue-select"
import * as alertify from 'alertifyjs';
import word from '../components/word.vue'
import '../w3-theme-blue-grey.css';
import 'w3-css/w3.css';



Vue.use(VueResource);

var update = new Vue({
	delimiters: ['${', '}'],
	el: '#plantDetail',
	components: {
		'word': word,
		'vSelect': vSelect
	},
	data: {
		plant: {},
		formvisible: false,
		planttypes: [],
		planttype: null,
		visuellidentifiers: [],
		visuellidentifier: null

	},
	mounted: function() {
		this.plant = $.extend(true, {}, startplant);
	},
	methods: {
		toggle: function (event) {
			this.formvisible = !this.formvisible;
			if (this.formvisible) {
				this.$nextTick(() => this.$refs.name.focus());
					
				// load plant types
				this.$http.get('/api/planttype').then(response => {
					this.planttypes = response.body;
				}, response => {
					alertify.error(response.body.Msg);
				});
				
				// load visuellidentifiers
				this.$http.get('/api/visuellidentifier').then(response => {
					this.visuellidentifiers = response.body;
					
					this.visuellidentifiers.forEach( function (v){
						v.imageurl = '/static/img/vi/'+v.VisuellidentiferID+'.svg'
					});
				}, response => {
					alertify.error(response.body.Msg);
				});
			}
		},
		askdel: function (event) {
			var _this = this;
			alertify.confirm(
				"Pflanze \"" + _this.plant.Name + "\" löschen?", 
				"Bist du dir sicher, dass du die Pflanze \"" + _this.plant.Name + "\" löschen möchtest?", 
				function(){
					_this.del();
				}, 
				function(){
					alertify.warning('Löschen von Pflanze "' + _this.plant.Name + '" abgegbrochen!')
				}
			)
		},
		del: function (event) {
			this.$http.post('/api/plant/delete', this.plant).then (response => {
				alertify.notify(response.body.Msg, 'success', 2, function(){
					window.location.replace("/plant");
				})
			}, response => {
				alertify.error(response.body.Msg)
			});
		},
		cancel: function (event) {
			this.$http.get('/api/plant/select/'+this.plant.PlantID, this.plant).then (response => {
				this.toggle();
				this.plant = response.body;
				alertify.warning("Abgebrochen!")
			}, response => {
				alertify.error(response.body.Msg);
			});
		},
		save: function (event) {
			this.$http.post('/api/plant/update', this.plant).then (response => {
				this.toggle();
				alertify.success(response.body.Msg)
			}, response => {
				alertify.error(response.body.Msg)
			});
		}
	},
	watch: {
		'plant.Name': function(val,oldval){
			document.title = this.plant.Name;
			$("li.breadcrumb-item.active").text(this.plant.Name);
		},
		planttype: function(val){
			this.plant.PlantTypeID = this.planttype.PlantTypeID;
			this.plant.PlantTypeName = this.planttype.PlantTypeName;
		},
		visuellidentifier: function(val){
			this.plant.VisuellidentifierID = this.visuellidentifier.VisuellidentiferID;
		}

	}
});
