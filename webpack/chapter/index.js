﻿import '../style.css';
import '../w3-theme-blue-grey.css';
import * as toastr from 'toastr';
import 'w3-css/w3.css';
import * as alertify from 'alertifyjs';

toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-bottom-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "1500",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

function scroll_to_vers(versID){
	$("html, body").animate({ scrollTop: $('#'+versID).offset().top -100 }, 1000);
}

window.scroll_to_vers = scroll_to_vers;

$( document ).ready(function() {

	 // if therer is a HTML anchor in the url scroll to the vers and highlight it
	 if (window.location.hash) {
		var hash = window.location.hash.substr(1);
		$('#'+ hash).parent().parent().css('background-color', '#FFFACD');
		$("html, body").animate({ scrollTop: $('#'+hash).offset().top -100 }, 1000);
	 }

	// observe if the user has selected some words of a verse
	$(document).on('mouseup touchend', '.vers', function(){
		saveSelection(window.getSelection());
	});
	
	// observe if the user has clicked a wordpart
	$(document).on('click', '.word', function(event){
		handleWord(event);
	});
	
	// observe if the user has clicked a person
	$(document).on('click', '.person', function(event){
		window.open('/person/'+event.target.id,'_blank');
	});

	 // observe if the user has clicked a place
	$(document).on('click', '.place', function(event){
		window.open('/place/'+event.target.id,'_blank');
	});

	// observe if the user has clicked a animal
	$(document).on('click', '.animal', function(event){
		window.open('/animal/'+event.target.id,'_blank');
	});

	// observe if the user has clicked a material
	$(document).on('click', '.material', function(event){
		window.open('/material/'+event.target.id,'_blank');
	});

	// observe if the user has clicked a plant
	$(document).on('click', '.plant', function(event){
		window.open('/plant/'+event.target.id,'_blank');
	});
	
	 // observe if the user has clicked a instrument
	$(document).on('click', '.instrument', function(event){
		window.open('/instrument/'+event.target.id,'_blank');
	});

	 // observe if the user has clicked a tribe
	$(document).on('click', '.tribe', function(event){
		window.open('/tribe/'+event.target.id,'_blank');
	});

	// observe if the user has clicked a unit
	$(document).on('click', '.unit', function(event){
		window.open('/unit/'+event.target.id,'_blank');
	});

	// observe if the user has clicked a thing
	$(document).on('click', '.thing', function(event){
		window.open('/thing/'+event.target.id,'_blank');
	});



	// highlight wordparts when site is loaded
	$.ajax({
		url: "/api/word",
		type: 'POST',
		data: JSON.stringify(data),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json'
	})
	.done(function( data ) {
		highlightWords(data);
	})
	.fail(function( data ) {
		toastr.error('Ich konnte die markierten Wörter nicht laden.', 'Fehler!');
	});
});


 
function saveSelection(selection) {
	// single click produces empty string therefor validate click only if a textselection is done by doubleclick or selecting
	if (selection.toString() != "") {
		try {
			// validate selection
			if (selection.toString() == " ") throw  'Du kannst keine Leerzeichen markieren';
			if (selection.anchorNode.parentNode.id != selection.focusNode.parentNode.id) throw 'Deine Auswahl darf nur innerhalb eines Verses sein.';
			if ((selection.focusOffset - selection.anchorOffset) < 0) throw 'Du darfst nur forwärts markieren.';
			
			// to store a selection in the database we require
			// the postion in the raw text
			// if there are allready selections in the vers we have
			// to calculate the offset
			var offset = 1; // add 1 because mysql starts to count with 1
			var node = selection.anchorNode.previousSibling;
			if (node != null) {
				do {
					offset = offset + node.textContent.length;
					node = node.previousSibling
				} while (node != null);
			}

			// define data for ajax
			var word = {
				VersID: parseInt(selection.anchorNode.parentNode.id), 
				Position: parseInt(selection.anchorOffset + offset), 
				Length: parseInt(selection.focusOffset - selection.anchorOffset)
			};
			
			// send ajax
			$.ajax({
				url: "/api/word/insert",
				type: 'POST',
				data: JSON.stringify(word),
				contentType: 'application/json; charset=utf-8',
				dataType: 'json'
			})
			.done(function( data ) {
				toastr.success("Textbereich erfolgreich gespeichert.");
				removeHighlightWordsInVerse(word.VersID);
				addHighlightWordsInVerse(word.VersID);
			})
			.fail(function( data ) {
				toastr.error("Fehler beim speichern.", "Fehler!");
			});
		}
		catch(err) {
			toastr.error(err, 'Fehler!');
		}
	}
	
}

function addHighlightWordsInVerse(vers_id) {
	var vers = {
		"VersID": parseInt(vers_id) 
	};
	
	$.ajax({
		url: "/api/word/select",
		type: 'POST',
		data: JSON.stringify(vers),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json'
	})
	.done(function( data ) {
		highlightWords(data);
	})
	.fail(function( data ) {
		toastr.error('Ich konnte die markierten Wörter nicht laden.', "Fehler !");
	});
}

function removeHighlightWordsInVerse(vers_id) {
	var vers = $("#" + vers_id +".vers")
	var text = vers.text();
	vers.html(text);
}

function highlightWords(data){
	var last_vers_id = 0;
	var offset = 0;
	
	$.each(data, function(index, word) {
		if (last_vers_id != word.VersID) {
			offset = 0;
		}
        
		var before_tag = '<span class="' + word.EntityType + '" id="' + word.EntityID + '">';
		var after_tag = '</span>';
  
		var span = before_tag + word.Word + after_tag;
		var vers = $("#" + word.VersID + ".vers").html();
		var reg = "(^.{" + (word.Position - 1 + offset) + "})(.{" + word.Length + "})(.*$)";
		var regex = new RegExp(reg, "g");
		var match = regex.exec(vers);
		var vers_new = match[1] + span + match[3]; 
		$("#" + word.VersID + ".vers").html(vers_new);
			
		last_vers_id = word.VersID;
		offset = offset + before_tag.length + after_tag.length;
	});
}

function handleWord(event){
	var textbereich = event.target.textContent;
	var vers_id = event.target.parentElement.id;
	var data = {
        "WordID": parseInt(event.target.id)
    };
	
	alertify.confirm(
		"Textbereich \"" + textbereich + "\" löschen?", 
		"Bist du dir sicher, dass du den Texbereich \"" + textbereich + "\" löschen möchtest?", 
		function(){ 
			$.ajax({
				url: "/api/word/delete",
				type: 'POST',
				data: JSON.stringify(data),
				contentType: 'application/json; charset=utf-8',
				dataType: 'json'
			})
			.done(function( data ) {
				toastr.success("Textbereich \""+ textbereich + "\" gelöscht.", "Gelöscht!");
				removeHighlightWordsInVerse(vers_id);
				addHighlightWordsInVerse(vers_id);
			})
			.fail(function( data ) {
				toastr.error("Fehler beim löschen aufgetreten.", "Fehler!");
			});
		}, 
		function(){ toastr.warning('Löschen von Textbereich "' + textbereich + '" abgegbrochen!', "Abgebrochen!")}
	)
	.set('labels', {ok:'Löschen', cancel:'Abbrechen'});
}
