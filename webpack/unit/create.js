import '../style.scss';
import '../style.css';
import 'bootstrap';
import Vue from 'vue';
import VueResource from 'vue-resource';
import vSelect from "vue-select"
import * as alertify from 'alertifyjs';
import '../w3-theme-blue-grey.css';
import 'w3-css/w3.css';



Vue.use(VueResource);

var create = new Vue({
	delimiters: ['${', '}'],
	el: '#createForm',
	components: {vSelect},
	data: {
		unit: {},
		unittypes: [],
		unittype: null,
		visuellidentifiers: [],
		visuellidentifier: null
	},
	mounted: function() {
		this.unit = $.extend(true, {}, startunit);
		this.$refs.name.focus();
		
		// load unit types
		this.$http.get('/api/unittype').then(response => {
			this.unittypes = response.body;
		}, response => {
			alertify.error(response.body.Msg);
		});
		
		// load visuellidentifiers
		this.$http.get('/api/visuellidentifier').then(response => {
			this.visuellidentifiers = response.body;
			
			this.visuellidentifiers.forEach( function (v){
				v.imageurl = '/static/img/vi/'+v.VisuellidentiferID+'.svg'
			});
		}, response => {
			alertify.error(response.body.Msg);
		});


	},
	methods: {
		cancel: function (event) {
			window.location.replace("/unit");
		},
		save: function (event) {
			this.$http.post('/api/unit/insert', this.unit).then (response => {
				window.location.replace("/unit");
			}, response => {
				alertify.error(response.body.Msg)
			});
		}
	},
	watch: {
		unittype: function(val){
			this.unit.UnitTypeID = this.unittype.UnitTypeID;
		},
		visuellidentifier: function(val){
			this.unit.VisuellidentifierID = this.visuellidentifier.VisuellidentiferID;
		}
	}

})
