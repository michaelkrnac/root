import '../style.scss';
import '../style.css';
import 'bootstrap';
import Vue from 'vue';
import VueResource from 'vue-resource';
import vSelect from "vue-select"
import * as alertify from 'alertifyjs';
import word from '../components/word.vue'

Vue.use(VueResource);

var update = new Vue({
	delimiters: ['${', '}'],
	el: '#unitDetail',
	components: {
		'word': word,
		'vSelect': vSelect
	},
	data: {
		unit: {},
		formvisible: false,
		unittypes: [],
		unittype: null,
		visuellidentifiers: [],
		visuellidentifier: null

	},
	mounted: function() {
		this.unit = $.extend(true, {}, startunit);
	},
	methods: {
		toggle: function (event) {
			this.formvisible = !this.formvisible;
			if (this.formvisible) {
				this.$nextTick(() => this.$refs.name.focus());
					
				// load unit types
				this.$http.get('/api/unittype').then(response => {
					this.unittypes = response.body;
				}, response => {
					alertify.error(response.body.Msg);
				});
				
				// load visuellidentifiers
				this.$http.get('/api/visuellidentifier').then(response => {
					this.visuellidentifiers = response.body;
					
					this.visuellidentifiers.forEach( function (v){
						v.imageurl = '/static/img/vi/'+v.VisuellidentiferID+'.svg'
					});
				}, response => {
					alertify.error(response.body.Msg);
				});
			}
		},
		askdel: function (event) {
			var _this = this;
			alertify.confirm(
				"Maßeinheit \"" + _this.unit.Name + "\" löschen?", 
				"Bist du dir sicher, dass du die Maßeinheit \"" + _this.unit.Name + "\" löschen möchtest?", 
				function(){
					_this.del();
				}, 
				function(){
					alertify.warning('Löschen von Maßeinheit "' + _this.unit.Name + '" abgegbrochen!')
				}
			)
		},
		del: function (event) {
			this.$http.post('/api/unit/delete', this.unit).then (response => {
				alertify.notify(response.body.Msg, 'success', 2, function(){
					window.location.replace("/unit");
				})
			}, response => {
				alertify.error(response.body.Msg)
			});
		},
		cancel: function (event) {
			this.$http.get('/api/unit/select/'+this.unit.UnitID, this.unit).then (response => {
				this.toggle();
				this.unit = response.body;
				alertify.warning("Abgebrochen!")
			}, response => {
				alertify.error(response.body.Msg);
			});
		},
		save: function (event) {
			this.$http.post('/api/unit/update', this.unit).then (response => {
				this.toggle();
				alertify.success(response.body.Msg)
			}, response => {
				alertify.error(response.body.Msg)
			});
		}
	},
	watch: {
		'unit.Name': function(val,oldval){
			document.title = this.unit.Name;
			$("li.breadcrumb-item.active").text(this.unit.Name);
		},
		unittype: function(val){
			this.unit.UnitTypeID = this.unittype.UnitTypeID;
			this.unit.UnitTypeName = this.unittype.UnitTypeName;
		},
		visuellidentifier: function(val){
			this.unit.VisuellidentifierID = this.visuellidentifier.VisuellidentiferID;
		}

	}
});
