import '../style.scss';
import '../style.css';
import 'bootstrap';
import Vue from 'vue';
import VueResource from 'vue-resource'
import unitlist from '../components/unitlist.vue'

Vue.use(VueResource);

var avm = new Vue({
	el: '#units',
	components: {
		'unitlist': unitlist
	}
});


