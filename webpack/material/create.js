import '../style.scss';
import '../style.css';
import 'bootstrap';
import Vue from 'vue';
import VueResource from 'vue-resource';
import vSelect from "vue-select"
import * as alertify from 'alertifyjs';
import '../w3-theme-blue-grey.css';
import 'w3-css/w3.css';



Vue.use(VueResource);

var create = new Vue({
	delimiters: ['${', '}'],
	el: '#createForm',
	components: {vSelect},
	data: {
		material: {},
		materialtypes: [],
		materialtype: null,
		visuellidentifiers: [],
		visuellidentifier: null
	},
	mounted: function() {
		this.material = $.extend(true, {}, startmaterial);
		this.$refs.name.focus();
		
		// load material types
		this.$http.get('/api/materialtype').then(response => {
			this.materialtypes = response.body;
		}, response => {
			alertify.error(response.body.Msg);
		});
		
		// load visuellidentifiers
		this.$http.get('/api/visuellidentifier').then(response => {
			this.visuellidentifiers = response.body;
			
			this.visuellidentifiers.forEach( function (v){
				v.imageurl = '/static/img/vi/'+v.VisuellidentiferID+'.svg'
			});
		}, response => {
			alertify.error(response.body.Msg);
		});


	},
	methods: {
		cancel: function (event) {
			window.location.replace("/material");
		},
		save: function (event) {
			this.$http.post('/api/material/insert', this.material).then (response => {
				window.location.replace("/material");
			}, response => {
				alertify.error(response.body.Msg)
			});
		}
	},
	watch: {
		materialtype: function(val){
			this.material.MaterialTypeID = this.materialtype.MaterialTypeID;
		},
		visuellidentifier: function(val){
			this.material.VisuellidentifierID = this.visuellidentifier.VisuellidentiferID;
		}
	}

})
