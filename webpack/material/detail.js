import '../style.scss';
import '../style.css';
import 'bootstrap';
import Vue from 'vue';
import VueResource from 'vue-resource';
import vSelect from "vue-select"
import * as alertify from 'alertifyjs';
import word from '../components/word.vue'
import '../w3-theme-blue-grey.css';
import 'w3-css/w3.css';



Vue.use(VueResource);

var update = new Vue({
	delimiters: ['${', '}'],
	el: '#materialDetail',
	components: {
		'word': word,
		'vSelect': vSelect
	},
	data: {
		material: {},
		formvisible: false,
		materialtypes: [],
		materialtype: null,
		visuellidentifiers: [],
		visuellidentifier: null

	},
	mounted: function() {
		this.material = $.extend(true, {}, startmaterial);
	},
	methods: {
		toggle: function (event) {
			this.formvisible = !this.formvisible;
			if (this.formvisible) {
				this.$nextTick(() => this.$refs.name.focus());
					
				// load material types
				this.$http.get('/api/materialtype').then(response => {
					this.materialtypes = response.body;
				}, response => {
					alertify.error(response.body.Msg);
				});
				
				// load visuellidentifiers
				this.$http.get('/api/visuellidentifier').then(response => {
					this.visuellidentifiers = response.body;
					
					this.visuellidentifiers.forEach( function (v){
						v.imageurl = '/static/img/vi/'+v.VisuellidentiferID+'.svg'
					});
				}, response => {
					alertify.error(response.body.Msg);
				});
			}
		},
		askdel: function (event) {
			var _this = this;
			alertify.confirm(
				"Material \"" + _this.material.Name + "\" löschen?", 
				"Bist du dir sicher, dass du das Material \"" + _this.material.Name + "\" löschen möchtest?", 
				function(){
					_this.del();
				}, 
				function(){
					alertify.warning('Löschen von Material "' + _this.material.Name + '" abgegbrochen!')
				}
			)
		},
		del: function (event) {
			this.$http.post('/api/material/delete', this.material).then (response => {
				alertify.notify(response.body.Msg, 'success', 2, function(){
					window.location.replace("/material");
				})
			}, response => {
				alertify.error(response.body.Msg)
			});
		},
		cancel: function (event) {
			this.$http.get('/api/material/select/'+this.material.MaterialID, this.material).then (response => {
				this.toggle();
				this.material = response.body;
				alertify.warning("Abgebrochen!")
			}, response => {
				alertify.error(response.body.Msg);
			});
		},
		save: function (event) {
			this.$http.post('/api/material/update', this.material).then (response => {
				this.toggle();
				alertify.success(response.body.Msg)
			}, response => {
				alertify.error(response.body.Msg)
			});
		}
	},
	watch: {
		'material.Name': function(val,oldval){
			document.title = this.material.Name;
			$("li.breadcrumb-item.active").text(this.material.Name);
		},
		materialtype: function(val){
			this.material.MaterialTypeID = this.materialtype.MaterialTypeID;
			this.material.MaterialTypeName = this.materialtype.MaterialTypeName;
		},
		visuellidentifier: function(val){
			this.material.VisuellidentifierID = this.visuellidentifier.VisuellidentiferID;
		}

	}
});
