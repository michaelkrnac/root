import '../style.scss';
import '../style.css';
import 'bootstrap';
import Vue from 'vue';
import VueResource from 'vue-resource'
import materiallist from '../components/materiallist.vue'
import '../w3-theme-blue-grey.css';
import 'w3-css/w3.css';



Vue.use(VueResource);

var avm = new Vue({
	el: '#materials',
	components: {
		'materiallist': materiallist
	}
});


