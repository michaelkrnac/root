import '../style.scss';
import '../style.css';
import 'bootstrap';
import Vue from 'vue';
import VueResource from 'vue-resource';
import snippet from '../components/snippet.vue'
 
Vue.use(VueResource);

var app = new Vue({
	el: '#snippets',
	components: {
		'snippet': snippet
	}

});
