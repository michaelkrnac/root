import '../style.scss';
import '../style.css';
import 'bootstrap';
import Vue from 'vue';
import VueResource from 'vue-resource';
import vSelect from "vue-select"
import * as alertify from 'alertifyjs';
import '../w3-theme-blue-grey.css';
import 'w3-css/w3.css';



Vue.use(VueResource);

var create = new Vue({
	delimiters: ['${', '}'],
	el: '#createForm',
	components: {vSelect},
	data: {
		instrument: {},
		instrumenttypes: [],
		instrumenttype: null,
		visuellidentifiers: [],
		visuellidentifier: null
	},
	mounted: function() {
		this.instrument = $.extend(true, {}, startinstrument);
		this.$refs.name.focus();
		
		// load instrument types
		this.$http.get('/api/instrumenttype').then(response => {
			this.instrumenttypes = response.body;
		}, response => {
			alertify.error(response.body.Msg);
		});
		
		// load visuellidentifiers
		this.$http.get('/api/visuellidentifier').then(response => {
			this.visuellidentifiers = response.body;
			
			this.visuellidentifiers.forEach( function (v){
				v.imageurl = '/static/img/vi/'+v.VisuellidentiferID+'.svg'
			});
		}, response => {
			alertify.error(response.body.Msg);
		});


	},
	methods: {
		cancel: function (event) {
			window.location.replace("/instrument");
		},
		save: function (event) {
			this.$http.post('/api/instrument/insert', this.instrument).then (response => {
				window.location.replace("/instrument");
			}, response => {
				alertify.error(response.body.Msg)
			});
		}
	},
	watch: {
		instrumenttype: function(val){
			this.instrument.InstrumentTypeID = this.instrumenttype.InstrumentTypeID;
		},
		visuellidentifier: function(val){
			this.instrument.VisuellidentifierID = this.visuellidentifier.VisuellidentiferID;
		}
	}

})
