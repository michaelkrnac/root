import '../style.scss';
import '../style.css';
import 'bootstrap';
import Vue from 'vue';
import VueResource from 'vue-resource';
import vSelect from "vue-select"
import * as alertify from 'alertifyjs';
import word from '../components/word.vue'
import '../w3-theme-blue-grey.css';
import 'w3-css/w3.css';



Vue.use(VueResource);

var update = new Vue({
	delimiters: ['${', '}'],
	el: '#instrumentDetail',
	components: {
		'word': word,
		'vSelect': vSelect
	},
	data: {
		instrument: {},
		formvisible: false,
		instrumenttypes: [],
		instrumenttype: null,
		visuellidentifiers: [],
		visuellidentifier: null

	},
	mounted: function() {
		this.instrument = $.extend(true, {}, startinstrument);
	},
	methods: {
		toggle: function (event) {
			this.formvisible = !this.formvisible;
			if (this.formvisible) {
				this.$nextTick(() => this.$refs.name.focus());
					
				// load instrument types
				this.$http.get('/api/instrumenttype').then(response => {
					this.instrumenttypes = response.body;
				}, response => {
					alertify.error(response.body.Msg);
				});
				
				// load visuellidentifiers
				this.$http.get('/api/visuellidentifier').then(response => {
					this.visuellidentifiers = response.body;
					
					this.visuellidentifiers.forEach( function (v){
						v.imageurl = '/static/img/vi/'+v.VisuellidentiferID+'.svg'
					});
				}, response => {
					alertify.error(response.body.Msg);
				});
			}
		},
		askdel: function (event) {
			var _this = this;
			alertify.confirm(
				"Instrument \"" + _this.instrument.Name + "\" löschen?", 
				"Bist du dir sicher, dass du das Instrument \"" + _this.instrument.Name + "\" löschen möchtest?", 
				function(){
					_this.del();
				}, 
				function(){
					alertify.warning('Löschen von Instrument "' + _this.instrument.Name + '" abgegbrochen!')
				}
			)
		},
		del: function (event) {
			this.$http.post('/api/instrument/delete', this.instrument).then (response => {
				alertify.notify(response.body.Msg, 'success', 2, function(){
					window.location.replace("/instrument");
				})
			}, response => {
				alertify.error(response.body.Msg)
			});
		},
		cancel: function (event) {
			this.$http.get('/api/instrument/select/'+this.instrument.InstrumentID, this.instrument).then (response => {
				this.toggle();
				this.instrument = response.body;
				alertify.warning("Abgebrochen!")
			}, response => {
				alertify.error(response.body.Msg);
			});
		},
		save: function (event) {
			this.$http.post('/api/instrument/update', this.instrument).then (response => {
				this.toggle();
				alertify.success(response.body.Msg)
			}, response => {
				alertify.error(response.body.Msg)
			});
		}
	},
	watch: {
		'instrument.Name': function(val,oldval){
			document.title = this.instrument.Name;
			$("li.breadcrumb-item.active").text(this.instrument.Name);
		},
		instrumenttype: function(val){
			this.instrument.InstrumentTypeID = this.instrumenttype.InstrumentTypeID;
			this.instrument.InstrumentTypeName = this.instrumenttype.InstrumentTypeName;
		},
		visuellidentifier: function(val){
			this.instrument.VisuellidentifierID = this.visuellidentifier.VisuellidentiferID;
		}

	}
});
