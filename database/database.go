package database

import (
	"github.com/jmoiron/sqlx"

	"log"
)

// Con holds the DB connection
var Con *sqlx.DB

// Connect conntest to the database
func Connect(dsn string) {
	var err error
	Con, err = sqlx.Connect("mysql", dsn)
	if err != nil {
		log.Print(err)
	}
	Con.SetConnMaxLifetime(100)
	Con.SetMaxIdleConns(5)
	Con.SetMaxOpenConns(5)
}
