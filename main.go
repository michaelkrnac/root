// Package main includes the commandline interface
package main

import (
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/michaelkrnac/root/configuration"
	"gitlab.com/michaelkrnac/root/database"
	"gitlab.com/michaelkrnac/root/handler"
)

// main starts the http server.
// It loads the config from the file hardcoded in this function.
// It connects to the database by DSN String defined in the config file.
// It starts the http server on the port givven in the config.
// All Requests are dispatched by the APP handler.
func main() {
	configuration.Load("config.json")

	database.Connect(configuration.Conf.DSN)
	defer database.Con.Close()

	app := new(handler.APP)

	log.Printf("Starting root server on port %s", configuration.Conf.Port)
	err := http.ListenAndServe(configuration.Conf.Port, app)
	if err != nil {
		log.Fatal(err)
	}
}
